---
layout: default
title: Dévoreur d'intellect
nav_order: 57
parent: PNJ Tactics
---

# DÉVOREUR D'INTELLECT

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

C'est un cerveau ! Avec des pieds ! Qu'est-ce qui n'est pas de l'amour ? D'une part, le fait qu'il se nourrisse de votre conscience et qu'il prenne le contrôle de votre corps.

Les vieux joueurs de Donjons & Dragons Avancés se souviendront du dévoreur d'intellect comme l'un des deux monstres les plus mémorables avec les psioniques - cet ensemble de règles supplémentaires étrange et compliqué qui permettait la télépathie, la télékinésie et le combat mental. La cinquième édition de D&D s'est débarrassée de tout cela. Les pouvoirs psioniques sont maintenant traités comme un trait spécial, une forme de lancé de sort ou les deux. Dans la version 5E du Dévoreur d'intellect, ceci est intégré dans ses caractéristiques de *Détection des conscience*, de *Dévoration d'intellect* et de *Voleur de corps*.

Selon le texte de la version 5E du Monster Manual, les dévoreurs d'intellect sont des aberrations, créées pour servir les intérêts des flagelleurs mentaux. Ce ne sont pas des créatures indépendantes. Vous n'allez pas en croiser un au hasard dans les bois. Au contraire, tout dévoreur d'intellect que vos personnages joueur rencontreront sera durant une mission. Cela affectera sur qui il utilisera ses pouvoirs et quand.

Tout d'abord, la décomposition habituelle. Les dévoreurs d'intellect ont une Force très faible, ainsi qu'une grande Dextérité et une grande Constitution, un profil d'escarmoucheur . On n'a pas l'impression, cependant, que les dévoreurs d'intellect sont une espèce nombreuse, ils n'auront donc pas beaucoup d'occasions de dominer leurs adversaires. Ils ont une Intelligence légèrement supérieure à la moyenne et une Sagesse et un Charisme moyens (selon la mesure humanoïde), donc ce ne sont pas seulement des animaux - ils peuvent planifier et s'adapter. Ils sont compétents en matière de furtivité et, bien qu'ils n'aient ni yeux ni oreilles, ils ont une *vision aveugle* sur 18 mètres et une *Détection des consciences* sur 90 mètres.

*Attaques multiples* comprend une utilisation du pouvoir de *Dévoration de l'intellect*, et *Dévoration de l'intellect* peut infliger l'état *Étourdi* ou l'état *Inconscient* (si les PV tombent à zéro, voir Perdre conscience, PHB p.197). Ces deux conditions incluent l'état *Neutralisé*, et *Voleur de corps* a besoin d'un adversaire humanoïde *Neutralisé*, donc comment ces caractéristiques s'assemblent : le dévoreur d'intellect utilise *Dévoration de l'intellect* jusqu'à ce qu'il étourdisse un adversaire, puis utilise *Voleur de corps* contre lui.

Combien de temps faudra-t-il à un dévoreur d'intellect pour étourdir un adversaire ? Cela dépend de l'Intelligence de l'adversaire. Examinons trois cas : un roturier ordinaire, un aventurier moyen et un aventurier dont la principale aptitude requise est l'Intelligence et qui possède une maîtrise sur les jets de sauvegarde d'Intelligence :

* un roturier avec une Intelligence de 10 a 55 % de chances d'échouer à son jet de sauvegarde contre *Dévoration de l'intellect*, alors que le dévoreur d'intellect a environ 62 % de chances d'étourdir. La probabilité que les deux se produisent est d'environ 34%.
* un aventurier moyen avec une Intelligence de 12 a 50% de chance d'échouer à son jet de sauvegarde, et la chance d'étourdir est d'environ 37%. La probabilité que les deux se produisent est d'environ 19 %.
* un magicien de bas niveau possédant une Intelligence de 15 et un bonus de maîtrise de +2 a 35 % de chances d'échouer sà son jet de sauvegarde, et les chances d'étourdir ne sont que d'environ 9 %. La probabilité que les deux se produisent est d'environ 3 %.

Il semble qu'aucun aventurier ne se fasse manger le cerveau aujourd'hui, n'est-ce pas ? Il faut deux tours pour que le dévoreur ait plus de 50 % de chances d'étourdir un roturier, trois tours pour avoir plus de deux tiers de chances. En revanche, trois tours ne suffisent pas pour donner au dévoreur d'intellect une chance sur deux d'étourdir un aventurier moyen, et le sorcier peut attendre que son associé, Hrodvald Thunderfist, écrase le dévoreur.

Cela nous amène à une conclusion intéressante : lorsqu'un groupe de PJs rencontre pour la première fois un dévoreur d'intellect, ce n'est probablement pas sous sa forme de dévoreur d'intellect. Au lieu de cela, il sera le marionnettiste du corps d'un roturier dont il a déjà dévoré l'intellect (au fait, il y a une erreur dans le bloc de statistiques dans le Monster Manual. Il ne suffit pas de réduire le corps de l'hôte à 0 pv pour chasser le dévoreur d'intellect : le corps doit en fait mourir, ou il faut utiliser l'une des deux autres méthodes énumérées). Le seul PJ qu'un dévoreur d'intellect est susceptible d'attaquer avec *Dévoration de l'intellect* est celui qui a choisi Intelligence comme sa stat la plus faible.

Arrêtons-nous un instant et constatons que *Étourdi* et *Inconscient* ne sont pas les seules conditions qui incluent *Neutralisé*. Une personne *Paralysée* ou *Pétrifiée* est également *Neutralisé*, de sorte qu'un humanoïde sous l'effet d'une sort de *Immobiliser un humanoïde*, par exemple, serait également vulnérable à une attaque de *voleur de corps*. *Motif hypnotique* et *Fou rire de Tasha* sont deux sorts de bas niveau qui peuvent infliger directement l'état *Neutralisé*. Un dévoreur d'intellect qui a déjà pris le corps d'un lanceur de sorts qui connaît *Motif hypnotique* (un lanceur de sorts extraordinairement malchanceux, il faut le conclure des probabilités ci-dessus) pourrait facilement utiliser ce sort pour préparer les corps hôtes pour d'autres dévoreurs d'intellect sans qu'ils aient à utiliser *Dévoration de l'intellect* du tout (*Sommeil* fonctionnerait également). Notez également que *Voleur de corps* ne donne pas d'immunité à la cible si le dévoreur d'intellect ne réussit pas du premier coup.

OK, alors que fait un dévoreur d'intellect pendant qu'il manipule un corps hôte ? Ce qu'il a été chargé de faire, aussi discrètement que possible. Le corps hôte n'est utile que tant que l'apparence est maintenue. Un dévoreur d'intellect éjecté de son hôte courra comme un rat, ne se battant que s'il est acculé (dans cette situation, il utilise son *Attaques multiples*, attaquant avec ses griffes et utilisant *Dévoration d'intellect* contre son adversaire, mais il peut ou non utiliser *Voleur de corps* sur un adversaire *Neutralisé* - sauf si cela l'aide à s'enfuir). Il résiste aux dégâts des armes normales, sa classe d'armure n'est pas particulièrement élevée, et il a une grande vitesse de mouvement, donc un dévoreur d'intellect en fuite utilisera *Se précipiter* plutôt qu'*Esquiver*. A une exception près : lors de son premier tour de fuite, si elle a deux ou plusieurs attaquants au corps à corps non surpris dans des cases ou des hexagones adjacents, sa première action sera de se *Désengager*.

Et que dire de ce cas où le "brain-puppy" essaie de *Dévoration d'intellect* sur un PJ plus faible ? Il ne va pas le faire à découvert, c'est sûr. Il va utiliser *Discrétion* pour se cacher en embuscade, jusqu'à ce qu'un PJ s'en approche à moins de trois mètres. Il n'utilisera que *Dévoration d'intellect*, pas sa combinaison *Attaques multiples* griffe / *Dévoration d'intellect*. De cette façon, il ne donne pas sa position s'il échoue, car *Dévoration d'intellect* n'est techniquement pas une attaque - Hrodvald a simplement un mal de tête inexpliqué et soudain. Si le dévoreur d'intellest ne peut pas faire tomber sa cible en trois rounds, il abandonne et s'enfuit. Cela lui fait perdre sa position, mais il ne devrait avoir que peu ou pas de difficultés à s'échapper.
