---
layout: page
title: Tyrannoeils revisités
nav_order: 10
parent: PNJ Tactics
---

# TYRANNOEILS REVISITÉS

Le Guide des monstres de Volo est très complet dans son traitement des tyrannoeils, tant en termes de tactique que de saveur. Il contient des informations sur la façon de déterminer l'apparence et le comportement d'un tyrannoeil, la disposition et le contenu de son repaire, et même l'origine des bébés tyrannoeils (c'est assez bizarre). Puisque ce blog est axé sur les tactiques, je vais me concentrer sur ce point.

Pour l'essentiel, tout ce que j'ai dit dans mon [analyse initiale des tyrannoeil](../tyrannoeil/) tient la route, mais il y a une petite contradiction sous-entendue.

Selon le bloc de statistiques du Monster Manual, "Le tyrannœil tire trois des rayons oculaires magiques suivants (déterminez-les aléatoirement et relancez en cas de résultats identiques), en désignant une à trois cibles situées à 36 mètres ou moins de lui et dans son champ de vision". Sur cette base, j'ai conclu que le tyrannoeil très intelligent dirigerait ces rayons choisis au hasard vers les adversaires les moins capables de leur résister.

Volo's nous informe : "Un spectateur analyse ses adversaires, prend note des armures, des armes et des tactiques, et ajuste sa stratégie pour éliminer les menaces les plus dangereuses le plus rapidement possible". En soi, ce n'est pas nécessairement une contradiction, mais cela va plus loin : "Un observateur peut tirer plusieurs rayons oculaires à son tour, et il peut les utiliser tous successivement sur son ennemi le plus dangereux. Même un combattant très dur aura des doutes après avoir subi les dommages d'un rayon de désintégration, d'un rayon de nécrose et d'un rayon de mort".

Aussi intelligent que soit un tyrannoeil, pourquoi utiliserait-il un *Rayon de nécrose* contre "un combattant très coriace" ? Ce combattant aura de bonnes chances de faire un lancer de sauvegarde de  Constitution DD 16 et de réduire de moitié les dégâts. Étant donné qu'un groupe d'aventuriers typique est composé de personnages aux capacités comparables, pourquoi un spectateur renoncerait-il à la possibilité de faire des dégâts complets avec ce *Rayon de nécrose* en visant une personne peu susceptible de lui résister ? Le combattant est-il vraiment plus menaçant que le sorcier à l'arrière ? D'autant plus que le tyrannoeil peut flotter au dessus du sol, hors de portée du combattant, mais à portée des attaques du sorcier ?

De plus, pourquoi un tel génie tirerait-il ses rayons au hasard ? S'il estimait vraiment que ce combattant était une telle menace qu'il devait se concentrer sur lui à l'exclusion de ses compagnons, pourquoi ne choisirait-il pas un *Rayon de sommeil* pour le frapper, plutôt qu'un *Rayon de nécrose* ?

Cette fois, quand je suis obligé de choisir entre une application du Monster Manual et une application du Volo, je me range du côté du Monster Manual. Le bloc de statistiques dit : "au hasard". Nous pouvons interpréter cela comme l'une des façons dont le tyrannoeil, une aberration, se comporte de manière aberrante. Mais étant donné que les rayons tirent au hasard, le tyrannoeil peut toujours faire des choix intelligents sur les personnes à viser, et je pense qu'il va les viser là où ils auront le plus d'effet. Peut-être que si les membres d'un groupe diffèrent sensiblement en termes de pouvoir - disons, si ce combattant est de niveau 12, alors que tous ses compagnons sont de niveau 10 ou inférieur - il focalisera un rayon sur celle-ci même si elle n'est pas une cible optimale pour ce rayon. Mais ce serait une exception à la règle.

J'étais également sceptique lorsque j'ai vu pour la première fois l'intitulé de la sous-section "Utiliser librement l'antimagie", mais il y a là un élément important. Lorsque j'ai envisagé pour la première fois la tactique du tyrannoeil, j'ai supposé (sans même en être pleinement conscient) que le tyrannoeil identifierait la meilleure position dans son repaire et la tiendrait - et que cette position serait relativement éloignée de ses adversaires. Mais surtout si le tyrannoeil dispose d'une marge de manœuvre verticale, le gardant hors de portée des combattants au corps à corps, il pourrait utiliser son *Cône antimagie* comme "quatrième rayon oculaire" en se repositionnant de manière à pouvoir viser ce cône sur un ou plusieurs lanceurs de sorts tout en laissant les autres adversaires en dehors du cône, où ils seront sensibles aux rayons oculaires (pour résumer, le problème avec le *Cône antimagie* est qu'il ne peut pas être rétréci, de sorte que si le tyrannoeil est trop loin de ses cibles, la "part de tarte" supprime ses propres rayons oculaires ainsi que toute magie que ses adversaires pourraient utiliser. En revanche, de près, cette part de tarte est plus étroite et peut être orientée pour n'inclure qu'une ou deux cibles).

Le reste du traitement que Volo réserve aux tactiques du tyrannoeil concerne l'aménagement de son repaire et les différents sous-fifres qui s'y trouvent. Cela est utile pour planifier un scénario d'aventure plus vaste, mais cela n'a pas grand chose à voir avec les tactiques employées lors d'une seule rencontre de combat.

La semaine dernière, j'ai mentionné que le suspense vient de l'inconnu - en particulier, ne pas savoir quel est son adversaire ou ce dont il est capable. Ainsi, la section "Capacités des Variantes", page 12, est un moyen fantastique de mélanger ce qui pourrait être, pour les joueurs ayant leur propre exemplaire du Monster Manual, une rencontre trop prévisible. "Chacun de ces effets est conçu pour avoir le même niveau de puissance que celui qu'il remplace, ce qui vous permet de créer un tyrannoeil personnalisé sans modifier la dangerosité du monstre", explique Volo, bien que je pense que le fait de remplacer le *Cône antimagie* par un seul mot de puissance étourdissant augmente considérablement la puissance du tyrannoeil, en raison de tous les cas où il pourrait renoncer à l'utilisation de son *Cône antimagie* afin de maximiser l'utilisation de ses rayons oculaires.

Notez également que Volo stipule que "le jet de sauvegarde pour une capacité alternative utilise le même DD et le même score de capacité que le sort sur lequel le rayon oculaire est basé" (c'est moi qui souligne). Prenez, par exemple, la substitution du bannissement pour le *Rayon de charme* du tyrannoeil. Le *Rayon de charme* demande un jet de sauvegarde de Sagesse DD 16, mais le sort de bannissement requiert un jet de sauvegarde de Charisme.

Voilà comment notre tyrannoeil super-intelligent peut adapter ses rayons oculaires choisis au hasard à la cible qu'il veut rectifier ! Vous avez lancé un *Rayon de nécrose*, mais vous voulez vraiment que votre tyrannoeil le dirige vers ce rude combattant ? Faites-en un *métamorphose*, qui demande un jet de sauvegarde de Sagesse au lieu d'un jet de sauvegarde de Constitution, et transformez-le en chèvre.

Voici un petit tableau montrant en quoi les différents jets de sauvegarde des rayons diffèrent de ceux des rayons standard correspondants (notez que *Danse irrésistible d'Otto* et *Cécité/Surdité* prennent effet automatiquement - les jets de sauvegarde indiqués doivent mettre fin aux effets) :

|  Rayon standard   | Caractéristique de jet de sayvegarde | Variante     | Caractéristique de jet de sayvegarde |
| ----------------- | ------------------------------------ | ------------ | ------------------------------------ |
| Rayon de charisme | Sagesse | Bannissement | Charisme |
| Rayon de mort | Dextérité | Cercle de mort | Constitution |
| Rayon de mort | Dextérité | Esprit faible | Intelligence |
| Rayon de désintégration | Dextérité | Mauvais oeil | Sagesse |
| Rayon de nécrose | Constitution | Métamorphose | Sagesse |
| Rayon de peur | Sagesse | Rayon de lune | Constitution |
| Rayon de paralysie | Constitution | Modification de mémoire | Sagesse |
| Rayon de pétrification | Dexterity | Danse irrésistible d'Otto | Sagesse |
| Rayon de sommeil | Sagesse | Cécité/Surdité | Constitution |
| Rayon de lenteur | Dextérité | Jeter une malédiction | Sagesse |
| Rayon de télékinésie | Force | Coercition mystique | Sagesse |

Il existe également plusieurs sorts (silence, tempête de neige, mur de glace, mur de force) qui agissent sur l'environnement, et non sur des cibles spécifiques, et qui ne nécessitent donc pas du tout de jet de sauvegarde. Et créer des morts-vivants est une alternative contextuelle au *Rayon de nécrose*, utile uniquement s'il y a un ou deux (ou trois) cadavres à portée de main.
