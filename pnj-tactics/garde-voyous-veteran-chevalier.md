---
layout: page
title: Gardes, Voyous, Vétérans et Chevaliers
nav_order: 5
parent: PNJ Tactics
---

# GARDES, VOYOUS, VÉTÉRANS ET CHEVALIERS

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Article original : [NPC Tactics: Guards, Thugs, Veterans and Knights](http://themonstersknow.com/npc-tactics-guards-thugs-veterans-knights/)

## GARDES

Prenez un roturier qui est en meilleure forme physique que la moyenne, mettez-lui une lance à la main, donnez-lui un entraînement au combat et vous aurez un **garde**, première ligne de défense contre les personnages joueurs qui pourraient autrement se déchaîner dans les villes pittoresques du cadre de votre campagne.

Avec leurs scores de caractéristiques physique supérieurs à la moyenne (mais pas exceptionnellement), les gardes sont bien adaptés au rôle de combat simple et direct de "Allez et frappez, puis frappez encore". Cela étant dit, ils ne sont essentiellement rien d'autre que des roturiers bien formés. Ils sont plus courageux que la moyenne et plus motivés par le devoir, mais cela suppose que 99 fois sur 100, ils ne font face à aucun ennemi plus difficile qu'un autre roturier. Si on leur envoit un monstre, ils ont autant de chances de s'enfuir ou de se figer qu'ils en ont de se battre. Ils savent comment utiliser leurs armes, mais ce n'est pas la même chose que de comprendre la stratégie et la tactique ; leur sophistication ne va pas plus loin que de savoir qu'un ennemi encerclé a moins de chances de s'enfuir, donc s'ils sont plus nombreux que leurs adversaires, ils vont prendre le flanc, et s'ils ne le font pas, ils formeront une ligne, s'encercleront en se tournant le dos ou enverront courir un des leurs pour obtenir plus de gardes. Lorsqu'ils sont en danger grave - réduits à 4 pv ou moins, ou à côté d'un autre garde qui l'est - leur discipline se relâche, mais pas au point qu'ils courent sans *se désengager* (action) d'abord, à moins que ce ne soit eux qui soient gravement blessés (la différence entre leurs capacités physiques et celles d'un roturier est suffisamment importante pour que les gardes non humains adoptent l'approche directe au combat, même si les roturiers de la même race ne le feraient pas).

Notez que les gardes ne maîtrisent même pas la technique de l'intimidation. Ils peuvent crier "Halte !" mais quelqu'un ne les prendra pas plus au sérieux.

## MALFRATS

Un **Malfrat** a *Attaque multiples*, *Tactique de groupe* et un profil physique que je qualifie de " brute " : FOR et CON élevés, taillé pour le combat de mélée. Le style de combat standard des malfrats consiste à se battre en groupe, en se rapprochant le plus possible de leurs cibles afin de se donner un avantage mutuel sur leurs jets d'attaque. Bien qu'ils portent des arbalètes, ils ne sont pas spécialement faits pour le combat à distance (d'une part, leur *Attaque multiples* ne leur permet pas de recharger une arbalète plus rapidement, alors qu'elle leur permet de frapper deux fois par tour avec une masse), et d'autre part, le fait de se faire tirer dessus de loin est l'une des rares choses qui les fera réfléchir à deux fois à un combat. S'ils peuvent se permettre d'être au moins deux à se séparer et à poursuivre un adversaire à distance, ils le feront ; mais un seul ne le fera pas, car ce voyou abandonnerait alors l'avantage de *Tactique de groupe*, et ils aiment que leurs combats soient faciles.

Un malfrat gravement blessé (réduit à 12 pv ou moins) *se désengagera* (action) et battra en retraite, et au tour suivant, les alliés de ce voyou battront également en retraite, même s'ils ne sont pas eux-mêmes gravement blessés. Mais cela ne signifie en aucun cas qu'ils considèrent que le combat est terminé. Les malfrats sont disponibles en quantité presque infinie, et ils peuvent réapparaître, à la recherche d'une revanche, à tout moment, en plus grand nombre qu'auparavant (quelle que soit la difficulté de la dernière rencontre, d'après le tableau de la page 82 du Guide du maître du donjon, rendez la prochaine rencontre plus difficile d'un niveau, tant que cela vous convient pour continuer à envoyer des voyous vindicatifs contre vos personnages joueur).

Malgré leurs respectables qualités physiques et leur enthousiasme pour le combat, les voyous sont paresseux et préfèrent obtenir ce qu'ils veulent par l'intimidation, et c'est ce qu'ils vont essayer en premier. Après tout, si une victime cède maintenant, il est probable qu'elle cède encore plus tard, et encore, et encore...

## VÉTÉRANS

Un **vétéran** est un combattant entraîné et expérimenté - un soldat de rang, un capitaine de la garde, un mercenaire de longue date ou autre. Formés à la guerre, ils connaissent la stratégie et la tactique et combattront en conséquence. Ils fortifient leurs positions, profitent du terrain et des points d'étranglement, reconnaissent et tentent de contrer les plans de leurs ennemis, saisissent les occasions de punir les erreurs de leurs ennemis, attaquent les combattants les plus faibles et esquivent et désengagent les plus forts, et négocient lorsque leurs chances de victoire semblent faibles, que le combat ait déjà commencé ou non. Un ancien combattant n'est presque jamais un fanatique ; il n'a aucun intérêt à se battre jusqu'à la mort. Une blessure grave (réduite à 23 pv ou moins) suffit à convaincre un vétéran que la reddition ou la retraite est la ligne de conduite la plus sage.

La plupart des vétérans combattent au corps à corps, soit à deux mains avec une épée longue et une épée courte, soit à une seule main avec un bouclier (non mentionné dans le Manuel des monstres - avec un bouclier, la classe d'armure du vétéran est 19 et non 17) ou à deux mains sans bouclier. Mais dans un groupe donné de vétérans - ou un groupe de gardes et/ou de malfrats dirigé par un vétéran - jusqu'à la moitié d'entre eux peuvent porter des arbalètes et les utiliser soit pour attaquer des adversaires à distance, soit pour couvrir les combattants en mêlée pendant qu'ils se déplacent. Le tir de couverture est assuré par l'action *Se tenir prêt*, la condition de déclenchement étant un ennemi qui s'expose à attaquer un combattant en mouvement. Un ancien combattant avec une arme à distance, ou un combattant à distance sous le commandement d'un ancien combattant, utilisera au maximum toute couverture disponible ; il ne tirera pas d'une position à découvert. Où qu'il se trouve, il sait quelles positions lui offrent un avantage maximal au combat, il se place à l'avance sur ces positions et y reste aussi longtemps qu'il peut tenir. Ils ne peuvent pas être amenés à quitter une telle position pour poursuivre un adversaire. Ils restent conscients de leur but et ne se laissent pas distraire. De plus, les gardes qui sont couverts par des combattants tirant à distance savent mieux que quiconque qu'il ne faut pas dépasser la portée normale de ces armes à distance.

Une autre chose qui distingue les vétérans des gardes et des malfrats est leur compétence en athlétisme. Celle-ci a plusieurs applications, mais celle qui me frappe le plus est son utilisation pour l'*empoignade* (voir PHB p.195) : les vétérans sont doués pour soumettre un adversaire de manière non létale et pour échapper eux-mêmes aux *empoignades*. Les gardes, en particulier, ne sont pas particulièrement bien équipés pour saisir et retenir quelqu'un, mais les vétérans le sont. De plus, les vétérans peuvent s'enfuir face à une menace écrasante, mais ils ne se figent pas, comme le ferait un roturier.

## CHEVALIERS

Un **chevalier** est un combattant au même titre qu'un vétéran, tout aussi habile au combat de mêlée, un peu moins au combat à distance, avec la réaction *Parade* et l'action *Meneur d'hommes*. L'action *Meneur d'hommes* est similaire à l'action *Cri de guerre* du chef de guerre orc, mais au lieu d'accorder un avantage sur les jets d'attaque, elle accorde un 1d4 supplémentaire - équivalent à un sort de *bénédiction* - sur les jets d'attaque et les jets de sauvegarde de chacun de ses alliés dans un rayon de 10 mètres. Deux autres différences essentielles par rapport au *Cri de guerre* sont (a) que *Meneur d'hommes* ne permet pas au chevalier de faire une attaque au cours du même tour et (b) qu'il dure une minute entière, et non pas seulement un tour. Pour en tirer le meilleur parti, le chevalier utilisera donc l'action *Meneur d'hommes* immédiatement dès qu'un combat est imminent ou déjà commencé ; après cela, le chevalier utilise exclusivement l'action Attaque. Le chevalier n'a aucune bonne raison de renoncer à l'utilisation de cette fonction, même au prix de sa propre action d'Attaque pour un round, car ses effets sont durables et proportionnels au nombre de troupes qu'il commande.

Quant à la réaction *Parade*, un chevalier l'utilisera contre celui de ses adversaires de mêlée (s'il en a plus d'un) qui fait le plus de dégâts moyens par coup. Le chevalier est suffisamment familier avec une variété d'armes, et d'ennemis, pour pouvoir en juger avec précision. Par exemple, si un chevalier est attaqué par un paladin de force 16 et un épée longue (1d8 + 3, soit 7,5 pv par coup) et un barbare de force 18 et une grande hache (1d12 + 4, soit 10,5 pv par coup), il sait qu'il doit laisser les coups du paladin toucher et parer ceux du barbare. C'est fonction de l'expérience, pas de l'intelligence.

Un chevalier peut être monté sur un cheval de guerre qui, en plus d'une attaque de mêlée avec ses sabots, possède également la capacité de *Charge écrasante* (voir MM p. 322). Un chevalier monté utilisera toujours cette fonction, si possible, lors de son premier engagement avec un ennemi. Comme le cheval de guerre a une vitesse de base de 18 mètres et que la charge ne nécessite qu'une distance minimale de 6 mètres, un chevalier déjà en plein combat qui vainc un adversaire s'éloignera pour atteindre au moins 6 mètres de sa prochaine cible, en utilisant l'action *Désengagement* si nécessaire, puis reviendra avec la *Charge écrasante*. Il ou elle attaquera ensuite au même tour si le *Désengagement* n'était pas nécessaire ou au tour suivant s'il l'était. [Voir la note dans les commentaires ci-dessous].

## EXTRAITS DES COMMENTAIRES

**Ilias dit :**
23 avril 2018 à 6:07

Avec les règles de combat à cheval de la cinquième édition, le cheval de guerre serait logiquement considéré comme une monture contrôlée et ne pourrait que *se précipiter*, *se désengager* et *esquiver*, non ?

**Keith Ammann dit :**
23 avril 2018 à 7:14

C'est techniquement vrai, mais ce genre de chose va à l'encontre de l'intérêt d'avoir un cheval de guerre, qui est une créature spécialement entraînée pour le combat. Je vois deux failles potentielles ici. Premièrement, le choix de la monture, qu'elle soit contrôlée ou indépendante, appartient au cavalier. Un cheval de guerre est une bête entraînée au combat et sait ce qu'elle doit faire dans une situation de combat, et elle ne défiera son cavalier que si elle est gravement blessée ou effrayée. Par conséquent, on peut supposer que dans la plupart des situations, il fait ce que son cavalier voudrait qu'il fasse. Deuxièmement, il va de soi que, puisqu'un cheval de guerre est spécialement entraîné pour la guerre, tout comme le chevalier, ce dernier devrait pouvoir utiliser un cheval de guerre contrôlé comme arme. Dans ce cas, le cheval de guerre ne peut que *se précipiter*, mais le cavalier peut faire sa propre action d'attaque en utilisant les sabots du cheval et la *Charge écrasante*, qui est une attaque plus dommageable que l'arme de poing du cavalier.

**Ilias dit :**
24 avril 2018 à 9:19

Il s'agit plus d'un grief contre les règles de combat à cheval de la 5e que de toute autre chose, qui ne correspond pas vraiment à la pleine utilité du combat à cheval dans la vie réelle.

D'une part, le fait que le cheval de guerre agisse comme une monture indépendante ouvre des possibilités d'attaque à la fois pour la monture et pour le cavalier, d'autre part, cela ajoute la complication que les montures indépendantes doivent lancer l'initiative séparément et, au moins parfois, à agir de manière inattendue.

Il est vrai que les montures entraînées devraient pouvoir attaquer sur ordre du cavalier, mais la façon dont les règles sont établies semble avoir été entièrement conçue pour empêcher de telles attaques coordonnées.

Notez que le cavalier qui se désengage alors que la monture attaque laisse toujours les deux possibilités d'attaques d'opportunité, puisque la monture ne s'est pas désengagée et que les AdO provoqués par une monture peuvent également choisir le cavalier comme cible. En premier lieu, le cavalier ne provoque pas d'AoO pendant que la monture se déplace parce qu'il est déplacé sans utiliser sa propre vitesse de déplacement, donc une monture qui se déplace après s'être désengagée garde les deux en sécurité. Cependant, ayant utilisé son action pour se désengager, elle ne peut pas attaquer.

Ce n'est pas une critique des tactiques que vous avez présentées, qui donnent aux chevaliers montés une personnalité et une menace distinctes, mais juste une note que vous pourriez avoir à faire quelques règles maisons, comme avoir une initiative du groupe cavalier/montures malgré le fait qu'ils agissent avec les règles de combat monté indépendantes, ajouter une capacité au chevalier qui leur permet d'utiliser l'attaque de piétinement de leur monture à leur tour, ou même apporter des changements aux règles de combat à cheval qui affectent également les joueurs et les autres combattants à cheval, etc. Je pense que c'est un argument en faveur des "décisions sur les règles" pour que les choses restent amusantes.

**Keith Ammann dit :**
24 avril 2018 à 10:08

D'accord sur tous les points.