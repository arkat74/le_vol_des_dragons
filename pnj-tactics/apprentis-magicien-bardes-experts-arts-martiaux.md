---
layout: page
title: Apprentis magiciens, Bardes et Experts en arts martiaux
nav_order: 1
parent: PNJ Tactics
---

# APPRENTIS MAGICIENS, BARDES ET EXPERT EN ARTS MARTIAUX

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Article original : [NPC Tactics: Apprentice Wizards, Bards and Martial Arts Adepts](http://themonstersknow.com/npc-tactics-apprentice-wizards-bards-martial-arts-adepts/)

Les nouveaux PNJ du Guide des monstres de Volo se répartissent en trois catégories : les futurs boss ennemis ou lieutenants de boss (l'archiduc, la garde noire, le champion, le prêtre kraken, le prêtre de guerre et le chef de guerre), les spécialistes de l'utilisation de la magie (l'abjureur, l'illusionniste, le devin, l'enchanteur, l'évocateur, l'illusionniste, le nécromancien, le transmutateur et trois variantes de sorciers) et les "autres" (l'apprenti sorcier, le barde, l'adepte d'arts martiaux, le maître voleur et l'écuyer) **//TODO: vérifier la trad**. L'analyse des utilisateurs de magie nécessite une attention particulière et longue sur leur liste de sorts, c'est pourquoi je vais remettre à plus tard l'étude de ces derniers pour le moment, tout comme l'archdruide, le prêtre kraken et le prêtre de guerre. La garde noire, le champion et le chef de guerre sont pour la plupart des brutes simples. La catégorie "autres" semble plus intéressante, c'est donc par là que je vais commencer.

## APPRENTI MAGICIEN

L'**apprenti magicien** est exactement ce que vous imaginez - et pourquoi vous battez-vous contre cette adorable mais dangereuse marionnette, de toute façon ? Assaillir Poudlard, c'est ce que font les méchants, et ce n'est pas votre personnage joueur, n'est-ce pas ? Eh bien, l'apprenti sorcier peut être de n'importe quel alignement, donc rien ne dit qu'il ou elle ne peut pas être l'apprenti(e) d'un magicien maléfique - ou peut-être l'une des tyrans, des "méchantes filles" ou des mécontents de sa communauté de sorciers. Mais il est plus probable que l'apprenti magicien se batte aux côtés de vos PJs.

Avec seulement 9 pv et des capacités physiques peu remarquables, l'apprenti magicien ne devrait pas se battre en première ligne de n'importe quelle bataille, mais plutôt se mettre à l'abri quelque part. En raison de sa dextérité moyenne, l'apprenti magicien doit éviter les attaques à distance ainsi que la mêlée ; le poignard dans la ceinture de l'apprenti est en grande partie ornemental. S'il en est capable, l'apprenti magicien attaque avec des sorts, et sinon, il court et se cache.

La liste de sorts de l'apprenti magicien est ultra basique, et il ne dispose que de deux emplacements de sorts de premier niveau : un pour *Mains brûlantes* et un pour *Bouclier* (l'apprenti magicien se déguise principalement pour paraître plus âgé, plus grand et plus séduisant). Tant que l'apprenti magicien est à l'abri, il ou elle se colle avec le Tour de magie *Trait de feu*, qui a une portée de 36 mètres, fait 1d10 de dégâts de feu et enflamme la plupart des objets inflammables. En revanche, les mains en feu n'atteignent que 4,5 mètres, et l'apprenti magicien ne le lancera donc que comme mesure d'autodéfense, que lorsqu'un ennemi se trouvera à sa portée. Il ou elle lancera *Bouclier* en réaction à la première fois qu'il ou elle est touché(e) par une attaque à l'arme.

Lorsqu'il est gravement blessé (réduit à 3 pv ou moins), l'apprenti magicien s'éloigne en *Se précipitant* (action). Malgré sa grande intelligence, l'apprenti sorcier n'a pas l'entraînement martial nécessaire pour savoir comment se Désengager, et la ou les attaques d'opportunité que *Se précipiter* pourrait provoquer risquent de mal se terminer pour notre petit copain.

## BARDE

Le **barde**, qui possède une grande dextérité, une constitution légèrement supérieure à la moyenne et une force moyenne, peut être un tirailleur si nécessaire, mais il est préférable de tirer à distance. Même "sniper" est un terme trop restrictif, car comme les PJs de barde, le PNJ de barde est essentiellement un contrôleur, manipulant la bataille au profit de ses alliés.

Le *Chant reposant* ne s'applique pas pendant une rencontre de combat (encore une fois, c'est le genre de chose qui entrera en jeu si le PNJ barde accompagne la troupe de PJs), mais *Raillerie* le fait. Il s'agit d'une action bonus, ce qui signifie qu'elle peut compléter la stratégie d'action du barde dans n'importe quel round, à l'exception de celui où il lance un *Mot de guérison*. De plus, bien qu'elle soit limitée à deux utilisations par jour, *Raillerie* n'est pas un sort et ne consomme pas d'emplacement de sorts. La rencontre de combat typique dure trois rounds, et il n'y a aucune raison de ne pas utiliser *Raillerie* lors d'un round où le barde ne lance pas de *Mot de guérison* à la place. (Au troisième round, si le barde a déjà utilisé *Raillerie* deux fois, soit ses alliés sont en train de se déchaîner, soit il lance définitivement un *Mot de guérison* sur quelqu'un).

Qui le barde devrait-il railler ? En pensant défensivement, tout ennemi qui vient chercher un allié moins bien équipé pour porter un coup. Ou bien, tout ennemi dont le barde veut saper le jet de sauvegarde.

En prenant l'idée du barde comme contrôleur de combat, comment peut-il y parvenir ?

* *Invisibilité* a de nombreux usages, mais le plus grand coup vient sûrement du fait de le lancer sur un roublard qui peut alors effectuer une *Attaque sournoise*. S'il y a un roublard parmi les alliés du barde, le barde le lancera au premier round de combat, puis le roublard partira pour frapper une cible de grande importance. S'il a le temps de revenir vers le barde, il peut même réussir cette tactique une deuxième fois.
* *Briser* est un sort qui brise les foule, optimal contre deux ou plusieurs ennemis regroupés, en particulier les petits êtres malingre comme les gobelins ou les kobolds, ou ceux qui sont faits de pierre, de cristal ou de métal, comme les gargouilles. Veillez à ne pas prendre les alliés du barde dans la zone d'effet.
* *Charme-personne* est plus efficace contre un ennemi sans alliés et qui n'a pas encore été attaqué par le barde ou l'un de ses alliés. Il peut également être utilisée pour éliminer un ennemi clé de la bataille. Mais le fait qu'on puisse y résister avec un jet de sauvegarde de Sagesse signifie qu'il ne sera pas aussi efficace contre les lanceurs de sorts, qui (à l'exception des Ensorceleur) ont tendance à avoir de bons scores de Sagesse. Si la cible réussit son jet de sauvegarde , c'est une action précieuse qui est perdue, il est donc utile pour le barde de savoir quelque chose sur la cible spécifique ou sur l'espèce de cette cible en général. Une autre option consiste à se railler (action bonus) pour désavantager une cible lors des jets de sauvegarde, puis à lancer un Charme-personne (action) contre cette cible raillée. Cette technique est connue sous le nom de "negging".
* *Mot de guérison* est une action bonus, et une solution évidente chaque fois qu'un allié est modérément ou gravement blessé. Le barde le fera-t-il passer au 2e niveau ? Seulement s'il n'a aucune raison de garder ses sorts de deuxième niveau pour *Invisibilité* ou *Briser*, par exemple s'il n'a pas d'allié roublard et combat un petit nombre d'ennemis plus forts et plus éloignés les uns des autres plutôt qu'un essaim d'ennemis de bas niveau. Gardez à l'esprit que si le barde lance un *Mot de guérison* comme action bonus, il ou elle ne peut pas lancer un autre sort de niveau comme action principale.
* *Héroïsme* est un atout pour un allié qui risque d'être la cible d'attaques ennemies. Mais il exige de la concentration, donc il ne peut pas être soutenu en même temps que *Invisibilité*. Si le barde a un allié roublard, il préférera *Invisibilité*, sinon il lancera *Héroïsme* au premier round de combat.
* *Sommeil* est un autre sort qui brise les foule, à utiliser contre des essaims d'ennemis faibles (il n'est en fait utile que contre les ennemis faibles), et mieux que *Briser* si vous essayez d'être sournois. Ne le renforcez que dans les circonstances où vous renforceriez également *Mot de guérison*.
* *Vague-tonnante* est le sort "Dégage, mec, je suis un rhapsodiste", utilisé pour la self-défense contre une attaque soudaine en mêlée. Ce n'est pas le genre de sort qu'un barde va lancer en plein milieu d'un groupe d'ennemis. Le barde ne va pas non plus le lancer au 2ème niveau, sauf dans les circonstances où il ferait de même avec *Mot de guérison*.
* *Amis* est un Tour de magie de manipulation grossière et rude qui s'applique principalement à des situations d'interaction sociale, et non de combat.
* *Main du mage* est bon pour jouer de la harpe que vous avez laissée de l'autre côté de la pièce lorsque vous ne voulez pas sortir du lit.
* *Moquerie cruelle* est un sort utile pour le barde, pour deux raisons. Premièrement, étant un Tour de magie, il peut être lancé au même tour que *Mot de guérison*. Deuxièmement, il inclut une partie de l'effet de *Raillerie*, donnant à la cible un désavantage sur son prochain jet d'attaque (mais pas sur le test de compétence ou le jet de sauvegarde), et infligeant également un montant nominal de dommages psychiques, si la cible échoue à son jet de sauvegarde de Sagesse. Cependant, le barde devrait probablement lancer ce Tour de magie uniquement pour atténuer les attaques d'un attaquant ennemi, car il n'a aucun effet si la cible réussit son jet de sauvegarde, et il fait moins de dégâts qu'un arc court. Bien qu'il semble redondant de combiner *Raillerie* avec *Moquerie cruelle* - et si *Raillerie* réussissait, cela le serait - *Moquerie cruelle* est une solution de secours décente si *Raillerie* échoue, car elle cible une capacité différente (Sagesse au lieu de Charisme). Mais le barde ne devrait le faire que pour la raison mentionnée ci-dessus, et seulement s'il n'y a pas de meilleur sort à lancer.

Si le barde dispose d'un sort adapté à la situation de combat, il le lancera. Ce n'est que s'il n'y a pas de sort approprié à la situation que le barde aura recours à une attaque à l'arme à distance à la place, ciblant celui qui semble faire le plus de dégâts ou causer le plus de problèmes. La plupart des bardes sont assez malins pour savoir que le fait de tirer sur un lanceur de sort qui est en train d'incanter risque de le déconcentrer.

Un barde se repliera - et demandera à ses alliés de faire de même - lorsqu'il sera gravement blessé (réduit à 17 pv ou moins). Un barde du Collège de la Vaillance saurait probablement comment se *Désengager*, mais un barde du Collège du Savoir ne le ferait probablement pas - et *Raillerie* semble être une nouvelle appellation de la fonction *Mots cinglants* du Collège du Savoir. Quoi qu'il en soit, le barde est un homme d'équipe (en général) et ne s'enfuira pas si ses alliés ne peuvent pas le suivre, mais il s'éloignera plutôt à toute vitesse et esquivera (action), lancera des sorts ou tirera des flèches pendant sa retraite.

## EXPERT DES ARTS MARTIAUX

De même, l'Expert des arts martiaux est de facto un moine de niveau 5 de la Voie de la main ouverte, avec 11 dé de vie - soit plus du double de son niveau apparent - sans la fonction de classe *Chute ralentie*, et sans les fonctions de ki *Défense patiente* ou *Déplacement aérien*, mais aussi avec un dé de dommage de d8 au lieu de d6. Le profil de score de caractéristiques de l'adepte suggère un tireur d'élite, mais les caractéristiques de ce PNJ sont clairement orientées vers le combat en mêlée ; c'est peut-être pour cela qu'il ou elle obtient ce dé de vie supplémentaire.

Le principal choix que nous devons faire avec les Experts en arts martiaux est celui de l'effet "Attaque à mains nues" à utiliser sur chaque coup. Nous pouvons créer une simple méthode heuristique :

* étourdir tout adversaire avec une action d'Attaque supplémentaire, d'Attaque multiple ou d'attaque bonus.
* faire tomber un objet contre tout lanceur de sorts tenant un focaliseur arcanique ou druidique ou un symbole sacré, tout adversaire utilisant un objet magique, ou un barde jouant d'un instrument de musique.
*  faire tomber à terre à la première ou à la deuxième attaque dans une attaque multiple (jamais comme la troisième - l'adversaire se relève simplement à son tour).

Si un opposant remplit plus d'un de ces critères, utilisez les options dans l'ordre ci-dessus, selon le cas. Si un ennemi est déjà étourdi, il n'est pas nécessaire de l'étourdir. Si l'adversaire effectue son jet de sauvegarde contre un type d'attaque, l'adepte peut juger si l'adversaire a été chanceux ou simplement bon, et utiliser cette information pour décider s'il doit réessayer ou passer à l'effet applicable suivant.

Par exemple, disons que l'expert se bat contre l'ensorceleur Epirotes, qui est une fleur fragile, avec des modificateurs de 0, +1 et +1 pour la Force, Dex et Con. À la première attaque de l'expert, il frappe et tente de faire tomber la baguette d'Epirotes. Epirotes effectue son jet de sauvetage, mais l'adepte sent que c'est un coup de chance : le sorcier est frêle et il n'a que 40 % de chances de maintenir sa prise. Lors de la deuxième attaque, l'expert rate son coup - son plan était d'essayer de faire tomber Epirotes à terre, car cet effet n'aurait plus de sens lors de la troisième attaque, mais il a raté sa chance. Ainsi, lors de la troisième attaque, lorsque l'expert frappe, il tente à nouveau de frapper la baguette de la main de l'Epirotes.

Supposons plutôt qu'il combatte Eofn, un combattant fougueux avec une arme d'hast. Ses modificateurs de sauvegarde de For, Dex et Con sont +4, +1 et +4. Il sait qu'elle résistera probablement à toute tentative de l'étourdir, mais comme elle a une Attaque supplémentaire, il doit essayer. Sa première attaque est réussie, et elle réussit son lancer de sauvetage. Inutile de réessayer maintenant ; la prochaine chose qu'il fera sera d'essayer de la mettre en position couchée. Sa deuxième attaque est réussie, et elle échoue à son jet de sauvegarde de Dex : elle est au sol. Il fait sa troisième attaque avec l'avantage, frappant facilement, et quoi qu'il en soit, il peut aussi bien essayer de l'assommer à nouveau, puisqu'il ne peut pas la faire tomber encore à terre, et s'il la désarme, elle va juste prendre son arme quand elle se relèvera. Elle réussit à nouveau son jet de sauvegarde. Oh, eh bien, rien n'est jamais acquis, rien n'est jamais gagné. En attendant, il est à noter qu'il lui a fait 3d8 + 9 de dégâts, ce qui n'est pas trop mal pour un attaquant désarmé.

Bien sûr, l'expert en arts martiaux ne doit pas se limiter à combattre un seul adversaire. Si plusieurs ennemis ont engagé l'expert dans une mêlée, il ou elle peut porter des coups à l'un ou à l'ensemble d'entre eux, en choisissant des effets supplémentaires adaptés aux cibles. L'adepte donne la même priorité aux cibles qu'aux effets, en attaquant d'abord les adversaires qui ont une stratégie d'action solide, puis les attaquants qui tiennent des focaliseur ou des objets magiques, et enfin tous ceux qui sont encore debout.

L'adepte des arts martiaux bat en retraite en cas de blessure grave (réduite à 24 pv ou moins), en utilisant l'action *Se désangager* pour assurer une fuite propre, grâce à son entraînement avancé et à sa vitesse exceptionnelle.
