---
layout: page
title: Observateur
nav_order: 11
parent: PNJ Tactics
---

# OBSERVATEUR

Article original : [Beholder-Kin Tactics - Gazer](https://www.themonstersknow.com/beholder-kin-tactics/)

Les observateurs sont de minuscules tyrannoeil que l'on trouve généralement en groupe autour d'un tyrannoeil de taille normale, bien qu'on les trouve parfois seul. Ils maîtrisent la compétence Discrétion et peuvent donc se cacher dans les recoins les plus sombres de la tanière de leur maître. Ils ont peu de points de vie et une Force très limitée, donc ils restent aussi loin que possible de leurs ennemis (jusqu'à la portée maximale de leurs *Rayon oculaire*, soit 18 mètres) et tirent leur rayon depuis les ombres. S'ils sont approchés, ils s'éloigneront à une distance sûre, en utilisant l'action *Se précipiter* si nécessaire, bien qu'en général ils devraient pouvoir battre en retraite, puis utiliser à nouveau leurs *Rayon oculaire*. Ils ne sont pas assez intelligents pour *Se désengager* ou même *Esquiver*. Ils ont la capacité *Agressif*, mais étant donné qu'ils sont totalement inadaptés au combat au corps à corps, ils ne l'utiliseront que s'ils doivent s'approcher à moins de 18 mètres d'une cible. Ils fuiront absolument, en utilisant l'action *Se précipiter*, s'ils sont réduits à 5 points de vie ou moins.
