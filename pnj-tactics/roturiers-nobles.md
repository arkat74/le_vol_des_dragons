---
layout: page
title: Roturiers et Nobles
nav_order: 6
parent: PNJ Tactics
---

# ROTURIERS ET NOBLES

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## ROTURIERS

Les ennemis que les personnages des joueurs rencontrent dans Donjons & Dragons ne sont pas tous des monstres. Beaucoup d'entre eux sont simplement des gens : villageois, citadins, nomades, vagabonds, ermites. Ils sont ce que nous appelons des "personnages non-joueurs".

Dans la cinquième édition du Manuel des Monstres, le modèle de base pour un PNJ est le roturier. Le roturier a des attributs moyens, aucune compétence ou caractéristique particulière, et aucune attaque avec une arme, à l'exception d'un gourdin, qui est interchangeable avec toute arme improvisée.

La psychologue allemande Karen Horney (qui rime avec "horsefly", et non "corny") a observé trois tendances dans le comportement des gens : se rapprocher des autres, s'opposer aux autres et s'éloigner des autres. Elle a ensuite appelé ces tendances "conformité", "agression" et "détachement". Dans toute personnalité donnée, l'une d'entre elles sera probablement plus forte que les deux autres. Ainsi, un roturier jeté dans une situation de conflit peut réagir de trois façons :

* **Combattre**. Ce personnage attaquera par réflexe un ennemi perçu comme tel. L'attaque ne sera pas sophistiquée. Le PNJ s'emparera de l'arme la plus proche (improvisée, si nécessaire), se déplacera vers son adversaire et attaquera (action) jusqu'à ce que l'ennemi soit vaincu ou que le PNJ soit gravement blessé (réduit à 1 pv) ou assommé. Certains roturiers - par exemple, un chasseur - savent utiliser une simple arme à distance, auquel cas ils attaquent sans se déplacer vers l'adversaire, mais poursuivent de façon limitée un adversaire qui tente de s'échapper.
* **Fuir**. Ce personnage va se retourner et s'enfuir. N'ayant pas reçu l'entraînement nécessaire pour savoir comment se désengager, il esquivera (action) lorsqu'il est à la portée de son adversaire, Se précipitera (action) dans le cas contraire et, dans les deux cas, se déplacera à toute vitesse vers l'endroit le plus proche où il se sent en sécurité.
* **Se figer**. Dans la vie réelle, cette réaction au danger est étonnamment courante. Le personnage ne se bat pas et ne fuit pas, mais reste enraciné, paralysé par la peur. Si le PNJ peut rassembler ses esprits (par exemple, en faisant un test de Sagesse contre un DD égal à 10 plus l'indicateur de dangerosité de l'ennemi multipliée par le multiplicateur de rencontre approprié de la page 82 du Guide du maître), il formulera les mots nécessaires pour se rendre.

Pourquoi vous attaquez les roturiers de toute façon, vilains PJs ? Eh bien, un MD peut avoir de bonnes raisons d'inclure des roturiers dans une rencontre autre que le comportement malfaisant des PJs. Peut-être que les roturiers sont attaqués par un monstre et doivent être sauvés ? Peut-être ont-ils été charmés par un ennemi plus puissant ? Peut-être que les PJs ont été charmés ou déguisés par magie pour apparaître comme une menace ? Peut-être que les roturiers sont xénophobes et que les PJs leur sont étrangers ? Peut-être sont-ils mêlés à une querelle avec d'autres roturiers ?

Les PNJ non humains peuvent être poussés vers un type de comportement particulier en fonction de leurs modificateurs de caractéristiques. Par exemple, un roturier nain des montagnes aura un point de Constitution supplémentaire, un point de Force supplémentaire et un point de moins dans tout le reste (pourquoi pas deux points supplémentaires de Constitution et de Force ? Parce que les humains sont la base, et qu'ils obtiennent un point de plus pour tout. Donc, à moins que vous n'alliez déclarer que les roturiers humains ont en fait 11 dans toutes leurs scores de caractéristiques plutôt que 10, vous devez déduire ce "bonus de polyvalence" humain des statistiques du roturier avant d'appliquer les modificateurs des autres races). Cela les pousse vers la catégorie "brute" et les rend plus susceptibles de se battre en combat rapproché ; en outre, leur vitesse lente suggère qu'ils seront moins susceptibles de fuir. Leur entraînement au combat suggère que même les nains des montagnes non entraînés manieront au moins des haches (leur arme de mêlée de choix) plutôt que des massues. Voici les tendances pour les autres races et sous-races :

* **nains des collines** : 11 Con, 11 Sag, tout le reste à 9. Ils cherchent la sécurité dans le nombre et choisissent leurs batailles avec soin, en évitant de se battre à moins d'avoir l'avantage. Ils manient des hachettes. Les nains des collines sont également lents et ont donc moins de chances de s'enfuir une fois que le combat a commencé.
* **hauts-elfes** : 11 Dex, 10 Int, tout le reste à 9, plus des Tour de magie. Ils recherchent la sécurité dans le nombre et tirent à distance, en utilisant non seulement des arcs mais aussi des Tours de magie faisant des dommages.
* **elfes sylvestres** : 11 Dex, 10 Sag, tout le reste à 9, plus *Cachette naturelle*. Ils recherchent la sécurité dans le nombre, attaquent en se cachant, espérant obtenir l'élément de surprise par le camouflage, et tirent à distance, à l'aide d'arcs courts. Comme ils ont *Pied léger*, ils ont plus de chances de fuir si la tendance du combat tourne un tant soit peu à leur encontre. Avec un peu de chance, ils auront plus de chances de se cacher et de tirer à distance.
* **elfes noirs** : 11 Dex, 10 Cha, tout le reste à 9, plus *Sensibilité à la lumière du soleil* et *Vision dans le noir supérieure*. Ils recherchent la sécurité dans le nombre, tirent à distance, utilisent des arbalètes à main et sont des créatures nocturnes et/ou souterrainnes.
* **halfelins pieds-légers** : 11 Dex, 10 Cha, tout le reste à 9, plus *Brave*. Ils recherchent la sécurité dans le nombre et tirent à bout portant avec de simples armes à distance. Mais ils sont aussi courageux, plus enclins à se battre qu'à se figer et peu enclins à fuir à cause de leur faible vitesse.
* **halfelins robustes** : 11 Dex, 10 Con, tout le reste à 9, plus *Brave*. Ils recherchent la sécurité dans le nombre et tirent à bout portant avec de simples armes à distance. Ils sont également plus enclins à se battre et moins susceptibles de s'enfuir, et ils seront des combattants combatifs s'ils sont engagés dans une mêlée et s'ils ont le nombre nécessaire pour se battre.
* **Sangdragons** : 11 For, 10 Cha, tout le reste à 9, plus *Souffle*. Ils choisissent leurs combats avec soin, évitant de se battre à moins d'en avoir l'avantage, et attaquent en se cachant, en attaquant avec leur *Souffle* et en poursuivant avec des attaques de mêlée à l'aide de massues ou d'autres armes improvisées. Malgré leurs origines, ils ont plus tendance à fuir qu'à se battre ou à se figer.
* **gnomes des forêts** : 11 Int, 10 Dex, tout le reste à 9, plus une illusion mineure. Ils cherchent à éviter tout combat et utilisent *Communication avec les petits animaux* pour employer des créatures de la forêt comme sentinelles. Les gnomes des forêts qui fuient utilisent des illusions pour couvrir leur fuite. Les gnomes des forêts qui se figent utilisent des illusions pour essayer de se déguiser en rochers, buissons, souches d'arbres ou autres.
* **gnomes des roches** : 11 Int, 10 Con, tout le reste)  9, plus *Bricoleur*. Ils cherchent à éviter tout combat et utilisent des pièges et des alarmes pour les avertir des intrusions et pour repousser ou neutraliser le ou les intrus.
* **demi-elfes** : 11 Cha, deux autres capacités valent 10, tout le reste à 9. Pas de tendance particulière ; traitez comme un humain.
* **demi-orcs** : 11 For, 10 Con, tout le reste à 9, plus *Acharnement* et *Sauvagerie*. Ils se battent presque toujours, fuient ou se figent rarement. Ils préfèrent le combat en mêlée, avec des massues ou des armes improvisées.
* **tieffelins** : 11 Cha, 10 Int, tout le reste à 9, plus *Ascendance infernale*. Manquant de force et de nombre, ils fuient le danger et utilisent la thaumaturgie pour tenter d'effrayer les créatures qui représentent une menace.
* **aasimars** : 11 Cha, 10 Sag, tout le reste à 9. Manquant à la fois de force et de nombre, ils fuient le danger.

## NOBLES

Le bloc de statistiques du **noble** reflète le fait que même l'aristocrate le plus inutile a néanmoins une longueur d'avance sur un roturier typique en raison de son éducation, sans parler de l'accès à une meilleure nutrition. Il suggère une certaine formation au maniement de l'épée ainsi qu'un raffinement courtois et l'accès à des livres de philosophie et de fiction. Ces trois statistiques physiques sont supérieures à celles d'un roturier, bien qu'elles ne soient pas particulièrement élevées dans le grand ordre des choses ; cela suggère qu'un noble sera courageux de manière sélective. En d'autres termes, un noble n'a pas peur d'un roturier et peut considérer qu'il a le droit de donner une raclée au misérable, mais face à un hibours, par exemple, il se souviendra rapidement de la maxime selon laquelle la discrétion est la meilleure partie de la valeur. Avant qu'un noble ne se batte, ne s'enfuie ou ne se fige face à un adversaire intelligent, il ou elle tentera de négocier - et aura une bonne idée de sa position de force ou de faiblesse. Le noble porte et maitrise la rapière, mais il sait quand il est surclassé et sait quand et comment se désengager (action) et battre en retraite. Le noble a un sens très aigu de l'auto-préservation et il lui suffira d'une blessure modérée (réduite à 6 pv ou moins) pour battre en retraite ou se rendre. Il ou elle ne continuera alors à se battre que si on ne lui donne pas de quartier et aucune possibilité de s'échapper. Le rôle de l'honneur dans les décisions de vie ou de mort d'un noble est très largement surestimé.
