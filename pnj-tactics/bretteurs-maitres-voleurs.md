---
layout: page
title: Bretteurs et Maîtres Voleurs
nav_order: 3
parent: PNJ Tactics
---

# BRETTEURS ET MAÎTRES VOLEURS

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Aujourd'hui, je regarde deux PNJ malhonnêtes dans le Guide des monstres de Volo, l'un tape-à-l'oeil, l'autre furtif : le bretteur et le maître voleur.

## BRETTEUR

Le **bretteur** de Volo ne ressemble pas beaucoup à l'archétype du **bretteur** dans le Guide de l'aventurier de la Côte des épées. Au lieu de cela, il possède une caractéristique passive, *Défense élégante*, qui augmente sa classe d'armure et une caractéristique d'amélioration de la stratégie d'action, *Pied léger*, qui lui accorde soit un une action bonus *Se précipiter* soit *Se désengager* (il s'agit en fait d'une version légèrement améliorée de *Ruse*, qui permet en plus à l'utilisateur de *Se cacher*).

Le bretteur se distingue par une Dextérité exceptionnellement élevée, une expertise en Acrobatie, en Athlétisme et en Persuasion, ainsi qu'une triple *Attaques multiples* à la florentine. La Dextérité suggère un tireur d'élite, mais les attaques du bretteur sont axées sur le combat au corps à corps (la dague peut être utilisé comme une arme à distance, mais un bretteur qui le fait perd les deux tiers de son *Attaques multiples*). Comme sa Force et sa Constitution ne sont que légèrement supérieures à la moyenne, nous devons imaginer un style de combat qui permette d'une manière ou d'une autre à l'épée de frapper au corps à corps tout en évitant de se faire toucher aux tours de ses adversaires. Comment y parvenir, sachant que le bretteur n'a qu'une vitesse de déplacement normale de 9 mètres ?

Dans ma méta-analyse "[Esquiver, Se précipiter ou Se désengager ?](http://themonstersknow.com/dodge-dash-disengage/)", j'ai examiné les trois actions qu'un combattant peut utiliser pour se retirer d'un combat. Le problème pour le bretteur, dont la classe d'armure est suffisamment élevée pour faire de *Esquiver* le choix par excellence, est que *Pied léger* n'accorde pas une action bonus pour *Esquiver*. Il ou elle doit choisir entre *Se précipiter* et *Se désengager*. D'un autre côté, le bretteur ne fuit pas la plupart du temps, mais essaie simplement de minimiser la capacité de l'adversaire à infliger des dégâts.

Il y a deux cas possibles : soit le combat commence avec le bretteur et son adversaire à portée de combat au corps à corps, soit il commence avec eux hors de portée. S'ils sont à portée de combat au corps à corps, alors le bretteur peut utiliser son action *Attaques multiples*, puis soit *Se précipiter* ou *Se désengager* (action bonus), puis se déplacer. S'ils sont hors de portée de combat au corps à corps, le bretteur doit utiliser une partie ou la totalité de son mouvement (déplacement), *Attaques multiples* (action), puis *Se précipiter* ou *Se désengager*  (action bonus), puis utiliser tout mouvement restant.

Précisons que l'adversaire du bretteur a aussi une vitesse de base de 9 mètres. Cela signifie que pour empêcher l'adversaire de riposter, le bretteur doit s'éloigner de plus de 9 mètres. Le plus grand mouvement possible du bretteur en un tour est de 18 mètres, ce qui nécessite l'action *Se précipiter*. Si le bretteur se trouve à 10 mètres de son adversaire, il doit à la fois *Se précipiter* et se déplacer pour se mettre à portée de mêlée, puis il ne lui reste que 8 mètres de mouvement, ce qui n'est pas suffisant pour le mettre à nouveau hors de portée. Ainsi, un bretteur qui est hors de portée n'a pas beaucoup d'intérêt à se mettre à portée de son adversaire. Il doit plutôt attendre que ses adversaires viennent à lui.

La deuxième difficulté à laquelle le bretteur est confronté est la provocation d'une attaque d'opportunité chaque fois qu'il se déplace hors de portée d'un adversaire, à moins qu'il n'utilise *Se désengager*. Mais *Se désengager* ne donne pas au bretteur assez de mouvement pour rester hors de portée, pour cela, il doit utiliser *Se précipiter*. Cela étant dit, si votre adversaire a une *Attaque supplémentaire* ou une *Attaques multiples*, il est toujours judicieux de risquer une attaque d'opportunité afin d'éviter d'être visé par deux ou trois attaques. Ainsi, contre de tels adversaires, le bretteur doit généralement préférer l'action *Se précipiter* par rapport à *Se désengager*.

Cependant, si le bretteur est engagé dans un combat au corps à corps avec plus de deux adversaires, l'équation est inversée. Elle devient alors fonction du nombre d'attaques d'opportunité auxquelles le bretteur est soumis en se glissant hors de portée, et du nombre de ces adversaires susceptibles de le poursuivre. Si le nombre d'adversaires est supérieur au nombre total d'attaques que les adversaires aussi rapides ou plus rapides que le bretteur peuvent effectuer en tant qu'actions, le bretteur doit *Se désengager*. S'il est égal ou inférieur, le bretteur doit *Se précipiter*. L'expression "tout aussi rapide ou plus rapide" est importante : un bretteur demi-elfe (vitesse : 9 mètres) entouré de halfelins hostiles (vitesse : 7,5 mètres) peut facilement les dépasser et doit *Se désengager* sans hésitation.

Maintenant, tout cela suppose un terrain plat. Mais regardez l'illustration du bretteur de Volo, qui saute dans le gréement du navire (et s'accrochant à une corde d'une main, ce qui va devoir avoir un effet sur son *Attaques multiples*, mais c'est une question secondaire). Si le bretteur peut grimper hors de portée, plutôt que de simplement se barrer, cela ouvre d'autres possibilités. La vitesse de déplacement en escalade n'est que la moitié de la vitesse de déplacement normale (plus précisément, chaque mètre de distance d'escalade coûte un mètre de déplacement supplémentaire - PHB, p. 182), mais d'un autre côté, à moins que les adversaires ne soient à la fois assez athlétiques et enclins à le poursuivre, le bretteur n'a pas besoin d'aller aussi loin pour rester hors de portée : 3 mètres suffisent, au lieu des 10 mètres qu'il devrait normalement parcourir. Appliquez à nouveau l'équation, en comptant cette fois le nombre total d'attaques que les adversaires qui peuvent suivre en escaladant peuvent faire en tant qu'actions, et *Se désengager* peut être un choix plus prometteur qu'il ne le serait autrement.

Les bretteurs doivent toujours utiliser au maximum le terrain pour rendre la poursuite difficile. S'ils peuvent sauter vers le haut, vers le bas ou à travers une caractéristique du terrain que leurs adversaires ne peuvent pas, ils devraient le faire.

Cela étant dit, le fait que le bretteur utilise exclusivement des armes de finesse ajoute une faille à la lecture des caractéristiques de Dextérité haute, de Force moyenne, de Constitution moyenne comme "sniper". Comme la Dextérité, et non la Force, est la capacité d'attaque du bretteur, il ou elle pourrait également être considéré(e) comme un(e) attaquant(e) de choc. Selon cette interprétation, le bretteur ne joue pas la prudence, mais charge plutôt pour porter cette triple *Attaques multiples* aussi vite que possible, puis *Attaques multiples* à nouveau au prochain tour avant de se retirer hors de portée. Ce rythme charge-attaque-attaque-retraite, charge-attaque-attaque-attaque-retraite donne au bretteur de bonnes chances d'infliger des dommages importants tout en minimisant les risques pour lui-même.

Les bretteurs apprécient leurs jolis visages. Si vous pouvez les blesser modérément (réduisez-les à 46 pv ou moins), ils commenceront à chercher une issue de secours et la prendront à la prochaine occasion. Ils continueront également à trottiner tout au long de la bataille, se moquant de l'opposition si elle est plus faible, négociant une alternative au combat si elle est plus forte.

## MAÎTRE VOLEUR

Le **maître voleur** est justement cela : un voleur. Ce n'est pas un assassin. Dans l'idéal, un voleur veut obtenir ce qu'il est venu chercher avec le moins de tracas possible - rapidement et sans traîner. Il a tout intérêt à éviter les combats, ainsi que tout ce qui pourrait entraver une fuite en toute sécurité. Ainsi, le maître voleur ne se bat que pour éliminer cette interférence aussi rapidement et proprement que possible, pour punir un rival ou s'il est acculé.

D'une Dextérité exceptionnelle et d'une grande Constitution, le maître voleur est un bagarreur qui devra rester engagé pendant un certain temps pour achever un adversaire s'il ne peut le faire immédiatement par une *Attaque furtive*. Mais encore une fois, le maître voleur ne veut pas rester engagé dans la mêlée, car c'est une perte de temps. Comme le maître voleur utilise également une arme de finesse (l'épée courte), il peut être utile de le considérer comme un attaquant de choc également. Le maître voleur frappe d'abord et fort avec une *Attaque furtive*. Soit ça élimine son adversaire, soit ça ne le fait pas. Dans les deux cas, le maître voleur n'est pas très enclin à rester dans les parages.

Le maître voleur maîtrise l'Acrobatie, l'Athlétisme et la Furtivité, utilisant cette dernière pour se déplacer sans être détecté et les trois pour s'échapper. *Ruse* permet d'utiliser les actions *Se précipiter*, *Se désengager* et *Se cacher* comme action bonus. À moins d'être entouré d'adversaires, le maître voleur utilisera principalement les actions *Se précipiter* et *Se cacher*.

D'après les caractéristiques du maître voleur, de son modificateur de maîtrise et de ses dégâts Sneak Attack, il s'agit d'un roublard de niveau 7. Tout comme l'apprenti magicien, le barde et l'expert en arts martiaux, le maître voleur a environ deux fois plus de dés de vie que son niveau ne le suggère - dans ce cas précis, c'est un dé de moins. Cela peut s'expliquer en partie par le fait que l'effet combiné de *Dérobade* et de l'*Esquive instinctive* est de doubler la capacité de résistance du maître voleur. Ce PNJ est difficile à attraper et plus difficile à tuer.

Le maître voleur veut toujours être le premier à frapper, idéalement en se cachant, car le moyen le plus simple de prendre l'avantage sur un jet d'attaque (la condition de déclenchement principale de l'*Attaque furtive*) est d'être un attaquant invisible, et idéalement à portée de combat au corps à corps, car l'*Attaque multiple* du maître voleur accorde trois attaques à l'épée courte, alors qu'il ne peut tirer qu'une seule fois à l'arbalète. La *Attaque furtive* s'applique au premier coup ; si les trois attaques touchent, cela fait un total de 7d6 + 4 dommages par coups d'épée, soit environ 28 pv en moyenne.

Cela ne suffit probablement pas pour achever un aventurier de niveau intermédiaire. Le maître voleur doit donc décider rapidement s'il est nécessaire d'achever l'adversaire ou de faire une sortie précipitée. Dans la plupart des cas, le maître voleur se trompe de sortie. Entre un CA suffisamment élevé et la capacité *Esquive instinctive*, le maître voleur peut choisir *Se précipiter* (action bonus) plutôt que *Se désengager* sans crainte, car même s'il est touché par une attaque d'opportunité - ou même par plusieurs attaques d'opportunité - les dégâts seront réduits de moitié.

De plus, il est intéressant de noter que rien dans le Manuel des Joueurs ne précise qu'une action et une action bonus ne peuvent pas être identiques, donc le maître voleur peut utiliser *Se précipiter* à la fois comme action et comme action bonus, triplant ainsi son mouvement ! Par ailleurs, un maître voleur qui se retrouve soudainement encerclé peut utiliser *Se désengager* comme action et *Se précipiter* comme action bonus (ou vice versa), ce qui lui permet de se déplacer de 30 mètres sans être soumis à une seule attaque d'opportunité.

Le maître voleur n'a pas besoin d'être gravement ou même modérément blessé pour s'enfuir : il s'enfuit immédiatement après avoir frappé le premier coup de l'action *Attaque furtive*, sauf s'il prend un tour de plus pour essayer d'achever un adversaire. Cela étant dit, un maître voleur qui est gravement blessé (réduit à 33 pv ou moins) alors qu'il est poursuivi par des adversaires arrêtera de fuir, se rendra à ses poursuivants et tentera de conclure un marché. Il n'y a rien pour lequel le maître voleur est prêt à mourir.
