---
layout: page
title: Duergar
nav_order: 7
parent: PNJ Tactics
---

# DUERGAR

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Les scores de caractéristique des Duergar suivent un profil de brute sans ambiguïté, ses traits de *Résilience* et de *Sensibilité à la lumière du soleil* sont passifs, et il est armé d'armes de corps à corps et à distance ordinaires. En général, leurs "tactiques" consistent à charger, frapper et poignarder. Les seules questions qui se posent sont de savoir quand utiliser *Agrandir* et quand utiliser *Invisibilité*.

*Agrandir* augmente la taille du Duergar, sa force (pas numériquement, mais sous la forme d'un avantage sur les tests de Force et les jets de sauvegarde de Force uniquement, pas sur les jets d'attaque) et ses dégâts, au prix d'une action. Sauf dans le cas d'une *empoignade* (voir PHB p.196), les dégâts sont le seul de ces avantages qui compte vraiment. Comme le temps est si précieux dans la cinquième édition de D&D, nous devons calculer si cet avantage en vaut le coût.

Commençons par les chiffres bruts des dommages, en ne prenant en compte que les coups par souci de simplicité, car la probabilité de réussite d'une attaque est la même quelle que soit l'arme utilisée par le Duergar ou sa taille actuelle. Supposons également que le Duergar utilise son arme la plus puissante, le pic de guerre (vous pouvez faire le calcul avec le javelot ; la conclusion sera la même).

Un coup avec un pic de guerre de Duergar de taille normale fait 1d8 + 2 dégâts, soit 6,5 en moyenne, que je n'arrondis pas dans ce cas, car les demi-points vont s'accumuler au fil du temps. Un Duergar agrandi inflige 2d8 + 2 points de dégâts, soit 11 points de dégâts en moyenne.

Voici comment elle se décompose sur la durée d'une rencontre de combat typique (en arrondissant les dommages accumulés à la moitié de la distance) :

| Rounds | Normal | Agrandi |
| :----: | :----: | :-----: |
| 1      | 6      | -       |
| 2      | 13     | 11      |
| 3      | 20     | 22      |
| 4      | 26     | 33      |
| 5      | 32     | 44      |

La cinquième édition de D&D part du principe que la durée moyenne d'une rencontre de combat est de trois rounds, deux à cinq étant typiques. Nous voyons ici que trois rounds est le seuil de rentabilité approximatif, en dessous duquel l'agrandissement n'offre qu'un faible avantage. Si un combat doit durer plus de trois rounds, l'avantage de l'agrandissement est substantiel. Si elle doit se terminer rapidement, l'agrandissement se fait au détriment du Duergar.

Comment savoir combien de temps une bataille va durer ? Nous ne le savons pas - et le Duergar non plus. Nous ne pouvons faire que des suppositions éclairées basées sur ce que nous savons de la construction de la rencontre, et le Duergar ne peut utiliser que son intuition façonnée par l'expérience.

Examinons ces possibilités :
* une rencontre Facile sera rapidement et définitivement en faveur des PJs.
* une rencontre Moyenne durera de deux à quatre rounds, avec seulement des dommages légers à modérés pour les PJs.
* une rencontre Difficile durera de trois à cinq rounds, et les PJs peuvent subir des dommages modérés à graves.
* une rencontre Mortelle peut durer de trois à cinq rounds, certains PJs prenant de graves, voire mortels, dommages ou les PJs peuvent être rapidement détruits. Nous allons devoir faire la distinction entre "Mortel" et "Super-Mortel".

Dans une rencontre Facile pour les PJs, le Duergar ne tire aucun bénéfice de l'agrandissement - et n'a également aucun espoir de gagner. C'est là que le Duergar se rabat sur l'*Invisibilité*. Un Duergar n'a qu'une Sagesse moyenne, mais cela suffit pour savoir quand il est gravement dépassé et doit s'enfuir.

Il est plus difficile de juger d'une rencontre Moyenne, mais il n'y a encore qu'une chance sur deux que l'agrandissement soit d'une aide significative pour le Duergar. Dans ce cas, il peut reconnaître immédiatement sa situation délicate, utiliser l'Invisibilité et s'enfuir ; sinon, il utilisera *Agrandir*, puis réalisera son erreur, utilisera l'*Invisibilité* et s'enfuira.

Une rencontre Difficile ou Mortelle (et non Super-Mortelle) risque de s'éterniser et, dans ce cas, l'avantage de l'agrandissement est incontestable. Le Duergar utilise cette fonction au premier tour de cette rencontre - plus tôt, s'il le peut, mais ne trichez pas et ne lancez pas un Duergar déjà Agrandi sur vos PJs si le Duergar n'a aucune raison de s'attendre à un combat. Un Duergar ne devient *Invisible* et ne s'enfuit que s'il est gravement blessé (réduit à 10 pv ou moins).

Une rencontre Super-Mortelle se terminera probablement assez rapidement, et le Duergar est favorisé pour la gagner. Dans ce cas, il n'a pas besoin de s'enfuir, et il n'a pas besoin d'un avantage plus important que celui dont il dispose déjà. Il se battra donc à sa taille normale.

Je parle ici d'un Duergar unique, mais les mêmes règles s'appliquent s'ils attaquent (ou défendent) à plusieurs. Utilisez les règles de "Création de rencontres" du chapitre 3 du  Dungeon Master’s Guide (voir DMG p.81) pour déterminer la difficulté de chaque rencontre et la réaction de vos Duergars à celle-ci, qui peut différer d'une rencontre à l'autre.

Mais attendez, vous vous demandez peut-être si la difficulté de combattre un Duergar n'est pas différente selon qu'il s'agit d'un Duergar de taille normale ou d'un Duergar agrandi ? Cela n'affectera-t-il pas l'indice de dangerosité, et donc la difficulté de la rencontre ?

Pas vraiment. La raison en est que le Duergar ne se bat qu'à taille normale lorsqu'il a un un avantage écrasant. Sinon, s'il doit rester dans les parages et se battre, il le fait Agrandi, et son ID prend en compte les dégâts causés par l'Agrandissement.

Quoi qu'il en soit, en résumé, vous n'avez pas à penser à la tactique du Duergar à la volée. Il vous suffit de déterminer l'Indice de dangerosité de chaque rencontre avec des Duergars au préalable, afin de savoir comment le Duergar va réagir aux PJs : disparaître et s'enfuir, s'agrandir et charger, ou simplement charger.
