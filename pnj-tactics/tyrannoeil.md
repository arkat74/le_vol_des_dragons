---
layout: page
title: Tyrannoeil
nav_order: 9
parent: PNJ Tactics
---

# FAMILLE DES TYRANNOEILS

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Notre groupe actuel de Donjons et Dragons s'est réuni après que l'un des collègues de ma femme ait qualifié un client de "quelqu'un qui a l'air de jouer à Donjons et Dragons dans la cave de sa mère", et qu'un autre ait rétorqué : "Je jouerais tout à fait à Donjons et Dragons". Il a fini par être l'hôte de nos sessions hebdomadaires.

Pourquoi est-ce que je mentionne cela ? Parce que le tyrannoeil est un monstre D&D si emblématique que notre hôte - qui ne connaissait presque rien au jeu avant que nous ne commencions à jouer - m'a dit vers le début de notre campagne : "Tout ce que je veux, c'est tomber sur un "oeil du tyrannoeil" et je serai heureux".

## TYRANNOEIL

Le **tyrannoeil** est une aberration - une créature d'origine extraplanaire invoquée par magie - au tempérament haineux, avare et territorial. Elle n'a guère d'autre but dans la vie que de protéger le territoire qu'elle a choisi. Bien qu'elle ne soit pas forte, elle possède de puissantes capacités mentales ainsi qu'une grande Dextérité et une très haute Constitution, ce qui la protège contre tous les "trois grands" types de jets de sauvegarde. Comme on peut s'y attendre de la part d'une masse flottante avec un œil central géant, sa capacité de perception est exceptionnelle ; elle a également une vision dans le noir jusqu'à 36 mètres. Il a une capacité innée à planer, de sorte qu'il ne peut jamais être mis dans l'état *à terre*.

Au corps à corps, il a une attaque *Morsure*, mais l'atout du tyrannoeil est ses rayons oculaires, qui émanent des nombreux petits yeux au bout des tiges s'étendant à partir de son corps. Ces rayons ont une portée de 36 mètres, suffisante pour tenir les intrus à distance pendant deux à cinq tours de combat. Il peut également projeter un *Cône antimagie* à partir de son œil central, mais cette capacité est problématique, comme nous le verrons dans un instant.

Enfin, un tyrannoeil dans son antre a accès à trois actions d'antre : de la bave glissante sur le sol, des appendices sortant des murs et des yeux de tyrannoeil qui apparaissent au hasard sur les surfaces voisines. Et c'est presque toujours dans son antre que le tyrannoeil se retrouve.

Le tyrannoeil est agressif, malveillant et antisocial, donc quand des intrus apparaissent, il ne va pas céder à une quelconque tentative de négociation du passage - il va attaquer immédiatement.

Au début de son tour, il doit décider si il veut utiliser son *Cône antimagie*. Voici le problème : selon le Monster Manual, le cône "fonctionne aussi contre les rayons oculaires du tyrannoeil qui l'a activée". OK, imaginez que le tyrannoeil passe tout son temps avec son regard concentré sur l'entrée de son repaire, car c'est exactement le genre de chose qu'une aberration ferait. Un groupe d'intrus apparaît dans l'embrasure de la porte. Son instinct lui dit de tuer les intrus. La seule fonction de son *Cône antimagie* est de l'empêcher de tuer les intrus.

Tel qu'il est écrit, le *Cône antimagie* semble à première vue être un pouvoir d'une utilité très discutable. Le tyrannoeil va se positionner comme on place une caméra de sécurité : dans un coin en hauteur où il peut tout voir et où personne ne peut manœuvrer derrière lui. Où qu'il dirige son *Cône antimagie*, il va aussi attraper les créatures sur lesquelles il veut tirer avec ses rayons oculaires.

Mais avec un peu de géométrie, on peut voir comment faire fonctionner efficacement le *Cône antimagie*. Dans la cinquième édition de D&D, une zone d'effet en forme de cône couvre une distance de x mètres, jusqu'à une largeur de x mètres à cette distance. Sans vous ennuyer avec les calculs, je vous dirai simplement que, quelle que soit la portée du cône, cela signifie qu'il couvre un arc d'environ 53 degrés, soit un peu moins d'un sixième de cercle. Le résultat est que, lorsque les ennemis sont à distance, le *Cône antimagie* est pratiquement inutile. Mais lorsqu'ils sont proches, et qu'ils entourent le spectateur, il peut isoler un ou deux adversaires à l'intérieur du cône tout en projetant des rayons oculaires dans d'autres directions.

OK, donc les intrus sont apparus, le tyrannoeil attaque, il choisit de ne pas utiliser son *Cône antimagie* pour l'instant - mais que se passera-t-il si les intrus voient l'expression méchante sur le visage de ce Bob Razowski dessiné par H. R. Giger, font preuve d'une plus grande initative et agissent en premier ? Le tyrannoeil dispose de trois actions légendaires *Rayon oculaire* qu'il peut utiliser entre ses tours, et il va en utiliser une contre chaque PJs qui aura un tour avant lui.

Quand son tour arrive, il projette trois *Rayon oculaire*. Ces rayons sont choisis au hasard, sur la base de trois jets de d10 (avec des doublons). Comment le tyrannoeil choisit-il ses cibles ? Examinons les jets de sauvegarde nécessaires pour résister aux différents rayons :

* Force : Rayon télékinétique
* Dextérité : Rayon de lenteur, Rayon de pétrification, Rayon de désintégration, Rayon de mort
* Constitution : Rayon de paralysie, Rayon de nécrose
* Sagesse : Rayon de charme, Rayon de terreur, Rayon de sommeil

Le tyrannoeil dispose d'une Intelligence de 17, il sait donc mieux que quiconque qu'il ne faut pas utiliser son *Rayon de télékinésie* contre une brute barbare, son *Rayon de désintégration* contre un roublard halfelin hyperactif, son *Rayon de paralysie* contre un nain douillet, ou son *Rayon de charme* ou son *Rayon de sommeil* contre un elfe.

Supposons que vous lanciez le dé et que le spectateur utilise son *Rayon de télékinésie*, son *Rayon de pétrification* et son Rayon de sommeil*. Il visera avec le premier un magicien à l'air faible, le second un frêle roublard ou un sorcier, et le troisième un combattant ou un roublard non elfique. Il se peut qu'il ait raison ; il se peut qu'il ait tort. Mais sa première supposition sera toujours une supposition sensée.

Lors des tours suivants, le tyrannoeil affine ses choix. Si un rayon n'a pas fonctionné contre un personnage jouer particulier, il n'utilisera plus ce rayon - ni aucun de ses rayons frères - contre ce PJ. S'il a fonctionné, il utilisera également les rayons frères de ce rayon contre ce PJ.

Après son tour, le tyrannoeil utilisera ses actions légendaires lors des trois prochains tours de PJs, mais maintenant avec un rebondissement : au lieu d'attaquer automatiquement le PJs qui vient de prendre son tour, il choisit ses cibles en fonction de ce qu'il sait des PJs et de leur sensibilité à ses différents rayons.

De plus, à l'initiative 20, le spectateur utilise une action d'antre. Si un ou plusieurs PJs se chargent de combattre le tyrannoeil de près, il privilégiera la bave glissante. Si un ou plusieurs PJs se trouvent près d'un mur, il favorisera les appendices préhensibles. Si aucune des situations ci-dessus ne s'applique ou s'il a déjà utilisé sa seule action d'antre favorisée au tour précédent, il choisit l'apparition de l'oeil sur un mur.

Tant que les PJs restent hors de portée du corps à corps, le tyrannoeil n'utilisera que ses *Rayons oculaires* contre eux ; il ne se déplacera pas non plus de son plein gré de l'endroit choisi, qui sera en vol stationnaire à au moins 3 mètres de hauteur si la pièce le permet, de façon à rester hors de portée des personnages même s'ils s'y précipitent. Mais il y a un revers à la médaille : les tyrannoeils sont souvent appelés à jouer les gardiens par d'autres entités utilisant la magie. Supposons qu'un tyrannoeil se trouve sous un sort de *Coercition mystique* pour garder un passage vers une salle. Même s'il préfère planer hors de portée, si un PJ s'approche de ce passage, le tyrannoeil sera obligé de descendre pour le bloquer. Les PJs pourront alors l'atteindre en lançant des attaques de corps à corps.

Si un nombre suffisant de ses attaquants s'approchent de lui, il commencera alors à utiliser son *Cône antimagie* pour arrêter les lanceurs de sorts, à condition qu'il puisse le faire et qu'il continue à cibler les non lanceurs de sorts avec ses *Rayons oculaires*. Maintenir l'utilisation des rayons oculaires est toujours la plus grande priorité du spectateur. Il ne mordra que (a) s'il est à portée d'une cible et (b) si tous les PJs qu'il peut attaquer avec ses *Rayons oculaires* ont déjà prouvé leur résistance à deux ou plusieurs d'entre eux. Ou encore, si un adversaire particulier s'est montré résistant à deux types de rayons oculaires ou plus, mais qu'il est le seul qu'il considère encore comme une menace réelle.

Un tyrannoeil est tellement ultra-territorial que s'il est rencontré dans son repaire, il se battra toujours jusqu'à la mort ; idem s'il a été convoqué comme sentinelle. Seul un tyrannoeil rencontré en dehors de sa tanière (il doit se nourrir de temps en temps) et sans obligation de rester sur place fuira le combat, et ce après avoir subi des dégâts relativement modestes : seulement 54 pv (ce qui le réduit à 126 pv ou moins). D'abord, il se désengagera (action) et s'élèvera hors de portée des PJs (mouvement), puis il se précipitera (action) en ligne droite vers son repaire.

## TYRAMORT

Le **tyramort** est un monstre de "haut niveau" : un tyrannoeil mort-vivant. À bien des égards, il se comporte exactement de la même manière qu'un tyrannoeil normal. Mais il y a quelques exceptions.

Premièrement, son *Cône d'énergie négative* n'interfère pas avec ses propres rayons oculaires comme le fait le *Cône antimagie* du tyrannoeil, donc le *Cône d'énergie négative* est toujours actif tant que des intrus sont présents. Il maintient ce cône orienté dans la direction qui attire le plus d'opposants, y compris les personnes inconscientes (qui peuvent mourir et être ressuscitées en tant que zombies).

Deuxièmement, le tyramort n'est jamais trouvé en dehors de son repaire, à moins qu'il n'ait été convoqué comme sentinelle magique. Dans tous les cas, il ne s'enfuit pas, quel que soit l'ampleur des dégâts qu'il a subis. Son obligation de garde l'emporte sur tout le reste, y compris sur sa propre survie.

Enfin, le tyramort a une Intelligence de 19. En tant que MD, vous pouvez interpréter cela comme lui permettant de "lire" les statistiques de capacité des PJs. En d'autres termes, il ne se contente pas de vous lancer un *Rayon de nécrose* parce que vous avez l'air frêle - il vous lance un  parce que vous avez un modificateur de Constitution de -1. Il dirige chacun de ses *Rayons oculaires* d'abord vers le PJ le moins équipé pour lui résister, puis il se fraye un chemin vers le haut.

## SPECTATEUR

À l'autre extrémité du spectre des dangers, nous avons le **spectateur**, un petit cousin du tyrannoeil qui n'est pas malveillant, mais simplement dévoué. Il n'a pas de territoire propre, il est convoqué comme une sentinelle magique, et c'est sa seule et unique responsabilité. Il est prêt à parler civilement (par télépathie) avec les PJs qui le rencontrent, même s'il risque de paraître décalé, tendu et suspect. Et les PJs ont intérêt à ne pas s'en prendre à ce qu'il garde.

Les spectateurs n'ont que quatre types de *Rayons oculaires* : deux (confusion et terreur) qui visent la sagesse et deux (paralysie et blessure) qui visent la constitution. Ils ne tirent également que deux fois par action, au lieu de trois. Ils n'ont pas d'actions légendaires ou d'antre. Par ailleurs, ils se battent de la même manière que les tyrannoeil - en particulier, ils préfèrent fortement leur *Rayons oculaires* à leur attaque de *Morsure*, et ils ne mordent que s'il y a un adversaire à portée de corps à corps et que les *Rayons oculaires* ne fonctionnent pas. Ils ont également la capacité unique de *Renvoi de sort*, qui, après un jet de sauvegarde réussi, leur permet de rediriger les sorts qui leur sont lancés vers d'autres cibles. Contrairement aux tyrannoeil, les spectateurs ne possèdent pas une intelligence de génie ; ils redirigent les sorts non pas vers celui qui est le plus vulnérable, mais vers celui qui leur cause le plus de soucis. Enfin, comme d'autres de leur espèce, ils ne fuient pas. Ils ont un travail à faire, et par conséquent, ils vont le faire ou mourir en essayant.
