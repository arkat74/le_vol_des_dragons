---
layout: page
title: Cultistes et Prêtres
nav_order: 4
parent: PNJ Tactics
---

# CULTISTES ET PRÊTRES

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Article original : [NPC Tactics: Cultists and Priests](http://themonstersknow.com/npc-tactics-cultists-priests-druids/)

Un sceptique pourrait dire que la seule différence entre un "acolyte" et un " cultiste " est son point de vue, mais le Manuel des Monstres ne serait pas d'accord. La piété prend différentes formes selon qu'un PNJ est un figurant ou un antagoniste.

## MEMBRE DE SECTE

La cinquième édition du Manuel des Monstres ne fait même pas la courtoisie de donner des sorts à des **Membres de secte**. En fait, pour la plupart, ce sont juste des roturiers sournois avec des épées exotiques. Ils ne sont pas forts, ils ne sont pas durs et ils ne sont même pas furtifs. Ils ont des compétences en tromperie, dont la seule fonction, d'après ce que je peux en dire, est d'essayer de vous convaincre qu'ils ne sont pas du tout des Membres de sectes ("Non, m'sieur, juste des villageois ordinaires qui vaquent à leurs occupations habituelles !") ou à des fins de recrutement ("Dites, ça vous dirait de venir à notre réunion discrète/atelier d'auto-réalisation/lecture de poésie ?). *Sombre dévotion* est une caractéristique intéressante, mais qui ne suggère aucune tactique de combat particulière.

Normalement, une créature dont la seule caractéristique supérieure à la moyenne est la dextérité est une sorte de sniper à distance, mais les adeptes de la secte ne portent même pas d'armes à distance. Et pourquoi diable un adorateur de démons devrait-il avoir une dextérité légèrement supérieure à la moyenne ? Quel est leur véritable intérêt ?

Il me semble que la seule façon utile de penser aux Membres de secte est de les considérer comme des roturiers dotés d'une personnalité exceptionnellement intense. Qu'est-ce que cela signifie ? Eh bien, pour commencer, leur priorité absolue n'est pas d'être découverts en tant que membre de secte. Si les personnages joueurs les rencontrent en société, ils feront tout leur possible pour dissimuler leur loyauté. Ce n'est que si leur couverture est dévoilée qu'ils se battront ou s'enfuiront, le premier cas s'ils sont plus nombreux que les personnages joueur, le second s'ils sont en infériorité numérique. Ils se battront aussi jusqu'à la mort, sachant qu'ils pourraient être exécutés pour leurs hérésies, mais croyant aussi que des infidèles comme les PJs méritent de mourir eux-mêmes. A part cela, ils se battent avec aussi peu de sophistication que n'importe quel autre roturier.

## FANATIQUE DE SECTE

Les **Fanatiques de secte**, en revanche, ne sont pas une plaisanterie, principalement en raison de leur capacité de jetteur de sorts, qui comprend *Arme spirituelle* et *Blessure*. *Arme spirituelle* est lancée comme une action bonus et continue à fournir des actions bonus supplémentaires au cours des neuf prochains tours, ce qui en fait un élément indispensable de la stratégie d'action des fanatiques de sectes. *Blessure* est un puissant sort de mêlée qui inflige des dommages nécrotiques sur un coup (dans notre groupe, nous l'avons surnommé "le Mauvais Toucher"). Comparez le avec l'*Attaques multiples* du fanatique de secte, qui comprend deux coups de poignard pour un total potentiel de 2d4 + 4 dégâts perforant (9 pv, en moyenne) : lancé en utilisant un emplacement de sort de 1er niveau, le sort inflige des blessures qui font 3d10 de dégâts nécrotiques (16,5 pv, en moyenne), et boosté au 2ème niveau, il fait 4d10 (22 pv). C'est grave.

Les autres sorts que le fanatique de secte peut lancer sont les suivants : *immobiliser un humanoïde* (Sauvegarde de Sagesse, paralysie, concentration), *injonction* (peut provoquer des attaques d'opportunité en faisant fuir les adversaires en mêlée), *bouclier de la foi* (action bonus, concentration) et *flamme sacrée* (fait 1d8 de dégâts radiants à distance). Au total, un kit puissant pour un lanceur de sorts de niveau 4.

En tant que PNJ doté d'une grande Dextérité (toujours étonné par cela) et d'une Constitution légèrement supérieure à la moyenne, le fanatique de secte dispose de deux modes de combat possibles : le tir à distance et le combat en mêlée. Dans les deux cas, il est très important d'être nombreux, qu'il s'agisse d'autres fanatiques de la secte ou de membres de la secte qui peuvent diviser l'attention des PJs. S'ils sont accompagnés de membres de secte, les fanatiques de secte leur laisseront la plupart du temps le soin de se battre au corps à corps, et utiliseront plutôt *flamme sacrée* et *arme spirituelle* pour faire des dégâts à distance (au prix d'un seul sort de deuxième niveau - quelle bonne affaire !). Ils peuvent utiliser cette combinaison encore et encore, car *flamme sacrée* est un tour de magie et *arme spirituelle* est une action bonus. Cependant, avant de lancer *arme spirituelle*, ils lanceront *bouclier de la foi*, un autre sort qui nécessite une action bonus, mais seulement lorsqu'il est lancé pour la première fois. Après ce premier tour, ils peuvent garder *bouclier de la foi* et utiliser leurs actions bonus pour lancer et diriger l'arme spirituelle.

Il existe d'autres combinaisons tactiques que les fanatiques de sectes peuvent utiliser contre les PJs et qui nécessitent une attention particulière. Par exemple, ils peuvent lancer *injonction* sur un adversaire gênant en lui demandant "Approche !" puis, au tour suivant, lui infliger des blessures. Ils peuvent laisser tomber *bouclier de la foi* pour lancer *immobiliser un humanoïde* et paralyser un PJ, puis lui infliger des coups critiques avec *arme spirituelle* (encore mieux si ce PJ est déjà engagé dans une mêlée avec un ou plusieurs membres de secte) et profiter du raté automatique des jets de sauvegarde (FOR et DEX) contre *flamme sacrée* (voir état Paralysé, PHB p.292). Et, comme mentionné précédemment, ils peuvent ordonner à un PJ qui est engagé dans une mêlée avec plusieurs adversaires de "Déguerpir !", ce qui permet à chacun de ces adversaires de bénéficier d'une attaque d'opportunité.

Dans l'épisode "Recognized !" du chapitre 4 de Hoard of the Dragon Queen, un PJ est reconnu par un cultiste de la caravane, qui tente de l'assassiner. Dans notre groupe, le PJ malchanceux était le paladin, et au nom de la tension dramatique, j'ai décidé que le cultiste qui l'avait reconnu était en fait un fanatique de secte. Pendant que le paladin dormait, le fanatique de secte lui a jeté *immobiliser un humanoïde* pour le paralyser, puis l'a traîné hors de sa tente, sans arme ni armure, dans l'obscurité au-delà du campement des caravanes. A ce moment-là, le fanatique de secte a frappé le paladin paralysé en lui lançant *blessure*. J'ai joué soigneusement cette rencontre au préalable, pour m'assurer que le paladin serait capable de faire son jet de sauvetage au bon moment et de survivre en empilant des capacités et des sorts comme *Châtiment divin* et faveur divine en plus des attaques de mêlée non armées. Comme prévu, il a survécu... à peine.

Les fanatiques de sectes, comme leur nom l'indique, vont se battre jusqu'à la mort.

## ACOLYTE

Les **acolytes** ne sont pas vraiment équipés pour le combat, sauf en tant que renfort. Leurs capacités physiques sont toutes moyennes. Ils sont armés de gourdins, représentant des armes improvisées (dans un temple ou un sanctuaire, un beau et lourd chandelier est une bonne chose). En fait, ce sont des roturiers avec une Sagesse supérieure à la moyenne et la capacité de lancer des sorts.

Les sorts clés de leur arsenal sont : *bénédiction*, *soin des blessures*, *sanctuaire* et *flamme sacrée*. Ils utiliseront *bénédiction* s'ils sont accompagnés d'alliés qui engagent de vrais combats, *soigner les blessures* de tout allié qui prend une blessure modérée ou pire, *sanctuaire* sur un allié clé qui a besoin de protection ou sur tout allié qui est gravement blessé, et *flamme sacrée* dans toute autre situation qui les amènerait à attaquer. Mais, comme les roturiers, ils ont autant de chances de se figer ou de fuir que de se battre ; cela dépend entièrement des circonstances et des personnes avec lesquelles ils se trouvent. Si les PJs menacent leurs supérieurs, leurs fidèles ou toute personne sous leur protection, ils sont plus susceptibles de se battre qu'ils ne le seraient autrement.

## ECCLÉSIASTIQUE

Les **Ecclésiastiques** ne sont pas non plus très adaptés au combat et ne combattent généralement qu'en légitime défense, mais ils ont quelques astuces dans leurs tenues. Parmi ceux-ci, on peut citer *esprits gardiens*, un puissant sort de défense à effet de zone qui exige de la concentration. Si un combat a lieu, c'est le premier sort qu'un prêtre lancera, renonçant à son action bonus (qui aurait pu être utilisée pour lancer *arme spirituelle*) afin de gagner ce puissant champ de protection divine. Lors de sa deuxième action, le prêtre lancera *arme spirituelle* comme action bonus et, si nécessaire, le Tour de magie *flamme sacrée*, qui fait 2d8 de dégâts plutôt que 1d8 en raison du niveau du lanceur de sorts du prêtre. Si un ennemi s'approche du prêtre, très probablement parce que la Sagesse de l'ennemi est suffisamment élevée pour résister aux effets de *gardien spirituel*, il ou elle utilisera *Distinction divine* comme action bonus, puis attaquera (action) avec son arme améliorée magiquement. Dans la plupart des cas, cependant, le prêtre cherche à repousser les attaquants plutôt qu'à les tuer. Un prêtre gravement blessé (réduit à 10 pv ou moins) se rendra ou s'enfuira à moins que les PJs ne soient d'un alignement directement opposé à celui du prêtre ou qu'ils ne vénèrent une divinité ennemie de celle que le prêtre vénère. Dans ce cas, le prêtre peut voir une mort glorieuse en combattant des infidèles comme son ticket pour un traitement miséricordieux dans l'au-delà.

En présence d'alliés, le prêtre dispose de deux autres options : après avoir lancé *arme spirituelle*, un prêtre peut choisir de lancer *balisage* au lieu de *flamme sacrée* sur un ennemi que l'un de ses alliés est en train de combattre : une réussite donne au prêtre 4d6 de dégâts radiants et à son allié l'avantage au prochain jet d'attaque contre cet ennemi. De plus, comme l'acolyte, un prêtre peut lancer *soins des blessures* sur un allié qui a pris une blessure modérée ou pire, ou un *sanctuaire* sur un allié qui a besoin d'un secours d'urgence (n'oubliez pas que les sorts de niveau lancés comme actions bonus, comme *sanctuaire* et *arme spirituelle*, ne peuvent être associés dans le même tour qu'à des Tours de magie, comme *flamme sacrée*, et non à d'autres sorts de niveau lancés comme actions, comme *balisage*).

## DRUIDES

Enfin, il est intéressant de noter que les **druides** sont plus combatifs que la moyenne des membres du clergé, avec une Dextérité et une Constitution légèrement supérieures à la moyenne et le délicieux Tour de magie *gourdin magique*, qui augmente leurs modificateurs d'attaque avec des portées de +2 à +4 et leurs dégâts de 1d6 à 1d8 + 2 (le MM dit seulement 1d8, mais il s'agit d'une erreur documentée) et ne coûte qu'une action bonus. Le principal atout de leur arsenal est cependant *enchevêtrement*, un sort à effet de zone qui a le potentiel de contenir les ennemis, leur accordant un désavantage sur les attaques et un avantage sur les attaques contre eux - et faisant d'eux des cibles faciles pour les sorts de dégâts qui nécessitent des sauvegardes de Dex (malheureusement, les druides n'en ont pas).

Les druides sont discrets, donc toute rencontre avec l'un d'entre eux se fera très probablement sur son propre terrain, et leur objectif en cas de conflit sera de faire disparaître les PJs (s'ils sont plus faibles) ou de s'éloigner des PJs (s'ils sont plus forts). Nous examinons ici principalement deux combinaisons tactiques. La première est *enchevêtrement* (action) plus *produire une flamme* (action) : premièrement, retenir les PJs ; deuxièmement, pendant qu'ils sont retenus, lancer des flammes vers eux avec un avantage pour 1d8 de dommages de feu par coup. L'autre est *gourdin magique* (action bonus) plus une attaque à l'arme de mêlée (action), avec *vague tonnante* en renfort lorsque le druide est entouré par trois adversaires de mêlée ou plus et doit faire le ménage. L'un ou l'autre peut être efficace pour tenir bon, en fonction des circonstances, du terrain et de l'adversaire.

Si un départ précipité est nécessaire, le druide lance *grande foulée* (action), augmentant sa vitesse à 12 mètres par tour ; puis, au tour suivant, Se précipiter (action) dans la nature. (*peau d'écorce* ne vaut pas le temps qu'il faut pour lancer ce sort ; si le druide lance ce sort, ce sera avant que le combat ne commence, et le druide perdra un emplacement de sort de 2ème niveau).
