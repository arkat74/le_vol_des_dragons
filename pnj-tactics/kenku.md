---
layout: page
title: Kenku
nav_order: 8
parent: PNJ Tactics
---

# KENKU

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Les Kenku, librement inspirés du tengu du mythe japonais, sont des bipèdes à plumes avec des têtes et des pieds d'oiseaux, des torses, des jambes et des bras humanoïdes, et un manque d'ailes marqué, que leurs ancêtres ont perdu en punition de leur duplicité et de leur avarice. Non seulement ils manquent d'ailes, mais ils manquent aussi de parole : Ils ne peuvent communiquer qu'en répétant les sons et les phrases qu'ils ont entendus.

La seule capacité marquante d'un kenku est sa grande dextérité ; toutes ses autres capacités sont moyennes. En combinaison avec ses compétences en matière de perception et de furtivité et son trait d'*Attaque surprise*, cela lui donne un certain potentiel d'attaque de choc en mêlée, mais dans la plupart des cas, il est mieux en tant que tireur d'élite. En particulier, les kenku n'ont pas une vision dans le noir, comme beaucoup d'attaquants en embuscade. Ils mettent en place des embuscades, mais ils le font pendant la journée, pas la nuit.

Un kenku solitaire peut tenter de s'en prendre à un individu isolé, mais les kenkus qui s'attaquent à des groupes ont besoin de la force du nombre - au minimum, deux kenkus par victime. En utilisant la furtivité, ils se cachent ou traquent leurs victimes ; quand le moment est venu, ils se lâchent avec des tirs surprises à l'arc court.

Après la première volée de flèches, un demi à un tiers d'une troupe de kenku se précipite pour engager des cibles plus faciles en mêlée avec leurs épées courtes, tandis que leurs alliés à l'arc lâchent volée après volée sur des combattants plus redoutables. Les archers kenku essaient toujours de maintenir une distance d'au moins 10 mètres de leurs cibles ; si possible, ils tirent à une distance de 15 à 20 mètres. Lorsqu'un ennemi charge, ils se retirent à la même distance, tandis que les autres alliés continuent à tirer. S'ils ont la place, les archers kenku s'espacent de façon à ce qu'un ennemi qui charge ne puisse pas facilement atteindre les autres. Un combattant à l'épée kenku se désengage et recule si un combattant en mêlée compétent commence à se diriger vers lui. Les kenku communiquent en permanence, grâce à un code d'effets sonores apparemment aléatoires, pour s'informer mutuellement de l'état de la situation et appeler à l'aide lorsqu'ils en ont besoin.

Les Kenku n'ont pas beaucoup d'appétit pour un combat à coups de poing et à coups de pied. Un kenku alerte ses alliés lorsqu'il est modérément blessé (réduit à 9 pv ou moins) ; s'il est engagé dans une mêlée, il se désengage et bat en retraite. Un groupe entier de Kenkus se retirera et se dispersera si le combat n'est pas terminé au quatrième tour - plus tôt si au moins la moitié d'entre eux sont gravement blessés (réduits à 5 pv ou moins).

Les Kenku sont cupides, mais étant neutres et chaotiques, ils ne sont pas nécessairement malveillants. Ce qu'ils veulent, c'est de la matière précieuse et brillante ; tuer des gens n'est qu'une façon de l'obtenir. Ils comprennent le langage courant et peuvent être achetés pour le prix de, disons, cinq pièces d'or chacun, ou un montant équivalent en pierres précieuses. Doublez ce prix, et vous vous êtes peut-être acheter une faveur ; quintuplez-le, et vous vous êtes peut-être acheté un ami, ou éventuellement un collaborateur. Si les PJs remarquent une bande de voleurs Kenku avant qu'ils n'aient une chance d'attaquer, la corruption est un bon moyen d'arrêter un combat avant qu'il ne commence.
