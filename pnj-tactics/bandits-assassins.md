---
layout: page
title: Bandits et Assassins
nav_order: 2
parent: PNJ Tactics
---

# BANDITS ET ASSASSINS

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Article original : [NPC Tactics: Bandits and Assassins](http://themonstersknow.com/npc-tactics-bandits-assassins/)

## BANDIT

Dans le [post d'hier](../garde-voyous-veteran-chevalier/), j'ai parlé des personnages non-joueurs qui sont susceptibles d'être trouvés en train d'appliquer la loi ; aujourd'hui, je parlerai de ceux qui sont susceptibles d'être trouvés en train de l'enfreindre, en commençant par les **bandits**. Le bloc de statistiques des bandits n'est pas le modèle idéal pour le cambrioleur ou le pickpocket typique. La cinquième édition du _Monster Manual_ omet complètement cet archétype. À la fin de cet article, je vous fournirai un bloc de statistiques fait-maison que vous pourrez utiliser pour ce type de PNJ. Le bandit du _Monster Manual_ ressemble plutôt à un bandit de grand chemin (sur terre) ou à un pirate (en mer), et sa motivation première est le pillage.

Les capacités physiques du bandit sont toutes modestement supérieures à la moyenne, avec en tête la dextérité et la constitution : les bandits sont des combattants teigneux qui comptent sur leur nombre. Ils manient des "cimeterres", pour des raisons que je ne peux que deviner - peut-être que c'est, dans la 5ème édition de Donjon & Dragon, ce qui ce qui se rapproche le plus du coutelas ? Peut-être parce qu'elle considère les épées courtes comme des armes d'estoc et pense que les bandits devraient plutôt porter des armes tranchantes ? Je ne sais pas. Les propriétés de l'arme et les dégâts sont les mêmes, sauf pour le type de dégat, et s'il existe une sorte d'armure ou d'enchantement qui résiste aux dégâts tranchants mais pas aux dégâts perforant ou vice versa, je ne l'ai pas encore trouvé. Quoi qu'il en soit, vous pouvez laisser la saveur du décor déterminer si vos bandits portent des cimeterres, des coutelas, des épée, des dirks, des gladii ou autre - ils font tous 1d6 + 1.

Les bandits engagent le combat en entourant leurs cibles, en nombre suffisant pour encourager une reddition rapide, et ils comptent sur le fait que leurs cibles se rendront facilement. Ainsi, toute rencontre avec un groupe de bandits doit être au moins de difficulté moyenne et doit vraiment être au moins difficile (voir _DMG_ p. 82), sinon les bandits seraient déjà partis à la recherche de proies plus faciles. Ils entourent leurs cibles à une distance de 12 à 24 mètres, des arbalètes légères à portée de main, et émettent leurs demandes (ils n'ont aucune compétence en matière d'_Intimidation_ ou de _Persuasion_ - ils se fient à leurs armes et à leur nombre pour convaincre à leur place). Si les PJ refusent, ils tiennent leurs positions, attaquant à distance, jusqu'à ce que les PJ viennent à eux, moment auquel ils passent à leurs lames.

Un bandit qui subit une blessure quelconque esquive (et ne se désengage pas) et se place du côté du bandit le plus proche, dans l'espoir que deux personnes soient capables de battre un ennemi qu'une seule ne peut pas vaincre. Il suffit d'une blessure modérée (réduisant le bandit à 7 pv ou moins) pour donner à un bandit l'idée que ces gens ne sont pas les pigeons qu'ils sont censés être ; si la moitié ou plus d'un groupe de bandits sont modérément blessés, ils se désengageront (action) et battront en retraite. Tout bandit seul qui est gravement blessé (réduit à 4 pv ou moins) s'enfuit en utilisant l'action _Se précipiter_, risquant ainsi de subir une ou plusieurs attaques d'opportunité. En revanche, si le combat se déroule mal pour les PJ et qu'ils se rendent, les bandits les dépouilleront volontiers de leurs objets de valeur et leur diront adieu. ... mais si un PJ d'origine noble fait partie du groupe, ils peuvent le faire prisonnier et le garder pour une rançon !

## CAPITAINE BANDIT

Le **capitaine bandit** se distingue d'un bandit ordinaire par des capacités physiques supérieures (cette fois, la dextérité et la force prennent le pas sur la constitution), une grande intelligence et un grand charisme, des compétences en _Athlétisme_ et en _Supercherie_, une action _Attaques multiples_ et une réaction _Parade_. Un capitaine bandit ira au-delà d'une simple demande de "se tenir debout et agir" - il ou elle s'engagera dans une véritable négociation. Par exemple, si les PJ semblent devoir renoncer à tous leurs objets de valeur, le capitaine bandit peut suggérer que pour le prix modeste de 10 pièces d'or par PJ, aucune des deux parties ne doit se donner la peine de voir qui est vraiment le plus fort. Bien sûr, le capitaine bandit n'a pas non plus de scrupules à mentir comme un arracheur de dents.

Le capitaine bandit n'a pas de compétences en matière d'_Intuition_ et n'a donc pas de talent particulier pour lire les motivations des PJ, mais il ou elle peut faire des suppositions éclairées en se basant sur les apparences et fabriquer une histoire en conséquence. Si les PJ sont principalement des héros populaires et des étrangers, le capitaine bandit peut prétendre que sa bande de brigands est composée de rebelles vertueux qui résistent à une aristocratie tyrannique. Si les PJ sont des Acolytes au coeur tendre ou des Nobles crédules, le capitaine bandit peut dire que la pauvreté les a poussés à faire du banditisme par nécessité. Si les PJ sont principalement des charlatans, des criminels ou des gamins des rues, le capitaine bandit peut laisser tomber toute prétention et dire que tout est permis, ou même recruter les PJ pour qu'ils rejoignent sa bande de hors-la-loi ! Ces histoires peuvent être vraies, fausses ou un peu des deux, mais cela n'a aucune importance : tout ce qui intéresse le capitaine bandit, c'est de soutirer du butin aux PJ par les moyens les plus simples possibles. Et si les PJ refusent, "Alors, je suppose que nous allons devoir le faire de la manière forte".

Le capitaine bandit tel que décrit dans le Monster Manual ne porte aucune arme à distance à l'exception d'une dague qu'il peut lancer, et comme il peut la manier avec un cimeterre (coutelas, épée à une main, dirk, gladius), il n'a aucune bonne raison de la lancer, surtout lorsqu'il est entouré de bandits avec des arbalètes légères qui font plus de dégâts. De plus, les principales capacités du capitaine bandit sont la Dextérité et la Force, ce qui signifie qu'il ou elle va se lancer dans des attaques rapides et des dégâts importants. Cela signifie action *Attaques multiples* avec deux attaques au cimeterre/épée et une autre attaque au poignard. Enfin, le capitaine bandit peut parer (réaction *Parade*) une attaque de mêlée par tour, et comme le chevalier, il peut juger lequel des multiples adversaires de mêlée présente le plus grand danger et doit donc être paré.

Le capitaine bandit dispose d'un nombre généreux de points de vie (10 dés de vie, contre 2 pour un bandit ordinaire) et ne craint pas d'être la cible d'attaques de plus d'un PJ : s'il est pris à deux ou trois, il esquivera (action) et parera (réaction) et laissera ses arbalétriers bandits tirer de loin sur les PJ. Le capitaine des bandits n'a pas particulièrement peur de la magie non plus, ayant des bonus de compétence sur les jets de sauvegarde de Dextérité et de Sagesse - deux des "trois grands". Cependant, si on lui donne l'occasion d'attaquer avec un avantage, le capitaine bandit la saisira, même s'il l'esquiverait autrement (par exemple, si le capitaine bandit est attaqué à la fois par un combattant et un barbare et que le barbare utilise son attaque *Témérité*, le capitaine bandit prendra l'ouverture et attaquera le barbare).

Comme les bandits, le capitaine bandit accordent une grande importance à leur vie. S'ils sont modérément blessés (45 pv ou moins), ils continueront à se battre, mais ils rouvriront aussi les négociations pendant le combat : "Nous pouvons sûrement parvenir à un arrangement mutuellement satisfaisant ? S'ils sont gravement blessés (réduits à 26 pv ou moins), ils déposeront leurs armes, se rendront sur place et accepteront les conditions qui leur permettront de continuer à respirer.

## ASSASSIN

Les **assassins**, dans le _Monster Manual_ de la 5ème édition, sont spectaculairement dangereux, principalement pour une raison : les dégâts de poison qu'ils infligent avec leurs attaques à l'arme. D'après les dégâts et les effets qu'il inflige, l'assassin semble utiliser du poison wyvern (voir Poisons, DMG p.257-258). À 1 200 po par dose, c'est un produit coûteux. Il va donc de soi que les assassins n'utilisent pas ce poison tout le temps - seulement lorsqu'ils sont en mission pour assassiner une cible particulière. Si vous éliminez les dommages causés par le poison lors des attaques de l'assassin, il ou elle devient un ennemi CR 4, plutôt que CR 8.

Dans l'épisode "Pas de place à l'auberge" du chapitre 4 de Hoard of the Dragon Queen, les joueurs rencontrent un groupe de quatre assassins, déguisés en nobles, qui ont pris possession d'une auberge au bord de la route. Comme l'écrit le MM, ces assassins constituent une rencontre impossible à tuer pour les PJ du niveau 4, ce que suppose l'aventure à ce stade. Cependant, ces assassins sont en route pour Waterdeep et n'ont pas l'intention de tuer qui que ce soit cette nuit-là, il n'est donc pas logique qu'ils aient du poison coûteux sur leurs lames. La rencontre est toujours potentiellement mortelle si les assassins sont des CR 4, mais ce n'est plus impossible. Si même les assassins de CR 4 sont trop puissants pour vos PJ, d'autres blogueurs ont recommandé d'utiliser le bloc de statistiques des vétérans à la place.

Un assassin est un adversaire tactiquement fascinant, mais légèrement compliqué. Sa grande dextérité et sa constitution, mais sa force moyenne en font un combattant acharné. Sa capacité de furtivité ultra élevée, ainsi que ses caractéristiques d'assassinat et d'attaque furtive, en font un attaquant d'embuscade. Normalement, les créatures de force inférieure essaient de compenser par le nombre, mais les assassins travaillent généralement seuls, ou du moins en très petits groupes, donc s'ils sont capables de rester dans les parages et de se battre, ils ne le veulent peut-être pas. Par conséquent, la première frappe est primordiale.

Voici comment tout cela s'assemble :

* **Assassinat** : "Pendant son premier tour, l'assassin est avantagé lors des jets d'attaque effectués contre une créature qui n'a pas encore joué son tour. Toutes les attaques que l'assassin réussit contre une créature surprise sont des coups critiques". Par conséquent, les assassins n'attaqueront pas du tout s'ils ne peuvent pas le faire par surprise. S'ils sont découverts avant d'attaquer - peu probable, étant donné leur modificateur de furtivité +9*, mais possible - les assassins battent en retraite.
* **Attaque sournoise** : "Une fois par tour, l'assassin inflige 14 (4d6) dégâts supplémentaires quand il touche une cible avec une attaque d'arme s'il est avantagé lors du jet d'attaque, ou quand la cible se trouve à 1,50 mètre ou moins d'un de ses alliés qui n'est pas neutralisé et que l'assassin n'est pas désavantagé lors du jet d'attaque". L'avantage vient de la capacité Assassinat.
* **Attaques multiples** : "L'assassin fait deux attaques à mots courts." Comme ces attaques se produisent toutes les deux au premier tour de l'assassin, ce dernier prend l'avantage sur les deux, et ce sont les deux crits s'ils frappent. Cependant, l'attaque furtive ne peut être utilisée qu'une fois par tour, elle ne s'applique donc qu'à la première attaque réussie de l'arme.

\* Il y a beaucoup d'erreurs dans le bloc de statistiques de l'assassin. Voir les errata MM pour une liste complète.

Supposons qu'un assassin se cache dans les chevrons, attendant de tuer le seigneur Milan de Lombard. Le seigneur Milan passe sous l'assassin. Sa perception passive de 10 ne lui suffit pas pour battre le jet de furtivité de 21 de l'assassin. L'assassin tombe au sol (compétence Acrobatie !) et utilise l'action *Attaques multiples*, avec surprise, tirant ainsi avantage des deux attaques. Le premier jet d'attaque est un touché - donc un critique, à cause de *Assassiner* - et aussi un *Attaque sournoise*, pour 2d6 + 3 dommages d'arme, plus ~~4d6~~ **8d6** dommages de l'*Attaque sournoise*, plus ~~7d6 14d6~~ **7d6** dommages de poison (réduits de moitié si Lord Milan fait son jet de sauvetage). Son deuxième jet d'attaque est également un coup critique mais pas une *Attaque sournoise*, et il inflige 2d6 + 3 dégâts d'arme, plus ~~7d6 14d6~~ **7d6** dégâts de poison (encore une fois, réduit de moitié si le sauvetage est réussi). Au total, les dégâts maximums possibles sont de ~~22d6 + 6, soit 83 pv~~ ~~40d6 + 6, ou 146 pv~~ **26d6 + 6**, ou **91 pv** en moyenne. Criminel.

Cela devrait être suffisant pour retirer une cible du jeu. Si ce n'est pas le cas, l'assassin s'éclipse, utilisant le reste de son mouvement du tour pour s'éloigner le plus possible. Notez que si le manuel du joueur 5E précise que chaque mètre escaladé vous coûte 1 mètre supplémentaire, il ne mentionne pas la quantité de mouvement consommée par la chute - ce qui laisse entendre qu'elle peut en fait ne consommer aucun mouvement. Ainsi, après une manœuvre comme celle décrite ci-dessus, ou une manœuvre dans laquelle l'assassin se cache dans l'ombre ou derrière une draperie et attend que la cible se déplace à portée de son arme, l'assassin peut encore avoir tout son mouvement disponible ! Si l'assassin utilise une manoeuvre d'*Acrobatie* pour s'échapper, par exemple en grimpant par une fenêtre, ou une manoeuvre de *Furtivité*, par exemple en se faufilant dans une foule, les chances de s'échapper sont encore plus grandes, car les poursuivants potentiels n'ont pas forcément les compétences nécessaires pour les poursuivre.

Je ne saurais trop insister sur ce point : l'assassin ne reste pas dans les parages et ne continue pas à se battre. Si cette première frappe ne suffit pas à achever la cible, tant pis. L'assassin essaiera de nouveau plus tard, lorsque les bonnes conditions se présenteront à nouveau. Pas maintenant.

Comme il est écrit, l'assassin porte également une arbalète légère, il y a donc un scénario alternatif à l'attaque initiale, qui est que l'assassin tire depuis un endroit où il est caché. L'*Attaques multiples* ne s'applique pas aux arbalètes en raison de leur temps de rechargement, donc dans ce cas, la première attaque fait tout. L'assassin fait le jet d'attaque avec avantage, obtient un crit sur un jet réussi, applique le bonus de l'*Attaque sournoise* et les dégâts du poison, pour un total potentiel de 2d8 + ~~11d6~~ 15d6 + 3 dégâts, soit une moyenne de ~~50,5~~ 64,5 pv. Si possible, l'assassin effectue cette attaque à partir de la portée normale maximale de l'arbalète légère, qui est de 24 mètres. Cela donne à l'assassin juste assez de distance pour pouvoir tirer une deuxième fois si nécessaire, ce qui n'inclura plus les bonus de l'*Assassinat*, qui ne s'applique qu'au premier tour de l'assassin, ou de l'*Attaque sournoise*, puisque le premier tir aura donné la position de l'assassin. Mais il reste les dégâts du poison, de sorte que l'assassin peut faire encore 1d8 + 7d6 + 3 dégâts, soit 32 pv en moyenne, avant de se faufiler. Cela rend cette méthode d'assassinat à peu près aussi efficace que la méthode de combat en mêlée décrite ci-dessus.

Toute rencontre avec un assassin est susceptible d'être immédiatement suivie d'une poursuite, alors familiarisez-vous avec la section Poursuites du DMG (pages 252-255). Cela en vaut la peine !

## Pickpocket

*Humanoïde (n'importe quelle race) de taille Moyenne, n'importe quel alignement autre que Mauvais et Bon*

**Classe d'armure** 13 (armure de cuir)
**Points de vie** 13 (3d8)
**Vitesse** 9 mètres

**FOR** 10 (0) **DEX** 14 (+2) **CON** 10 (0) **INT** 10 (0) **SAG** 12 (+1) **CHA** 10 (0)

**Compétences** Acrobaties +4, Déception +2, Perception +3, Tour de main +6, Outils de voleurs +6
**Sens** Perception passive 14
**Langues** une seule langue (généralement commune), argot des voleurs
**Dangerosité** 1/4 (50 XP)



***Rusée.*** En action bonus, le Pickpocket peut Se précipiter, Désengager, Se cacher ou Utiliser un objet ; faire un tes de Dextérité (Escamotage) ; Désarmer un piège ou ouvrir une serrure avec des outils de voleurs.

***Monte-en-l'air.*** Le Pickpocket peut grimper à une vitesse de mouvement normale et peut faire un saut en longueur de 3,6 mètres ((FOR / 3) + (0,3 x mod DEX.) = (10 / 3) + (0,3 x 2)).

### ACTIONS

***Dague.*** Mêlée ou attaque à distance : +4 pour toucher, allonge 1,5 mètres ou une portée de 6/18 mètres, une créature. Touché : 4 (1d4 + 2) dommages perforant.