---
layout: page
title:  "Séance 1 - création des personnages-joueurs"
nav_order: 2020-01-28 20:00:00
parent: Séances
---

Création des personnages avec les joueurs :

* Fabrice : barde humain
* Loïc : roublard elfe
* Constant : magicien gnome
* Christophe : clerc du savoir nain des collines (dieu Dugmaren Brilletoge)
* Matthis : Guerrier Sangdragon ascendant d'or

Historique :
* Loïc : enquêteur du Guet (à voir s'il passe dans l'Odre vigilant des Magistes et Protecteurs) Rynnarod
![Rynnarod Stardust](../../assets/images/rynnarod-stardust.jpg "Rynnarod Stardust")
* Christophe : acolyte (Torgath)
![Torgath](../../assets/images/torgath.jpg "Torgath")
* Constant : Sage (Eldon)
* Fabrice : chasseur de prime urbain (Arikel)
![Arikel](../../assets/images/arikel.png "Arikel")
