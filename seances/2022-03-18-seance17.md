---
layout: page
title:  "Séance 17"
nav_order: 2022-03-18 20:00:00
parent: Séances
---

## Question séance précédente

* trouver les faiblesses des rats-garous

## Questions en cours de séance

* Faire un trombinoscope des PNJs

## La séance

### 22 Ches

* Barnibus propose à Rynn de faire l'interrogatoire de Dalakhar (communication avec les morts)
* Rynn demande à Renaer s'il veut bien être présent pour l'interrogatoire de Dalakhar
* Interrogatoire agent du Zhentarim
* Torgath lance _Doux repos_ pour préserver le corps de Nat (10j), elle est mise dans son lit à l'étage
* prévoit un _Rappel à la vie_ pour Nat, on prevenue les pretres

### 23 Ches

* Torgath lance _Doux repos_ sur Nat
* Parte à l'auberge de la Dague Sanglante, interroge Filiare (le tavernier) sur Dalakhar
* vont voir la chambre
* en sortant de l'auberge de la Dague Sanglante, Arikel détecte qu'ils sont suivit par l'équipe d'intervention de Xanathar (un capitaine bandit et 3 malfrats)