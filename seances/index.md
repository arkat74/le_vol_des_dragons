---
layout: page
title:  "Séances"
date:   2020-01-28 20:00:00
has_children: true
nav_order: 2
---

## Liste des séances

Les séances de la campagne Waterdeep: le Vol des Dragons

Personnages Joueurs :

* Rynnarod Stardust : enquêteur du Guet (Loïc)
* Torgath : acolyte (Christophe)
* Eldon : Sage (Constant)
* Arikel : chasseur de prime urbain (Fabrice)
