---
layout: page
title:  "Il l'a dans la peau"
nav_order: 1
grand_parent: Préparations
parent: Missions
---

# IL L'A DANS LA PEAU
{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## CONTEXTE DE L'AVENTURE

Le seigneur démoniaque Graz'zt est préoccupé par la croissance rapide du pouvoir de l'archidiable Asmodée dans Waterdeep. Incapable de prendre pied dans la ville proprement dite, Graz'zt a jeté son dévolu sur la zone rurale adjacente de Sousfalaise.

Un jeune homme nommé Basil Mureal travaillait comme gardien à la prison de travail de *la Ferme de la Rédemption*, l'occupation parfaite pour un homme cruel qui n'a jamais fait preuve de la constitution ou de la volonté de travailler dans l'agriculture. Lorsque le charismatique Graz'zt a proposé à Mureal de faire de son entourage ses bailleurs qui s'inclineraient devant lui, Mureal s'est immédiatement mis au service de Graz'zt sans même y réfléchir.

Graz'zt a enseigné à Mureal un sombre rituel transformant un simple marteau utilisé dans la construction en une puissante arme magique. Sur ordre du seigneur des ténèbres, Mureal a utilisé le marteau pour assassiner trois prisonniers, emprisonnant leurs esprits dans l'arme. Il a ensuite remis l'outil au service de la construction : trois épouvantails, chacun animé par un esprit capturé chez les victimes de Mureal.

Alors que les épouvantails s'attelaient à leur tâche immortelle qui consistait à terroriser les habitants de Sousfalaise qui soutenaient ouvertement le pouvoir de Waterdeep, Graz'zt s'est tourné vers la construction de son prochain outil : une relique de la Guerre du Sang représentant une victoire contre l'armée des archidémons. Intoxiqué par le pouvoir qui lui était déjà conféré et avide de plus, Mureal accepta naïvement de devenir un réceptacle pour le démon, détruisant complètement tout semblant d'homme dans le processus.

Son démon babau à présent convoqué sur le plan matériel, Graz'zt le chargea de trouver des tunnels oubliés depuis longtemps dans la falaise qui menait à la ville de Waterdeep. S'il parvient à établir une présence dans la ville, il en profitera pour semer le chaos et saper l'autorité d'Asmodée de toutes les manières possibles.

> **Waterdeep: Le Vol des Dragons - Factions**
>
> Cette aventure est basée sur la première mission de l'Enclave d'émeraude (pour les personnages du 2ème niveau) à la page 35 du livre Waterdeep : Le Vol des Dragons.
>
> Bien que l'Enclave d'émeraude convienne parfaitement à la mission explorée dans cette aventure, elle peut également être émise par n'importe laquelle des factions. Une accroche pour chaque faction est inclus dans la première section de l'aventure.
>
> Si votre campagne se déroule en été et que les Cassalantre vénèrant Asmodée sont les méchants, n'ayez crainte. Rien de ce que le seigneur démon Graz'zt accomplit n'interférera avec le chapitre 6 de la campagne.
>
> Bien que cette aventure se déroule dans les terres agricoles à l'est de Waterdeep, elle pourrait facilement être adaptée à n'importe quel cadre rural de votre campagne.

## APERÇU

L'aventure se compose de ces sections :

**Accroches des Factions**. Une faction de votre choix demande aux aventuriers d'enquêter sur les rumeurs d'un trio d'épouvantails qui ont pris vie et qui terrorisent Sousfalaise.

**Les épouvantails prennent vie**. A un moment de l'aventure de votre choix, les épouvantails enchantés se révèlent et attaquent le groupe.

**Sous la falaise**. Sur le chemin du village de Sousfalaise, le groupe rencontre une foule en colère.

**Coup à la tête**. Les personnages enquêtent sur un triple homicide à la Ferme de la Rédemption, au moment où la tension des détenus grimpe en flèche.

**Hors de Sa Peau**. La maison de Basil Mureal révèle de sombres secrets laissés derrière lui.

**Quelques mauvaises pommes**. La disparition de Mureal n'empêche pas sa secte de se réunir. De quel côté de l'embuscade les personnages se retrouveront-ils ?

**En train de creuser**. Les aventuriers affrontent un démon dans une caverne cachée dans la falaise de Waterdeep.

## ACCROCHES DES FACTIONS

Toutes les factions de Waterdeep ont des raisons d'envoyer le groupe dans cette aventure, et chacune a une sorte d'aide à offrir.

Si votre campagne se situe en dehors de Waterdeep, utilisez les exemples ci-dessous pour créer votre propre accroche unique.

### LES MÉNESTRELS

Mirt envoie aux aventuriers un *oiseau en papier* qui les invite à le rejoindre au Portail Béant. Lorsque les personnages arrivent, Mirt leur offre une tournée et partage la rumeur de trois épouvantails animés terrorisant les fermes avoisinantes. Il exprime un certain scepticisme, notant que "les gens d'en bas sont enclins à répandre de telles histoires". Mirt demande que le groupe se rende à l'*Auberge de la Moisson*, dans le village de Sousfalaise, pour enquêter. Le propriétaire de l'auberge, Anselm Griggons, est vaguement affilié aux Ménestrels.

### L'ALLIANCE DES SEIGNEURS

La guerre des gangs entre les Zhentarim et la guilde de Xanathar a épuisé les ressources de Jalester Silvermane. Travaillant tard dans la nuit, il délègue à la hâte les prétendus épouvantails animés aux aventuriers, en griffonnant sur un bout de papier "Trio d'épouvantails vivants et terrorisant Sousfalaise ? Allez-y. Faites un rapport. -Silvermane". La note est livrée le lendemain matin par une jeune coursière.

## LES ÉPOUVANTAILS PRENNENT VIE

Les épouvantails sont monnaie courante à Sousfalaise. Presque tous les champs que le groupe croisent sont surveillés par un ou plusieurs épouvantails. Pointez au moins un groupe de trois épouvantails lorsque les personnages passent devant une ferme sur la route du village de Sousfalaise. Ils restent complètement immobiles et paraissent inoffensifs.

Rendez banales les observations d'épouvantails. Peut-être en voir un au loin qui bouge, seulement agité par le vent. Envisagez de rendre vos joueurs paranoïaques à chaque observation, ou de les bercer dans la complaisance.

Les trois épouvantails animés par Basil Mureal sentent que les personnages sont de dangereux alliés de Waterdeep et ils ont jeté leur dévolu sur le groupe.

L'aventure fournit des suggestions quant au moment où les épouvantails animés pourraient attaquer, mais c'est finalement à vous qu'il revient de décider quand ils feront leur apparition.

### RENCONTRE AVEC UN ÉPOUVANTAIL

Les **épouvantails** (voir MM p.128) peuvent attendre que les personnages s'approchent, après s'être mis en route vers un objectif. Si les personnages sont proches, les épouvantails peuvent tenter de s'approcher furtivement du groupe. Dans tous les cas, dès que les personnages se trouvent à moins de 9 mètres, ils commencent à se battre avec leur *Regard terrifiant*, puis se déplacent au corps à corps en faisant des attaques avec leurs griffes. Vérifiez si les personnages sont surpris lorsque le combat commence. Les trois épouvantails concentrent leurs attaques sur le personnage le plus faible et se battent jusqu'à la mort.

### SOUS LA FALAISE

Des dizaines de fermes sont dispersées le long de la Route de Sousfalaise, la longue route qui serpente à travers la prairie ouverte et vallonnée à l'est de Waterdeep, reliée à la Grand-route au nord et au sud où elle quitte la ville.

Décrivez l'environnement en fonction de la saison où se déroule votre campagne. Au printemps, les travailleurs labourent et sèment les champs, le bétail paît sous le chaud soleil de l'été, de hautes rangées de maïs et d'orge sont prêtes pour la récolte d'automne, et le rude froid hivernal révèle une terre stérile couverte de neige.

> **Le climat politique à Sousfalaise**
>
> *La ville de Waterdeep a connu un redressement spectaculaire dans le peu de temps qui s'est écoulé depuis la fin de la Fracture. Sous la direction de Laerel Silverhand, la plupart des familles nobles ont retrouvé leur richesse, et le niveau de vie de tous les habitants de Waterdeep, à l'exception des plus pauvres, n'a cessé d'augmenter.*
>
> *La plupart des Sousfalaisiens, comme ils se désignent eux-mêmes, se sentent exclus de cette prospérité. Ils nourrissent la ville, mais mènent une vie agraire difficile, remplie de travail manuel et de peu de commodités. Ils entendent les citoyens de Waterdeep les appeler "les gens du dessous" et se moquent de leur lent accent nasal. Ils leur rendent ce mépris en retour, ridiculisant même les travailleurs de la ville comme des "élites" sur une montagne de richesses qui ne comprennent pas ce que signifie vivre comme un "vrai" citoyen du Nord.*
>
> *Graz'zt a trouvé facile de puiser dans ce ressentiment et de former un petit culte servant son objectif d'affaiblissement d'Asmodée. Bien que le nombre de ceux qui se consacrent au culte soit peu élevé et secret, leur idéologie politique, basée sur la supériorité raciale des humains et des halfelin (la grande majorité de la population de Sousfalaise) et le désir de vengeance économique et culturelle contre la ville, se répand rapidement.*

### LA FOULE EN COLÈRE

Alors que le village de Sousfalaise apparaît au loin, le groupe remarque qu'un grand nombre de personnes se sont rassemblées près d'un chêne isolé sur le bord de la route.

Un tieffelin à l'allure terrifiée est assis sur le côté d'un cheval non sellé. La corde qui est nouée autour de son cou est attachée, avec un peu de mou, à une grosse branche de l'arbre qui se trouve au-dessus de lui.

Vance Lifften (**fanatique de secte humain LM**, armé d'une épée courte plutôt que d'un poignard) est assis sur la selle d'un cheval à côté du condamné à la pendaison, flanqué de trois **bandits** (voir MM p.343). Les quatre hommes sont humains. La foule en colère qui se trouve devant eux est un mélange à 70/30 d'humains et de halfelin, tous mâles.

Lifften est le chef du culte de Graz'zt, l'Ordre Impitoyable de la Main aux Six Doigts, mais personne n'est au courant de ce fait. Bien qu'il possède de la magie noire, il ne prendra pas le risque de trahir le secret en jetant des sorts. Une paire de gants à six doigts est cachée dans sa poche arrière.

Lifften est actuellement en train d'aranguer la foule. Lorsque le groupe approche, ils entendent la fin de son discours :

> *...et les monstres engendrent d'autres monstres ! Z'avez tous vu ce qui se passe ces derniers temps. Les élites en haut de la ville encouragent ce mal à s'épanouir ... mais pas ici, au cœur de notre pays !*

Le discours est suivi de quelques cris de la foule, par exemple "Mort au monstre !".

Lifften voit les personnages s'approcher alors que son discours s'achève et fait un signe de tête à ses sbires, qui se déplacent dans des directions opposées pour flanquer le groupe. Il remarque qu'un personnage est un Sangdragon, un drow, un demi-orc, un tieffelin, ou de toute autre race monstrueuse, à moins qu'il ne porte une cagoule et ne réussisse un test de Charisme (Supercherie) DD 12 ; ou qu'il n'ait utilisé la magie ou un kit de déguisement pour cacher sa véritable race (aucun test requis).

S'il a repéré un personnage de l'une de ces races, Lifften s'écrie : "Eh bien, regardez ici. C'est notre jour de chance ! Il semble que nous allons avoir une double (ou triple etc.) justice !" Ses hommes de main se dirigent vers le groupe dans le but de capturer tout personnage de ces races. La foule en colère s'engage dans un combat de niveau de mentalité 3.

Un groupe considéré comme n'étant composé que de races socialement acceptables ne sera pas attaqué, à moins qu'il n'intervienne physiquement dans le lynchage.

Les personnages pourraient essayer de persuader la foule de ne pas pendre le tieffelin. Ils ne convaincront pas Lifften, mais ils peuvent retourner le sentiment de la foule. Lorsqu'un personnage plaide en faveur d'épargner la vie du tieffelin, demandez-lui de faire un test de Charisme (Persuasion). Ce test est effectué avec un avantage pour un clerc ou un paladin qui invoque le nom d'un dieu, mais avec un désavantage pour tout personnage qui n'est pas humain ou qui est à moitié humain. Ce test est contesté par un test équivalent de Lifften (+2), qui présente ces arguments, dans l'ordre :

* "Cette chose est un démon, pas un homme !"
* "Qui sait quelles autres créatures cauchemardesques il créera avec sa magie noire ?  Les épouvantails ne sont sûrement qu'un début".
* "Comment Chauntea pourrait-elle à juste titre bénir les récoltes de ceux qui tolèrent ce mal ?" Ce test est effectué avec un avantage.

Le niveau de mentalité de la foule commence à 3, ce qui est son maximum. Lorsque les personnages gagnent un test, réduisez le niveau de un. Si Lifften gagne, augmentez-le d'une unité.

Après trois tours d'argumentation, Lifften se lasse du débat et frappe la croupe du cheval adjacent, l'envoyant courir et le tieffelin se balancer.

Si le groupe tente de sauver la vie du tieffelin, le combat s'ensuit. Notez la mentalité actuelle de la foule au début du combat. Si le tieffelin n'est pas libéré de la corde, il meurt étouffé à la fin du cinquième round de combat.

Lorsqu'il est sauvé, le tieffelin s'identifie comme Falados. Émigrant à Waterdeep avec seulement les vêtements qu'il portait sur le dos, il a été agressé par l'homme à cheval et ses bandits. Falados exprime son profond désarroi face à ce qui vient de se passer - sa décision de voir une nouvelle vie à Waterdeep a été motivée par le fait qu'il avait compris que son espèce y était acceptée.

Falados ne veut pas le dire tout de suite, mais il laisse entendre plus d'une fois qu'il souhaite que le groupe l'escorte jusqu'à Waterdeep. S'ils refusent, il s'enfuit en courant vers la porte de la Rivière.

Si les personnages n'interviennent pas, ils regardent l'homme innocent rendre son dernier souffle. La foule se disperse paisiblement et le cadavre du tieffelin est laissé pendu à l'arbre.

**La foule en colère**

**Classe d'armure** : 9 (sans armure)  
**Points de vie** : 4 (1d8), **seuil de dommages** 4 (identique)  
**Vitesse** : 9m **Allonge** : 1,5m

La foule occupe un cercle de 12 mètres de diamètre. Toute créature de taille moyenne ou plus petite peut occuper un espace à l'intérieur de ce cercle, mais il est considéré comme dans un terrain difficile.

**Niveaux de mentalité de la foule**

**niveau 0.** Apaisé. La foule est bouche bée pendant le combat, mais ne fait aucune action ou réaction.

**A un niveau supérieur à 0** : toute créature hostile qui termine son tour à la portée de la foule doit effectuer un jet de sauvegarde de Force DD 14 ou se faire projeter à terre. La foule peut réagir à n'importe quel moment au cours d'un round de combat pour lancer une attaque d'opportunité contre toute créature qui quitte son rayon d'action.

**Niveau 1**. Agacée. La foule fait une seule attaque de Bousculer (+5) contre chaque ennemi à portée, en essayant de projeter à terre. Si la cible est déjà dans l'état *à terre*, elle est également dans l'état *empoigné* (fuite DC 14).

**Niveau 2**. Furieuse. Même chose que le niveau 1, mais la foule fait également une attaque de coup de pied supplémentaire contre toute créature *empoignée*, +5 pour toucher, dégâts 1.

**Niveau 3**. Frénétique. La foule se déplace vers l'ennemi le plus proche et effectue une attaque sournoise, +0 pour toucher, dommages 6 (1d4 + 1d6) contre chaque ennemi à portée.

Si la foule subit 4 dommages ou plus lors d'une seule attaque, un des roturiers (MM 345) qui la composent a été tué. Les autres roturiers utilisent leur réaction pour fuir.

### AUBERGE DE LA MOISSON

Le cœur du village de Sousfalaise est l'Auberge de la Moisson. C'est le plus grand bâtiment à des kilomètres à la ronde, construit à un angle suivant une courbe de la rue principale au centre du village. La vaste salle des fêtes et la salle à manger au niveau principal servent de centre social de Sousfalaise, et le deuxième étage est divisé en six chambres d'hôtes spacieuses et un logement privée.

Le propriétaire, Anselm Griggons, est un homme d'âge moyen avec des cheveux roux flamboyants et une barbe assortie. Né et éduqué à Waterdeep, il a passé une dizaine d'années d'aventures avant de "prendre sa retraite" pour s'installer dans le pays. Une grande partie du succès de l'auberge peut être attribuée à la richesse accumulé durant la carrière précédente d'Anselm, ce qui lui a permis de fonctionner sans faire de profit.

Lorsque les personnages arrivent, Anselm est debout derrière le bar et lit un journal. Un personnage qui l'examine remarquera qu'il s'agit d'une édition du Popotin de Waterdeep datant de plus d'une décade.

Lorsque les personnages s'informent sur les épouvantails, un adolescent halfelin assis au bar, mangeant un bol de ragoût délicieusement odorant, s'interrompt avant qu'Anselm ne puisse placer un mot. "*Je l'ai vu moi-même, messieurs, plus grand que vous tous, avec une grosse citrouille pourri pour tête ! Il m'a poursuivi sur la route principale pendant un peu plus de 400 mètres avant que je n'arrive assez loin devant lui et il a tourné pour aller chercher une génisse à la place. J'ai continué à courir jusqu'à la maison !*"

Anselm confirme qu'il a entendu un certain nombre d'histoires similaires et qu'il les croit vraies. "*J'ai commencé à entendre ces contes il y a cinq ou six jours avec trois variations distinctes. Celle à tête de citrouille qui a pris en chasse le jeune Wessel, une autre avec un sac de toile de jute sur la tête et une autre vêtue de haillons rouge vif. Les histoires ont commencé à peu près en même temps que les meurtres commis à la Ferme de la Rédemption. Ça semble être une sacrée coïncidence.*"

Tout ce qu'Anselm sait des meurtres, c'est que trois détenus se seraient mutuellement frappés la tête avec des pierres ("*...ou leur propre tête ? Ça aurait pu être un pacte de suicide, je suppose*").

Si un personnage associé aux Ménestrels révèle qu'ils ont été envoyés par Mirt et montre à Anselm leur épingle en forme de harpe, Anselm leur **donne un parchemin d'héroïsme** (voir PHB p.250). Il met en garde contre le fait que dans certaines histoires d'épouvantails, on se sentirait paralysé par la peur en en regardant un.

Anselm dispose de chambres à louer si les personnages le souhaitent. A seulement 10 pa, repas compris, c'est une offre exceptionnelle pour ce niveau de confort.

Si les personnages passent la nuit dehors à un moment donné de l'aventure, cela pourrait être une bonne occasion d'organiser la rencontre avec les épouvantails animés.

### TAVERNE DE L'HEURE DE LA CHOPE

Directement en face de l'Auberge de la Moisson se trouve un établissement de boisson dégoûtant qui doit sa survie au seul fait qu'Anselm Griggons ne tolère pas le comportement de ce type de clients dans son propre établissement. Quelle que soit l'heure de la journée, les clients de l'Heure de la Chope sont impolis, grossiers, enclins à se battre et très ivres.

Le propriétaire, Bartholomew Ender, incarne le lieu. Il ne sert les elfes et les nains qu'à contrecœur, tout en faisant des remarques hostiles sur les "oreilles pointues" et les "dames à barbe". Les autres races sont carrément interdites dans les locaux.

Deux Zhents, Morlan Frost et Dirk Starnag (bandits humains) vivent pratiquement ici. Ils n'ont aucune association directe avec les Pillards de Tenure ou Manshoon, mais vendent leurs services de mercenaires à quiconque est prêt à les payer 3 pa par jour chacun. Ils ne travaillent qu'à deux et si l'un d'eux est loyal, c'est uniquement envers l'autre. Si les choses tournent mal, ils sont prompts à abandonner leur poste.

## COUP À LA TÊTE

La Ferme de la Rédemption se trouve juste au nord de la falaise, près de la porte nord de Waterdeep. Les criminels condamnés aux travaux forcés (voir le code juridique W:VdD p.222) travaillent dans les champs et les vergers pour rembourser leur dette envers la société. La plupart des prisons sont de très faible sécurité, car la population y purge principalement de courtes peines pour des vols de faible gravité. Bien que beaucoup s'évadent facilement, pour la plupart, cela ne vaut pas la peine de s'exiler définitivement de Waterdeep.

Un petit groupe de prisonniers purge des peines plus longues pour des crimes graves tels que le meurtre ou la participation à la traite des esclaves. Ces détenus sont plus lourdement surveillés et résident dans des baraquements situés dans une section clôturée de l'extrémité nord-ouest de la prison.

Cette zone de haute sécurité est celle où Basil Mureal travaillait comme gardien et où il a trouvé ses trois victimes. Il y a six jours, il a eu l'occasion de se retrouver seul avec elles dans l'un des dortoirs, chacune étant attachée après une dure journée de travail dans le verger. Mureal a frappé le crâne de chaque prisonnier avec le bout arrondi de son marteau, puis s'est enfui chez lui pour cacher l'arme. Il est revenu avec trois grosses pierres, les a tamponnées de sang et de cervelle, a libéré les cadavres de leurs menottes, et a couru vers son supérieur pour lui signaler que les prisonniers s'étaient entretués dans une sorte d'altercation.

### ENTRER DANS LA PRISON

Le groupe est accueillie à l'entrée principale de la prison par un gardien halfelin nommé Dandel Groslapin. Il informe les personnages que le jour de visite était hier, et que le groupe devra revenir à la prochaine décade.

Lorsqu'on lui dit que les personnages sont là pour enquêter sur les récents meurtres, Dandel escorte immédiatement un groupe affilié à l'Alliance des Seigneurs ou à Griseforce au bureau du directeur de la prison. Sinon, un test réussi de Charisme (Persuasion) DD 14 ou un pot-de-vin d'au moins 5 po est nécessaire pour le groupe ne soit pas refoulé. Si le test échoue, Dandel, pas si subtilement, demande le pot-de-vin en toussant légèrement tout en faisant tinter le porte-monnaie à sa ceinture.

### ENQUÊTER SUR LES MEURTRES

La directrice de la Ferme de la Rédemption, Emmeline Crater, est dans son bureau quand le groupe arrive et est heureuse de les recevoir. Elle a secrètement de sérieux doutes sur l'histoire de Mureal et soupçonne qu'il a assassiné les prisonniers. Crater espère qu'elle pourra faire porter aux aventuriers la responsabilité du résultat d'une enquête. L'Ordre fraternel des gardiens de la paix exige avant tout la loyauté envers ses collègues, même si un crime a été commis. Dans une affaire impliquant un prisonnier, ne pas se ranger du côté d'un membre de la guilde serait particulièrement impensable. Si les personnages parviennent d'une manière ou d'une autre à fouiller son bureau, ils trouvent une lettre adressée au directeur de la prison minière du Labeur de l'Expiation, le long de la piste de Troisanglier dans les Monts des Epées, au nord. Cette lettre félicite Mureal pour ses services et lui recommande un transfert et une promotion - l'approche de la guilde pour faire face à ce genre de situation.

Crater appelle l'officier de haute sécurité, Villhelm Price, dans son bureau pour répondre aux questions du groupe. Price, tout comme Mureal, est un membre de haut rang de la secte de Graz'zt, mais il n'a aucune connaissance directe des meurtres ou des actions récentes de Mureal. Il a une très mauvaise opinion des prisonniers qu'il garde et a pris l'explication de Mureal au pied de la lettre. Si les aventuriers ont des soupçons sur Mureal, il devient de plus en plus irritable et sur la défensive.

**Parler à Mureal**. Basil Mureal a été mis en congé payé pour une décade à la suite de l'incident, ce qui est une procédure normale. Price ne l'a pas vu depuis l'incident, mais il soupçonne que Mureal est chez lui, dans son cabanon, en train de se remettre du traumatisme causé par son arrivée sur les lieux de l'incident. Price est heureux de donner au groupe les indications pour se rendre au domicile de Mureal, qui est couvert dans la section ci-dessous, Hors de Sa Peau.

**Médecins légistes amateurs**. Si les personnages souhaitent inspecter un corps, ils ont de la chance. Deux des victimes étaient originaires de Waterdeep et sont actuellement enterrées parmis leur famille dans la Cité des morts, mais un homme est originaire de la Porte de Baldur, et son corps attend d'être transporté. Price conduit les personnages vers un hangar de stockage contenant une boîte en pin dans laquelle se trouve le corps, généreusement enveloppé de sel.

Un test réussi d'Intelligence (Investigation) DD 14 révèle que l'unique coup profond qui a frappé le crâne et le cerveau de l'homme a été porté par un instrument émoussé et arrondi ne faisant probablement pas plus de 7 centimètres de diamètre.

Parler avec un mort ou toute autre magie nécessitant l'esprit du défunt n'a aucun effet, car l'esprit de l'homme anime maintenant un épouvantail.

**La scène du crime**. Le dortoir de 6 mètres sur 9 où les meurtres ont eu lieu est resté vide et intact depuis que les corps ont été enlevés. Vieillissant et devant être remplacé l'année suivante, il est prévu de le démolir. Un personnage qui réussit un test d'Intelligence (Investigation) DD 12 remarque que des empreintes de pas ensanglantées apparaissent à la fois vers et depuis l'entrée du bâtiment, ce qui, selon Price, provient des corps enlevés. Un test réussi révèle également que les éclaboussures de sang n'apparaissent que sur le mur est du bâtiment.

## HORS DE SA PEAU

La dernière décade a été la plus mouvementée de la triste vie de Basil Mureal. Parmi tous les membres du culte de Graz'zt que lui et ses amis ont baptisé le Gant Impitoyable de la Main aux Six Doigts, le seigneur démoniaque l'avait choisi. Conformément aux instructions, Mureal a enchanté son marteau à panne ronde et l'a utilisé pour tuer les trois prisonniers. Avec le même marteau, il construisit trois épouvantails animés par les esprits des prisonniers.

Le lendemain du déchaînement de ses épouvantails sur Sousfalaise, Basil Mureal a reçu un autre édit du Seigneur des Ténèbres. Graz'zt offre à Mureal l'opportunité de devenir un être immortel en échange de son corps mortel. Mureal sauta sur l'occasion et accomplit le rituel prescrit par Graz'zt. À la pleine lune, les muscles, les os et les viscères de Mureal se sont décomposés, le transformant en démon babau. La corne et la queue du babau traversaient la peau humaine de Mureal, qui s'est effondrée en un tas sur le sol lorsque le démon est apparu. Le babau a escaladé le mur jusqu'à la seule fenêtre du cabanon située au-dessus et a disparu dans la nuit à la recherche des tunnels perdus dans la falaise, reliant Sousfalaise à Waterdeep.

### LE CABANON DE BASIL MUREAL

Le cabanon de Mureal se trouve à environ 1,5 km au sud, sur la même route que la Ferme de la Rédemption. Il est petit et se compose d'une seule pièce de 6x6 mètres contenant un poêle en fer, une table à manger et un bureau. Une échelle mène à un grenier situé au sommet du bâtiment, qui contient le lit de Mureal, une petite commode et la seule fenêtre de la maison. De l'autre côté du chemin de terre se trouvent trois épouvantails. Les aventuriers localisent facilement la maison à partir des indications et de la description de Villhelm Price.

**Porte piégée**. La porte de la maison de Mureal est fermée à clé. Elle peut être ouverte avec un test de dextérité DD 10 réussi avec des outils de voleurs ou forcée avec un test de Force (Athlétisme) DD 15 réussi.

Avant d'entreprendre son rituel de transformation, Mureal a placé deux flacons de feu grégeois (voir PHB p.151) sur une étagère au-dessus de la porte, reliés par de la ficelle fixée du côté intérieur de la porte. Lorsque la porte est ouverte, les fioles sont éjectées de l'étagère et se brisent sur le sol, projetant la substance collante en un cercle de 3 mètres de rayon centré au milieu de l'embrasure de la porte. Tout personnage dans la zone doit réussir un test de sauvegarde de Dextérité DD 14 ou être éclaboussé par le fluide et s'enflammer.

Un personnage enflammé de cette manière prend 1d4 dégâts au début de chacun de ses tours jusqu'à ce qu'il puisse faire un test de Dextérité DD 10 réussi pour éteindre les flammes.

Le piège met également le feu au chalet. Les personnages peuvent facilement éteindre le feu, mais s'ils ne le font pas, la structure brûlera au point qu'aucun indice utile ne pourra être trouvé après 5 minutes. Pensez à cette occasion pour faire attaquer les épouvantails animés.

Tout personnage qui regarde par la fenêtre au sommet du bâtiment peut repérer le piège, sans qu'il y ait besoin d'un test. Le piège peut être désarmé de l'intérieur du chalet, avec la porte toujours fermée, en faisant un test réussi de Dextérité DD 5. En cas d'échec, les flacons tombent au sol comme si le piège avait été déclenché.

**Restes du rituel**. Tout personnage qui regarde à l'intérieur du chalet remarque immédiatement un tas de peau rose tachée de sang qui se trouve au milieu d'un cercle dessiné à la craie. Autour de ce cercle, écrit en abyssal, on peut lire les mots "Comme Glasya a versé son sang, je deviendrai moi aussi". Un personnage qui peut lire ces mots ou les faire traduire peut faire un test d'Intelligence (Histoire). Un résultat de 15 ou plus révèle qu'il s'agit d'une référence à l'histoire du démon babau (voir page 136 du Guide des monstres de Volo). Le personnage est également conscient des résistances aux dommages du babau, de sa capacité à créer des ténèbres magiques, de son *regard affaiblissant*.

L'examen de la peau détachée révèle qu'elle a été tranchée de l'intérieur comme avec deux lames dentelées ; l'une provenant du sommet du crâne et l'autre du coccyx. Déplacer la peau révèle le dessin à la craie d'une main à six doigts.

Juste à côté du cercle se trouve un marteau à panne ronde (voir encadré, le marteau à panne ronde de Mureal) trempé dans du sang séché et incrusté de morceaux de bois et de paille. Tout personnage maîtrisant les arcanes éprouve immédiatement un sentiment de malaise lorsqu'il le regarde.

**Recherches de quelques minutes**. Une recherche dans la maison de Mureal révèle les objets banals auxquels on peut s'attendre, et rien de valeur. La poche de la veste de Mureal, qui est posée sur une chaise, contient deux gants en cuir à six doigts, avec une petite carotte placée dans le sixième doigt, à côté du petit doigt.

Sur le bureau se trouve un journal à reliure de cuir avec des procès-verbaux relatant les réunions du Gant Impitoyable de la Main aux Six Doigts. Les réunions ont été convoquées une heure après le coucher du soleil, le même jour pour chacune des dernières décades. La dernière entrée (un document est inclus à la fin de cette aventure) est datée d'une décade précédent demain. Si les joueurs posent des questions sur les autres pages du journal, c'est le même genre de choses. La réunion n°9 marque la dernière fois que les adeptes du culte ont entendu "LA VOIX", qui leur a demandé de concentrer leurs efforts sur la recherche d'un tunnel perdu dans les parois de la falaise. Les références antérieures à "LA VOIX" sont des missives sur la supériorité des humains et des halfelins, et les dangers des autres races et de la ville d'en haut qui les tolère.

En lisant le compte-rendu de la réunion n°14, les personnages doivent apprendre que le lieu des réunions est une grange abandonnée de l'Hydromellerie et vergers des Snobeedle. Si les joueurs ne le savent pas, leurs personnages peuvent le savoir grâce à un test réussie d'Intelligrence (Histoire) DD 10.

Les personnages pourraient également noter que Basil Mureal est le secrétaire de la secte, et que les initiales du vice-président, V.P. correspondent à celles de Villhelm Price de la Ferme de la Rédemption. Ils n'ont peut-être pas trouvé son nom lorsque le personnage a été lynché, mais le président V.L. était le meneur, Vance Lifften.

**Le marteau à panne ronde de Mureal**

*Arme (marteau léger)*

Vous obtenez un bonus de +1 aux jets d'attaque et de dégâts effectués avec cette arme magique.

Lorsque le marteau à panne ronde de Mureal est utilisée pour tuer un humanoïde maléfique, son esprit se manifeste immédiatement sur le plan matériel sous la forme d'un **dretch** (voir MM p.51), qui semble jaillir du corps de la victime. Le dretch est hostile à toutes les créatures, sauf au porteur du marteau de Mureal. Lancer l'initiative pour le dretch. Le dretch poursuit et attaque le non-démoniaque le plus proche au mieux de ses capacités.

**Construction du Mal (nécessite une harmonisation par un personnage maléfique)**

Lorsqu'un personnage harmonisé avec le marteau de Mureal tue un humanoïde maléfique avec son arme, il peut choisir de capturer l'esprit plutôt que de le voir se manifester sous forme de dretch. S'il le fait, il peut ensuite passer une heure à utiliser le marteau pour construire un **épouvantail** (voir MM p.128), le liant à l'esprit capturé.

## QUELQUES MAUVAISES POMMES

L'Hydromellerie et vergers des Snobeedle est située à l'est de la Route de Sousfalaise, à un peu moins d'un kilomètre au nord du village de Sousfalaise. Le verger, planté de diverses variétés de pommes, prunes, cerises, pêches et abricots, est en synergie avec la grande colonie d'abeilles qui fournit le miel de l'hydromellerie.

La saison où se déroule votre campagne peut ajouter un peu de saveur au lieu. Par exemple : les bourgeons et les abeilles au printemps, les travailleurs d'été qui taillent, l'abondante récolte de l'automne, une salle de réception remplie de fermiers qui s'ennuient en plein hivers.

### LA SALLE DE DÉGUSTATION

Les cousins Snobeedle sont des hôtes attentifs aux invités dans leur vaste salle de dégustation, qui est dimensionnée pour accueillir des invités de taille humaine. Outre le célèbre hydromel, la carte des boissons contient pas moins de sept variétés de cidre. Le vin et la bière sont absents de la liste car ils ne sont pas produits sur place. Le choix de plats est limité, mais tout à fait délicieux. Les Snobeedle font cuire de grandes quantités de pain blanc moelleux dans la maison, qui est servi avec du miel et un large éventail de confitures, qui peuvent également être achetées en pot dans une petite boutique.

Les humains et les halfelins sont accueillis chaleureusement par tous, les elfes et les nains le sont moins mais sont toujours traités poliment, et les races "indésirables" mentionnées plus haut dans l'aventure sont tolérées mais reçoivent un accueil mitigé. Les personnages de ces races font des tests de Charisme avec désavantage à l'intérieur de la salle de dégustation.

La salle est généralement très fréquentée, et si les aventuriers prennent le temps de la visiter ou simplement d'aller y faire un tour, ils entendent deux sujets de conversation populaires (pas besoin de tests) :

* **Les épouvantails**. Les aventuriers n'entendent pas un récit de première main, mais beaucoup d'histoires de seconde ou troisième main avec des niveaux d'embellissement croissants. Le point commun semble être qu'il y a trois épouvantails animés. L'un est vêtu de haillons, l'autre d'un sac en toile de jute sur la tête avec des trous pour les yeux, et l'autre dont la tête est une citrouille géante.
* **Meurtres à la Ferme de la Rédemption**. Le consensus semble être que trois dangereux condamnés se sont disputés et se sont entretués, bien qu'au moins un halfelin affirme que le responsable pourrait être un gardien ("et je ne lui en veux pas si c'était le cas ! Imaginez le stress de devoir surveiller ces misérables hommes !"). Il existe également un sentiment général selon lequel la prison ne devrait pas exister là où elle se trouve. Elle est en concurrence avec les fermes locales qui n'ont pas de main-d'œuvre gratuite, et pourquoi la vie de leurs familles devrait-elle être menacée par les éléments criminels produit par la ville ?

### INVITATION DE LA PART D'UN CULTISTE

Si les personnages n'ont pas découvert les réunions du culte dans le journal du cabanon de Basil Mureal, ils sont plutôt approchés par un halfelin nommé Quentin Snobeedle. Comme membre de la secte, Q.S. a identifié le groupe comme une menace et tente de les attirer dans une embuscade à la grange abandonnée.

Blond de cheveux et de barbe, à la fois clairsemé et mal soigné, Q.S. est l'un des nombreux cousins qui tiennent l'hydromellerie. Ce n'est cependant pas un des propriétaires et il ne s'intègre jamais vraiment dans le comportement joyeux du reste de la famille. En tant qu'étranger, même parmi ses propres parents, Q.S. trouve très attrayante cette contre-culture avec des ennemis bien définis et des promesses de pouvoir.

Si les personnages essayaient de sauver le tieffelin lynché, Q.S. était dans la foule. Il s'approche du groupe en les félicitant pour leur bravoure. Si les personnages ont laissé pendre le tieffelin, Q.S. était dans la Maison de la Moisson et a entendu le groupe s'enquérir des épouvantails. Dans ce cas, il approche les personnages en expliquant avoir des soupçons sur la source d'information sur les épouvantails.

Dans les deux cas, Q.S. invite le groupe à visiter l'hydromellerie le soir de la réunion de la secte. Il est un hôte aimable à l'intérieur de la salle de dégustation, et trouve une excuse pour conduire le groupe dans la grange abandonnée à l'heure qui suit le coucher du soleil.

Les tactiques expliquées ci-dessous supposent que les adeptes du culte n'attendent pas les aventuriers. Dans le cas où les adeptes préparent une embuscade, les tactiques sont généralement les mêmes, à quelques exceptions près. Le fanatique de culte lancera un bouclier de la foi avant le début du combat, ce qui lui permettra de déployer son arme spirituelle un tour plus tôt. Bien que les membres du culte soient prêts avec leurs cimeterres, ils restent des combattants non entraînés et indisciplinés.

### TRAQUER LA SECTE

Si le groupe arrive à l'hydromellerie avec l'intention de rechercher la grange abandonnée, elle n'est pas difficile à trouver (voir Grange abandonnée, ci-dessous). Les Snobeedles permettent à n'importe qui de se promener dans la propriété comme bon lui semble. À la fin de l'été et en automne, le terrain est plein de clients qui transportent des sacs de fruits fraîchement cueillis.

Si les personnages se renseignent sur les sectes ou les démons dans la salle de dégustation, ils sont accueillis avec un pur scepticisme. Une telle perversion doit sévir en ville, bien sûr, mais sûrement pas dans cette zone plus civilisée. Il y a cependant une exception. Lors d'un test de Sagesse (Perception) DD 16 réussi, un personnage remarque que Quentin Snobeedle est devenu encore plus pâle en entendant parler de démons ou de sectes, et montre des signes visibles de nervosité.

Si le groupe confronte Q.S., il est clair qu'il cache quelque chose. Un test réussi de Charisme (Persuasion) DD 10 est suffisant pour obtenir quelque chose de lui. Il bégaie un mensonge peu convaincant sur le fait d'avoir entendu des étrangers de la ville parler de rituels d'animation d'épouvantails dans le sous-sol de la Maison de la Moisson du village de Sousfalaise. Un personnage exprimant un doute peut déterminer que le halfelin n'est pas sincère en réussissant un test de Sagesse (Perspicacité) DD 12.

Q.S. est faiblement armé sans ses amis pour le soutenir. Si un personnage menace le halfelin, faites-lui faire un test de Charisme (Intimidation) DD 14 :

* sur un succès, Q.S. se retourne contre la secte pour tenter de sauver sa propre peau : "Écoutez, je ne voulais pas être mêlé à ça ! Tye (Tye Tosscobble, T.T. du compte-rendu de la réunion) m'y a forcé. Vous ne pouvez pas le dire à mes cousins. L'entreprise familiale serait dévastée !" Q.S. informe les personnages de la date et du lieu de la prochaine réunion de la secte et accepte d'aider le groupe de toutes les façons qu'ils lui demandent. Un test réussi de Sagesse (Perspicacité) DD 18 révèle qu'il est fermement décidé à se plier à la volonté du camp qui sortira vainqueur, et qu'il n'hésitera pas à trahir le groupe.
* sur un échec, Q.S. feint l'offense : "Ma famille accueille tous ceux qui souhaitent profiter de notre hospitalité, mais, personnellement, je ne tolérerai pas une telle impolitesse. Bonne journée !". Q.S. s'en va va vaquer à ses occupations et s'assure de rattraper les autres adeptes avant la réunion pour les avertir de la présence du groupe.

### LA GRANGE ABANDONNÉE

L'une des structures originales construites il y a plusieurs décennies par la famille Snobeedle se trouve toujours à l'extrémité nord-est de la propriété, à côté d'une vieille parcelle de pommiers qui ont depuis longtemps cessé de produire des fruits.

La grange fait 12 mètres de large sur 24 mètres de long, bien que les 9 mètres arrière se soient effondrés et soient complètement inutilisables. L'ouverture de 6 mètres sur 6 mètres à l'avant est ouverte sur le monde, les portes ayant depuis longtemps pourries sur leurs gonds. De nombreuses ouvertures de la taille d'un halfelin sont visibles dans le bois des murs latéraux de la grange.

Une échelle branlante de 6 mètres mène à un grenier de 4,5 mètres de profondeur au-dessus de la porte et qui couvre toute la largeur de la grange. Les deux sont dangereux après des décennies de négligence. Toute créature traversant l'une ou l'autre pendant son tour doit faire un test de Dextérité (Acrobaties) DD 12. En cas d'échec, la structure entière s'effondre sous son poids et elle tombe plus bas sur sol, subissant 2d6 dommages contondant et atterrissant à plat ventre parmi les débris.

La grange offre de nombreux endroits où se réfugier, notamment un chariot sans roues dont l'essieu est cassé et de nombreuses grosses balles de foin (qui sont dangereusement sèches et peuvent facilement s'enflammer).

De vieilles pelles, houes et autres outils agricoles rouillés sont éparpillés sur les bords de la grange, pouvant servir d'armes improvisées.

Une douzaine de tabourets de traite, tous capables de supporter le poids d'une personne assise, sont disposés en cercle au milieu de la zone utilisable de l'étable.

Lorsque le culte se réunit, l'intérieur de la grange est faiblement éclairé par des bougies placées en toute sécurité à l'intérieur de pots de confiture en verre.

### 96 DOIGTS GANTÉS IMPLACABLES

Peu après le crépuscule, les membres de la secte, portant tous des gants de cuir à six doigts avec une carotte jouant le rôle du doigt supplémentaire à l'intérieur, arrivent à la grange pour la réunion. Juste assez de bougies sont allumées pour fournir une faible lumière à l'intérieur de la grange, sans pour autant attirer l'attention de l'extérieur.

Vance Lifften, qui a mené le lynchage du tieffelin au début de l'aventure, est un **fanatique de secte** (voir MM p.347) et le chef des 7 **membre de secte** (voir MM p.348) restants maintenant que Mureal n'est plus parmi eux. Le fanatique de secte est clairement identifié comme le chef par les vêtements fantaisistes qu'il porte, par rapport aux simples robes noires des autres membres de la secte.

Si Lifften a été tué, faites plutôt de Villhelm Price (de la prison) le fanatique de culte. Si les deux hommes sont morts, choisissez un autre membre pour jouer le rôle. S'il était nécessaire de passer à la doublure, réduisez le nombre de membres en conséquence. Notez que Q.S. est inclus dans leurs rangs, et son rôle peut avoir changé en fonction de ses interactions avec les aventuriers.

N'oubliez pas que si un membre mauvais du culte est tué par le marteau de Mureal pendant la rencontre, un dretch malodorant (voir MM p.51) éclatera du cadavre du membre tué !

**Embuscade des cultistes**. Si le groupe n'a pas appris l'existence de la réunion grâce aux indices dans le cabanon de Mureal et qu'il est plutôt mené à la grange par Q.S., un guetteur avertit que le groupe approche. Les membres de sectes se positionnent pour tendre une embuscade aux aventuriers, et le fanatique de secte se prépare au combat en jetant *bouclier de la foi*.

Faites un seul test de Dextérité (Discrétion) pour les membres de culte. Cela fixe la difficulté pour que les aventuriers remarquent l'embuscade lorsqu'ils entrent dans la grange. Si le DD est supérieur au score de Sagesse (Perception) passive d'un personnage (-5 en raison du désavantage dû à la faible lumière), il est *surpris* et ne peut pas bouger ou entreprendre des actions ou des réactions pendant le premier tour de combat. Si un personnage indique qu'il recherche activement le danger en entrant dans la grange, demandez-lui plutôt de faire un test de Sagesse (Perception) avec un désavantage dû à la faible luminosité.

**Embuscade des personnages**. C'est bien si le groupe décide de prendre d'assaut la grange pendant la réunion, mais il peut aussi décider de planifier une embuscade. Chaque personnage peut se cacher avec succès grâce à un test de Dextérité (Discrétion) DD 8.

Si Q.S. est sur le coup, il arrive avec le reste de la secte en état d'alerte, et ils ne peuvent pas être surpris. Ils entreprennent une fouille approfondie de la grange, repèrent le personnage le plus proche à l'affût (pas de test nécessaire) et engagent ensuite le combat.

Sinon, les membres du culte arrive en groupe et commence à allumer les bougies éparpillées dans la grange. Les personnages observent l'un d'entre eux chuchoter avec excitation au chef de la secte et lui montrer un morceau de papier qu'il tient dans la main.

Si le groupe attend pendant le déroulement de la réunion, il suit la même ligne que celle suggérée par le procès-verbal de la réunion précédente. On note l'absence de Basil Mureal et on exprime sa sympathie pour la terrible tragédie à laquelle il a dû assister à la prison. Le membre de culte avec le papier annonce triomphalement qu'il a localisé le tunnel, sous des acclamations enthousiastes. Un *quasi* (voir MM p.51 et 57) est convoqué, qui se métamorphose rapidement en chauve-souris et s'envole de la grange.

La fête est particulièrement intense. Si les personnages ont attendu aussi longtemps, leur patience est récompensée. Tous les membres de la secte s'enivrent suffisamment pour subir la condition *empoisonnement*, imposant un désavantage sur leurs jets d'attaque et leurs tests de compétence.

Développements. Si le groupe vainc les membres du culte, ils trouvent la carte de la falaise dessinée à la hâte, avec une flèche pointant vers un endroit derrière une zone boisée légèrement au nord de l'endroit où la dernière tour de garde marque la fin du mur de la ville avant Sousfalaise au sud. "Tunnel ! terrible obscurité" est griffonné sur le papier.

## EN TRAIN DE CREUSER

Bien que sorti des entrailles de Basil Mureal, il ne reste plus rien de l'homme dans le démon babau. Les pensées de la créature sont tournées exclusivement vers la réalisation de son objectif : localiser un tunnel perdu depuis longtemps dans la falaise qui mène à la ville de Waterdeep.

Après avoir rampé par la fenêtre de la maison de Basil Mureal, le babau a écumé la falaise sans arrêt pendant deux jours avant de trouver l'ouverture creusée et bien cachée, apparemment comblée intentionnellement par des rochers allant des gravats aux rochers. Enveloppé tout le temps dans une obscurité magique, le babau poursuit inlassablement ses fouilles.

### MON DOUX BABAU

Le démon babau (voir VGtM p.136) représente un défi peut-être mortel pour les aventuriers qui tentent de le tuer. Cependant, il est singulièrement concentré sur le forage du tunnel et ignore tout ce qui ne l'empêche pas d'accomplir son travail. Si le combat se déroule mal, les personnages peuvent facilement s'échapper.

Après environ 6 mètres, le tunnel de 1,5 mètres de diamètre s'ouvre dans une caverne naturelle d'environ 12 mètres de large et 18 mètres de profondeur. La caverne est jonchée de nombreux blocs de roche de 1,5 m de diamètre qui peuvent fournir une couverture de 3/4 à une créature de taille moyenne.

Une sphère de 4,5 mètres de noirceur magique émane d'un point situé à l'extrémité de la caverne, où le babau est en train de creuser. Il attaque s'il sent des créatures hostiles s'approcher.

## ÉPILOGUE

Si les aventuriers retournent à Waterdeep vivants et rapportent ce qu'ils ont appris sur les épouvantails à la faction qui leur a confié l'aventure, ils gagnent 1 renom, ou 2 renom s'ils ont également vaincu les épouvantails.

Anselm Griggons rend visite au groupe au Manoir du Crâne-de-troll deux jours plus tard pour remercier personnellement les personnages d'avoir débarrassé Sousfalaise des épouvantails (et du culte et du démon babau, éventuellement). En tant qu'(ancien) compagnon d'aventure, il est impressionné par la façon dont ils se sont comportés et présente une récompense de 250 po de sa propre fortune. Si le groupe a réussi à tuer le démon babau, il offre également un cadeau de ses jours d'aventurier, une amulette de santé (voir DMG p.150).

Anselm raconte que la nouvelle des exploits des personnages s'est répandue dans Sousfalaise, et qu'une chanson à boire populaire détaillant leurs exploits a fait son apparition dans sa taverne. Il note qu'il y a eu un changement d'attitude marqué à l'égard des citadins, et si le groupe contenait des races "indésirables", il semble optimiste que ces attitudes changent aussi lentement.

## DÉMON BABAU

Les démons et les diables s'affrontent sans cesse pour le contrôle des plans inférieurs. L'une de ces batailles a opposé les légions de l'archidémon Glasya aux hordes hurlantes du seigneur démoniaque Graz'zt. On dit que Glasya a blessé Graz'zt avec son épée, et les premiers babaus se sont levés là où son sang a touché le sol. Leur apparition soudaine a contribué à mettre en déroute Glasya et à faire de Graz'zt l'un des principaux seigneurs démons des Abysses.

Un démon babau possède la ruse d'un diable et la soif de sang d'un démon. Il possède une peau noire et coriace, tendue sur sa carcasse décharnée, et une corne incurvée qui dépasse de l'arrière de son crâne allongé. Le regard malfaisant d'un babau peut affaiblir une créature.

[Stats démon Babau](https://www.aidedd.org/dnd/monstres.php?vf=babau)

## DOCUMENTS

### PROCÈS-VERBAUX DES RÉUNIONS

Le Gant Impitoyable de la Main à Six Doigts

Procès-verbal réunion n°14.

Appel de service.

Officiers présents : V.L., Président, V.P., Vice-président, B.M., Secrétaire, M.E., Trésorier

Membres présents : T.T., Q.S., I.P., G.G.

Recrues présentes : N.G.

B.M. lit le procès-verbal de la réunion n°13. Procès-verbal approuvé à l'unanimité.

M.E. rapporte le montant de l'argent en caisse 9 pa. Rappelle à tous que la bière n'apparaît pas comme par magie lors de ces réunions. Passe le chapeau.

V.L. remercie Q.S. pour l'utilisation continue de la grange abandonnée de l'hydromellerie pour les réunions. Appuyé par tous.

V.L. Enquête sur les tentatives de localisation du tunnel. Aucun succès n'a été signalé.

Q.S. Se propose pour tenter l'invocation. V.L. aide. V.P. objecte car il n'a pas réussi depuis la dernière fois que nous avons entendu LA VOIX, c'était lors de la réunion n°9. Motion approuvée 5-3.

Le démon a été convoqué avec succès. Le démon mord N.G. Le démon émet un rot qui provoque un gaz vert. Démon tué par V.L. Grange temporairement évacué.

Reprise de la réunion.

V.P. propose de lancer une bénédiction. V.P. remercie, demande que LA VOIX apparaisse pour donner des conseils. Aucune apparition ne se produit.

N.G. partage des biscuits à l'avoine préparés par sa mère. Déclaré délicieux par consentement unanime.

V.L. passe à l'intronisation de N.G. en tant que membre à part entière. I.P. aide. Motion approuvée 7-1.

Détente.

V.L. demande l'ajournement de la séance. V.P. aide. Motion approuvée 9-0.
