---
layout: page
title:  "Détectives d'animaux de compagnie"
nav_order: 1
grand_parent: Préparations
parent: Missions
---

# DÉTECTIVES D'ANIMAUX DE COMPAGNIE
{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## LA CIBLE

> *Une de mes petites amies met de la distance entre elle et son terrible passé, et cela signifie mettre de la distance entre elle et son salaud de petit ami. La seule chose qui la lie à ce minable, c'est son chat - il l'a caché alors qu'ils étaient en pleine rupture, et elle n'ose pas retourner chez lui pour récupérer son petit Fluffles. C'est vrai que c'est une affaire plutôt banale, mais récupérez la et j'aurai une raison de vous donner des tâches plus importantes.*
>
> *M. Porter*

M. Porter est un demi-elfe nerveux, musclé et manchot de la main droite, avec plus de dents en or que de vraies et des favoris roux mal soignés.

L'animal de compagnie en question est probablement gardé à portée de main dans la résidence de Kronkk Tête-épaisse, le sale petit ami dans ce récit de malheur. Kronkk a une redoutable réputation, à la fois d'ivrogne et de briseur de jambes, prometteur pour les Zhentarim. M. Porter recommande aux personnages de trouver une solution non conflictuelle à leur problème, tant pour leur propre sécurité que pour toute autre chose.

## LE PAIEMENT

M. Porter a prévu un budget de 250 po pour ce type de filouterie. Les personnages qui réussissent un test de Charisme (Persuasion) DD 14 peuvent convaincre M. Porter de leur donner 50 po supplémentaires à l'avance.

## LA PRÉSENTATION

*Kronkk Tête-épaisse* vit dans les entrailles d'un bloc de logements communaux dans la zone la plus pauvre du quartier Nord (22 allée des Veilleurs). Son appartement se trouve au troisième étage, après le bureau de la réception, un grand monte-plats et une rangée de portes en bois.

## OBSTACLES

Les autres habitants de l'immeuble collectif (utilisez les statistiques de **bandits**) font office de système d'alarme rudimentaire et de force de garde pour Kronkk. Ils trouvent plus pratique de le garder heureux que de s'occuper de lui quand il est en colère.

Les personnages qui souhaitent infiltrer l'immeuble sans être détectés doivent trouver un moyen de s'occuper de cette populace suspecte. Un seul personnage infiltré qui réussit un test de Charisme (Supercherie) ou Charisme (Performance) DD 12 peut se déplacer sans attirer l'attention. Un personnage qui utilise un kit de déguisement a un avantage sur ce test.

Si les personnages souhaitent être plus conventionnellement furtifs, ils doivent s'assurer d'éviter les oreilles attentives des voisins. Un personnage qui réussit un test de Dextérité (Discrétion) DD 11 à chaque étage de l'immeuble peut se déplacer sans alerter les résidents.

L'extérieur du bâtiment est fait de briques grossières, ce qui en fait une ascension assez facile. Un personnage qui réussit un test de Force (Athlétisme) DD 12 peut grimper à l'extérieur du bâtiment, mais a un désavantage sur les test de Dextérité (Discrétion) pour passer inaperçu.

Les personnages se déplaçant en groupe de quatre ou plus peuvent utiliser des tests de groupe s'ils le souhaitent.

### LA RÉCEPTION

À la réception se tapit un vieil homme amer et tordu du nom de M. Shickadance, le propriétaire de l'immeuble. Il se méfie énormément de tous ceux qu'il rencontre, en particulier de ses locataires, et est toujours à l'affût des problèmes. M. Shickadance utilise les statistiques d'un **fanatique de culte**, avec une perception et une perspicacité de +5. Il porte des clés qui ouvrent toutes les portes de l'immeuble, mais refuse de les remettre à qui que ce soit.

### PORTES ET FENÊTRES

Les portes et les fenêtres du bâtiment sont généralement verrouillées et sécurisées par des serrures de qualité inhabituelle. Un personnage doit réussir un test de Dextérité DD 14 avec des outils de voleurs pour ouvrir une porte ou une fenêtre verrouillée.

À l'inverse, toutes les portes, fenêtres, murs et sols sont de mauvaise qualité. Un personnage qui réussit un test de force DD 12 peut faire éclater une porte, une fenêtre, un mur, un plafond ou une section de plancher, mais cela alerte immédiatement tous les occupants du bâtiment.

## REBONDISSEMENTS

Des bosses de la taille d'une tête dans le mur, des éclaboussures de sang et une étrange dent humanoïde égarée le long du couloir menant à la chambre de M. Kronkk (toutes accompagnées d'une petite note de colère de M. Shickadance) sont autant d'indications de l'échec des tentatives précédentes de sauvetage de Fluffles. Kronkk est à l'affût d'autres tentatives de sauvetage ; pendant qu'il est assoupit sur le canapé, il n'est pas désavantagé lors des tests de Sagesse (perception)).

Kronkk est un [orog](https://www.aidedd.org/dnd/monstres.php?vf=orog) avec les changements suivants :

* il a une armure de cuir au lieu d'une plaque (AC 13).
* il manie un grand gourdin (+6 au toucher, 1d8 +4 dégâts contondant).

Il a équipé son appartement d'une porte plus robuste ; le DD pour l'enfoncer est de 14.

### FLUFFLES

Fluffles est une panthère habituée à vivre comme un animal domestique choyé. Kronkk l'a affamée et battue pour tenter d'en faire une bête de combat sur laquelle on parie.

Fluffles est gardé dans une cage, juste hors de vue de la porte d'entrée. Un personnage qui réussit un test de Dextérité DD 14 avec des outils de voleurs peut déverrouiller la cage, et un personnage qui réussit un test de Force DD 12 peut faire éclater la cage. L'éclatement de la cage réveillera instantanément Kronkk.

Fluffles a très faim et très peur. Ses gémissements et ses cris constants pour la nourriture font actuellement un bruit de fond pour tout l'étage. Un personnage qui réussit un test de Charisme (Dressage) DD 10 se met Fluffles dans la poche. Un personnage qui fait un 13 ou plus à ce test est capable de le faire sans changer les bruits que Fluffles fait. Si un personnage échoue à ce test, Fluffles devient plus avide de nourriture que d'affection et ne coopère pas tant qu'il n'est pas nourri. Un personnage qui offre de la nourriture à Fluffles a un avantage sur ce test. Si Fluffles cesse de pleurer, Kronkk se réveille pour voir ce qui a changé.

## L'ÉVASION

Les habitants de l'immeuble collectif sont tout aussi conscients des tentatives de sauvetage en cours, et alerteront Kronkk s'ils voient quelqu'un partir avec Fluffles. Un personnage qui réussit un test de Charisme (Supercherie) ou Charisme (performance) DD 15 peut faire sortir Fluffles du bâtiment avant que l'alarme ne soit déclenchée. Si Fluffles n'est pas coopérative, ce test est effectué avec un désavantage.

Sinon, Kronkk se réveille lorsque les personnages sont sur le point de quitter son étage, et donne la chasse (voir les règles de poursuite dans le DMG, p.252). Son trait Agressif l'aide à suivre les personnages agiles et à garder la poursuite intéressante.

A moins que Fluffles n'ait été correctement apaisé, la panthère réagit avec terreur à la vue de Kronkk. Fluffles est terrorisée (comme la condition) par Kronkk, et attaque tout personnage qui la tient jusqu'à ce qu'elle soit libéré, auquel cas elle tente de s'enfuir.

## ÊTRE PAYÉ

En rendant Fluffles à M. Porter, vous obtiendrez la récompense promise. Un personnage qui raconte une histoire amusante de frustration et de malheur et qui réussit un test de Charisme (Persuasion) ou de Charisme (Performance) DD 16 peut convaincre M. Porter de lui donner 100 po supplémentaires, bien que M. Porter rappelle aux personnages qu'il est très mal vu de négocier une fois qu'un accord est conclu.
