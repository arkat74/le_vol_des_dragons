---
layout: page
title: Oiseau de papier
grand_parent: Préparations
parent: Objets spéciaux et magiques
nav_order: 100
---

# OISEAU DE PAPIER

## DESCRIPTION

**Oiseau de papier**

*Objet merveilleux, peu courant*

Une fois que vous avez écrit un message 50 mots ou moins sur cette feuille de parchemin magique et prononcé le nom d'une créature, elle se plie par magie pour prendre la forme d'un Très Petit oiseau de papier, qui s'envole pour rejoindre le destinataire dont vous avez prononcé le nom.
Ce dernier doit se trouver sur le même plan d'existence que vous, sinon l'oiseau est réduit en cendres au moment où il prend son envol.

L'oiseau est un objet avec les caractéristiques suivantes :
- 1 PV
- CA 13
- Vitesse de vol 18 m
- DEX 16 (+3)
- 1 à toutes les autres caractéristiques (-5)
- Immunisé au poison et attaques psychiques

Il emprunte le chemin le plus direct pour rejoindre le destinataire du message. Quand il se trouve dans un rayon de 1,5 m autour de celui-ci, il redevient une feuille de papier inanimée non magique mais seul le destinataire peut le déplier.

Si les PV ou la vitesse de vol sont réduits à 0 d'une manière ou d'une autre, il tombe en cendres.

On trouve généralement les oiseaux de papier dans de minces boites plates contenant 1D6 + 3 feuilles de parchemin.
