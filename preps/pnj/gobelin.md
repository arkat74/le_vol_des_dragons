---
layout: page
title: Gobelin
grand_parent: Préparations
parent: PNJs
nav_order: 115
---

# GOBELIN

*Humanoïde (gobelinoïde) de taille P, Neutre Mauvais*

**CA** 15 (armure de cuir, bouclier)  
**PV** 7 (2d6)  
**Vitesse** 9 m

**FOR** 8 (-1)  
**DEX** 14 (+2)  
**CON** 10 (+0)  
**INT** 10 (+0)  
**SAG** 8 (-1)  
**CHA** 8 (-1)

**Compétences** Discrétion +6  
**Sens** vision dans le noir 18m Perception passive 9  
**Langue** commun, gobelin  
**Dangerosité** 1/4 (50 PX)

***Fuite agile***  
Le gobelin peut effectuer l'action se désengager ou se cacher par une action bonus à chacun de ses tours.

## ACTIONS

***Cimeterre*** *Attaque au corps à corps avec une arme* :  
+4 au toucher, allonge 1,50 m, 1 cible. Touché : 5 (1d6 + 2) dégâts tranchants.

***Arc court*** *Attaque à distance avec une arme* :  
+4 au toucher, portée 24/96 m, 1 cible. Touché : 5 (1d6 + 2) dégâts perforants.

## TACTIQUE

* grande Dextérité et bonne Discrétion
* vision dans le noir
* attaque souvent dans le noir (avantage pour eux, désavantage pour les PJs)
* attaque à couvert
* combat typique: attaque arc court (action), déplacement, Se cacher (action bonus avec **Fuite agile**)
* Petite créature => bonne chance de Se cacher, même si raté, peut offrir +5 CA dû à **Abri important** (PHB p.196)
* ne s'approche pas à moins de 10m et pas à plus de 24 * (portée max de l'arc court pour un tir sans malus)
* si ennemi au contact
  * se désengage (action bonus avec **Fuite agile**)
  * en fonction de la puissance du PJ
    * si trop fort, se précipite (action) et oblige le PJ a se précipiter aussi
    * sinon, déplacement (distance totale) vers un endroit où se cacher et Se cacher (action), attaque avec avantage au prochain tour
* essaient de séparer le groupe de PJs
* réduit à 1-2 pv => fuite
* réduit à 3-4 pv => continue le combat, 1 - déplacement, 2 - Se cacher, 3 - Attaque furtive à distance (avantage)
* si PJs se mettent à couvert aussi, Goblins essaient de se déplacer furtivement et se placent derrière les PJs et continuent les attaques à distance
* peu de cas où Goblins attaquent au corps à corps, sauf si combinaison de facteurs : dans le noir, flanking et plusieurs sur une même cible par exemple
* si au bout d'un tour, la cible n'est pas à terre => se désengagent (action bonus), se déplacent et se cachent (Action)
* Goblins savent reconnaitre les créatures qui voient dans le noir => ne les attaquent qu'à distance
