---
layout: page
title: Kenkus
grand_parent: Préparations
parent: PNJs
nav_order: 110
---

# KENKU

![Kenku](../../../assets/images/preps-kenku.png "Kenku")

## CARACTÉRISTIQUES

*Humanoïde (kenku) de taille M, chaotique neutre*

**CA** 13  
**PV** 13 (3d8)  
**Vitesse** 9 m

**FOR** 10 (+0)  
**DEX** 16 (+3)  
**CON** 10 (+0)  
**INT** 11 (+0)  
**SAG** 10 (+0)  
**CHA** 10 (+0)

**Compétences** Supercherie +4, Perception +2, Discrétion +5  
**Sens** Perception passive 12  
**Langues** comprend l'aérien et le commun mais parle uniquement grâce au pouvoir imitation  
**Dangerosité** 1/4 (50 XP)

***Attaque surprise***  
Le kenku est avantagé sur les jets d'attaque contre les créatures qu'il surprend.

***Imitation***  
Le kenku peut imiter tous les sons déjà entendus, y compris des voix. Une créature qui entend ces sons peut reconnaître leur vraie nature en réussissant un test de SAG (perception) DD 14.

## ACTIONS

***Épée courte.*** *Attaque d'arme au corps à corps* :  
+5 au toucher, allonge 1,50 m, une cible. Touché : 6 (1d6 + 3) dégâts perforants.

***Arc court.*** *Attaque d'arme à distance* :  
+5 au toucher, portée 24/96 m, une cible. Touché : 6 (1d6 + 3) dégâts perforants.

## TACTIQUES

Voir **[ici pour les détails](../../../pnj-tactics/kenku/)**

* grande Dextérité
* pas de Vision dans le noir
* Discrétion + *Attaque surprise*
  * se cachent + volée de flèches (1er tour) => Surprise
* 2 kenkus vont au contact (visent des cibles faciles)
  * Se désengage et recule si contre combattant compétent
* 2 kenkus tirent des fèches contre les cibles plus coriaces
  * archers maintiennent toujours **au minimum 10 mètres** de distances
  * tirent si possible à 15 - 20 mètres
  * archers toujours espacés pour éviter qu'un ennemi qui charge puisse les toucher
* groupe entier se désengage et se disperse si combat + 4 tours ou si 1/2 groupe réduit à 5 pv
* **objectif** obtenir de la matière brillante et précieuse, pas doué pour le combat
* cupide et donc corruptible :
  * 5 po/kenku  = ne combattent pas
  * 10 po/kenku = une faveur
  * 25 po/kenku = collaborateur
