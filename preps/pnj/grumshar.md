---
layout: page
title: Grum'shar - Aprenti Magicien
grand_parent: Préparations
parent: PNJs
nav_order: 112
---

# GRUM'SHAR - APRENTI MAGICIEN

*Humanoïde (demi-orc) de taille M, Chaotique Mauvais*

**CA** 10  
**PV** 15 (2d8 + 6)  
**Vitesse** 9 m

**FOR** 16 (+3)  
**DEX** 10 (0)  
**CON** 16 (+3)  
**INT** 14 (+2)  
**SAG** 10 (0)  
**CHA** 11 (0)

**Compétences** Arcane +4, Histoire +4  
**Sens** vision dans le noir 18m Perception passive 10  
**Langue** commun, orc  
**Dangerosité** 1/4 (50 PX)

***Acharnement***  
Lorsqu'il se trouve à 0 PV, mais pas tué sur le coup, réduction à 1 PV (réutilisable après long repos).

***Incantation***  
Un apprenti est un lanceur de sort de niveau 1. Sa caractéristique d'incantation est l'Intelligence (DD du jet de **sauvegarde contre les sorts 12**, +4 pour toucher avec les attaques de sort). Il a préparé les sorts suivants :

Tours de magie (à volonté) : *trait de feu, poigne électrique, réparation*
1er niveau (2 emplacements) : *mains brûlantes, déguisement, simulacre de vie, bouclier, serviteur invisible, carreau ensorcelé*

## ACTIONS

***Dague*** *Attaque d'arme au corps à corps ou à distance* :  
+2 pour toucher, allonge 1,50 m ou portée 6/18 m, 1 cible. Touché : 5 (1d4 + 3) dégâts perforants.

## TACTIQUE

A préparer les sorts :

* Tours de magie :
  * [trait de feu](https://www.aidedd.org/dnd/sorts.php?vf=trait-de-feu), évocation, 1 action, 36m, Si l'attaque avec un sort touche, inflige 1d10 dégâts de feu (dégâts/niv). Un objet peut prendre feu.
  * [poigne électrique](https://www.aidedd.org/dnd/sorts.php?vf=poigne-electrique), évocation, 1 action, contact, Si l'attaque avec un sort touche, inflige 1d8 dégâts de foudre (dégâts/niv) et la cible ne peut pas prendre de réaction.
  * [réparation](https://www.aidedd.org/dnd/sorts.php?vf=reparation), transmutation, 1 minute, N/A, Répare fissure, déchirure, fêlure d'un objet (maillon de chaîne cassé, clé brisée, accroc sur un manteau, fuite d'une gourde).
* 1er niveau :
  * [mains brûlantes](https://www.aidedd.org/dnd/sorts.php?vf=mains-brulantes), évocation, 1 action, personnelle (cône 4,5m), Les créatures dans un cône de 4,50 m doivent réussir un JdS de Dex. ou subir 3d6 dégâts de feu (dégâts/niv).
  * [bouclier](https://www.aidedd.org/dnd/sorts.php?vf=bouclier), abjuration, 1 réaction, personnelle, En réaction, la lanceur gagne un bonus de +5 à la CA et ne prend aucun dégât du sort projectile magique.