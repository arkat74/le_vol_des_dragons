---
layout: page
title: Malfrat
grand_parent: Préparations
parent: PNJs
nav_order: 110
---

# Malfrat

## CARACTÉRISTIQUES

*Humanoïde (n'omporte quelle race) de taille M, n'importe quel alignement autre que Bon*

**CA** 11 (armure de cuir)  
**PV** 32 (5d8 + 10)  
**Vitesse** 9 m

**FOR** 15 (+2)  
**DEX** 11 (+0)  
**CON** 14 (+2)  
**INT** 10 (+0)  
**SAG** 10 (+0)  
**CHA** 11 (+0)

**Compétences** Intimidation +2  
**Sens** Perception passive 10  
**Langues** une langue au choix (généralement le commun)  
**Dangerosité** 1/2 (100 XP)

**_Tactique de groupe_**  
Le malfrat a un avantage aux jets d'attaque contre une créature si au moins l'un de ses alliés est à 1,50 mètre ou moins de la créature et n'est pas incapable d'agir.

## ACTIONS

**_Attaques multiples._** Le malfrat réalise deux attaques au corps à corps.

**_Masse d'armes._** _Attaque au corps à corps avec une arme_ :  
+4 au toucher, allonge 1,50 m, une cible. Touché : 5 (1d6 + 2) dégâts contondants.

**_Arbalète lourde._** _Attaque à distance avec une arme_ :  
+2 au toucher, portée 30/120 m, une cible. Touché : 5 (1d10) dégâts perforants.

## TACTIQUES

Voir **[ici pour les détails](../../../pnj-tactics/garde-voyous-veteran-chevalier/#malfrats)**

* utilise massivement _attaque de groupe_
* pas de combat à distance, uniquement de la mélée
* si un malfrat réduit à 12pv ou moins, action _Se désengager_, puis les autres malfrats _se désengageront_ aussi
* reviendront plus tard prendre leur revanche mais encore plus nombreux
