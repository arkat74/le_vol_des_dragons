---
layout: page
title: Nihiloor - Flagelleur mental
grand_parent: Préparations
parent: PNJs
nav_order: 113
---

# NIHILOOR - FLAGELLEUR MENTAL

*Aberration de taille M, Loyale Mauvaise*

**CA** 15 (cuirasse)  
**PV** 71 (13d8 + 13)  
**Vitesse** 9 m

**FOR** 11 (0)  
**DEX** 12 (+1)  
**CON** 12 (+1)  
**INT** 19 (+4)  
**SAG** 17 (+3)  
**CHA** 17 (+3)

**Jet de sauvegarde** Int +7, Sag +6, Cha +6
**Compétences** Arcane +7, Discrétion +4, Perception +6, Perspicacité +6, Persuasion +6, Supercherie +6  
**Sens** vision dans le noir 36m Perception passive 16  
**Langue** commun des profondeurs, profond, télépathie 36m  
**Dangerosité** 7 (2900 PX)

***Incantation innée (psionique)***  
Le flagelleur mental utilise sont Intelligence comme caractéristique d'incantation innée (DD des **jets de sauvegarde: 15**). Il peut lancer les sorts suivants de manière innée, sans utiliser de composantes :

A volonté : *détection des pensées, lévitation*
1/jour : *dominer un monstre, changement de plan (sur soi uniquement)*

***Résistance à la magie***  
Le flagelleur mental est avantagé lors de ses jets de sauvegarde contre les sorts et autres effets magiques

## ACTIONS

***Décharge mentale (recharge 5-6)***  
Le flagelleur mental émet une vague d'énergie psychique dans un cône de 18 mètres. Chaque créature présente dans la zone doit réussir un jet de sauvegarde d'Intelligence DD 15 ou subir 22 (4d8 + 4) dégâts psychiques et être étourdie pendant 1 minute. Une créature peut retenter son jet de sauvegarde à la fin de chacun de ses tours, mettant fin à l'effet qui l'affecte en cas de réussite.

***Extraction de cerveau*** *Attaque d'arme au corps à corps*  
+7 au toucher, allonge 1,50 m, un humanoïde incapable d'agir et agrippé par le flagelleur mental. *Touché : la cible subit 55 (10d10) dégâts perforants*. Si ces dégâts font tomber les points de vie de la cible à 0, le flagelleur mental tue la cible en lui extrayant et dévorant le cerveau.

***Tentacules*** *Attaque d'arme au corps à corps*  
+7 au toucher, allonge 1,50 m, une créature. *Touché : 15 (2d10 + 4) dégâts psychiques*. Si la cible est de taille M ou inférieure, elle est agrippée (DD 15 pour s'échapper) et doit réussir un jet de sauvegarde d'Intelligence DD 15 ou être étourdie tant qu'elle est agrippée de cette manière.

## TACTIQUE
