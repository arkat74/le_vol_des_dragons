---
layout: page
title: Dévoreur d'intellect
grand_parent: Préparations
parent: PNJs
nav_order: 114
---

# DÉVOREUR D'INTELLECT

*Aberration de taille TP, Loyal Mauvais*

**CA** 12  
**PV** 21 (6d4 + 6)  
**Vitesse** 12 m

**FOR** 6 (–2)  
**DEX** 14 (+2)  
**CON** 13 (+1)  
**INT** 12 (+1)  
**SAG** 11 (+0)  
**CHA** 10 (+0)

**Compétences** Perception +2, Discrétion +4  
**Résistances aux dégâts** contondants, perforants et tranchants infligés par des attaques non magiques  
**Immunité contre l'état** aveuglé  
**Sens** vision aveugle 18 m (aveugle au delà de cette distance), Perception passive 12  
**Langues** comprend le profond mais ne peut pas parler, télépathie 18m  
**Dangerosité** 2 (450 XP)

**Détection des consciences**. Le dévoreur d'intellect peut sentir la présence et l'emplacement des créatures situées à 90 m ou moins autour de lui et dont l'Intelligence est supérieure ou égale à 3, quels que soient les obstacles qui les séparent. Seul le sort *esprit impénétrable* empêche cette détection.

## ACTIONS

**Attaques multiples**. Le dévoreur d'intellect effectue une attaque avec ses griffes et utilise dévoration d'intellect.

**Griffes**. *Attaque au corps à corps avec une arme* :

+4 au toucher, allonge 1,5 m, une cible. Touché: 7 (2d4 + 2) dégâts tranchants.

**Dévoration d'intellect**. Le dévoreur d'intellect cible une créature dotée d'un cerveau et située à 3 m ou moins de lui dans son champ de vision. La cible doit réussir un JdS Intelligence DD 12 contre cette magie pour ne pas subir 11 (2d10) (psychique). De plus, en cas d'échec, lancez 3d6: si le total est égal ou supérieur à la valeur d'Intelligence de la créature, cette valeur est réduite à 0. La cible est étourdie jusqu'à ce qu'elle récupère un point d'Intelligence au moins.

**Voleur de corps**. Le dévoreur d'intellect effectue un test Intelligence opposé à celui d'un humanoïde neutralisé, qui n'est pas protégé par le sort protection contre le mal et le bien, et situé à 1,5 m ou moins autour de lui. S'il remporte cette opposition, le dévoreur d'intellect consume par magie le cerveau de la cible, se téléporte dans sa boîte crânienne et prend le contrôle de son corps. Une fois à l'intérieur d'une créature, le dévoreur d'intellect bénéficie d'un abri total contre les attaques et effets provenant de l'extérieur de son hôte. Le dévoreur d'intellect conserve ses valeurs d'Intelligence, Sagesse et de Charisme, sa compréhension du profond, son pouvoir de télépathie et ses traits. Il adopte toutes les autres stats de la cible et sait tout ce qu'elle savait, y compris les sorts et les langues.
Si le corps de l'hôte meurt, le dévoreur d'intellect doit l'abandonner. Un sort de protection contre le mal et le bien lancé sur le corps oblige le dévoreur d'intellect à en sortir. Le dévoreur d'intellect est également forcé de sortir si la cible récupère son cerveau par le biais d'un souhait.
En dépensant 1,5 m de sa vitesse de déplacement, le dévoreur d'intellect peut volontairement quitter le corps et se téléporter à l'emplacement inoccupé le plus proche à 1,5 m ou moins du corps. Ce dernier meurt ensuite, à moins que son cerveau ne soit restauré en l'espace d'un round.

## TACTIQUE

Voir [ici pour les détails](../../../pnj-tactics/devoreur-intellect/)

* première action : essait de Se cacher
* attend qu'un PJ arrive à moins de 3 mètres
* utilise *Dévoration d'intellect*
* si le dévoreur d'intellest ne peut pas faire tomber sa cible en **trois rounds**, il abandonne et s'enfuit
* **résiste aux dégâts des armes normales**
* classe d'armure pas particulièrement élevée
* grande vitesse de mouvement
==> dévoreur d'intellect en fuite utilisera *Se précipiter* plutôt qu'*Esquiver*