---
layout: page
title: Observateur
grand_parent: Préparations
parent: PNJs
nav_order: 111
---

# OBSERVATEUR

![Observateur](../../../assets/images/observateur.jpg "Observateur")

*Aberration de Très Petite taille, Neutre Mauvais*

**CA** 13  
**PV** 13 (3d4 + 6)  
**Vitesse** 0 m, vol 9 (stationnaire)

**FOR** 3 (-4)  
**DEX** 17 (+3)  
**CON** 14 (+2)  
**INT** 3 (-4)  
**SAG** 10 (+0)  
**CHA** 7 (-2)

**Jet de Sauvegarde** SAG +2  
**Compétences** Perception + 4, Discrétion +5  
**Immunité contre l'état** à terre  
**Sens** vision dans le noir 18m, Perception passive 14  
**Langues** -  
**Dangerosité** 1/2 (100 XP)

***Aggressif***  
Par une action bonus, un observateur peut se déplacer sur une distance égale ou inférieure à sa vitesse en direction d'une créature hostile qu'il peut voir.

***Imitation***  
Un observateur peut imiter des mots simples qu'il entend, peu importe leur langue. Une créature qui entend ces bruits comprend qu'il s'agit d'une imitation si elle réussit un test de Sagesse (Perspicacité) DD 10.

## Actions

***Morsure*** *Attaque d'arme au corps à corps* :  
+5 au toucher, allonge 1,5m, une cible. Touché: 1 dégâts perforants.

***Rayon oculaire*** :  
Un observateur peut tirer deux rayons oculaires magiques au hasard (relancez les dés en cas de résultats identiques), dirigés contre une ou deux cibles dans son champ de vision et un rayon de 18 m :

1. Rayon d'ahurissement. La créature ciblée doit réussir un JdS Sagesse DD 12, sinon elle est charmée jusqu'au début du prochain tour de l'observateur. Tant que la cible est charmée, sa vitesse est divisée par deux et elle est désavantagée lors de ses jets d'attaque.
2. Rayon de terreur. La créature ciblée doit réussir un JdS Sagesse DD 12, sinon elle est terrorisée jusqu'au début du prochain tour de l'observateur.
3. Rayon de givre. La créature ciblée doit réussir un JdS Dextérité DD 12, sinon elle subit 10 (3d6) dégâts de froid.
4. Rayon de télékinésie. Si la créature ciblée est de taille Moyenne ou inférieure, elle doit réussir un JdS Force DD 12, sinon elle est repoussée d'au max 9 m dans la direction opposée à l'observateur. Si la cible est un objet qui pèse 5 kg ou moins et qui n'est pas porté, l'observateur peut le déplacer de 9 m dans n'importe quelle direction. L'observateur peut également utiliser ce rayon avec précision sur des objets, et ainsi manipuler un outil simple ou ouvrir une boîte.

## TACTIQUES

* Se cache
* Attaque à distance avec son *Rayon oculaire*
* Reste toujours à environ 18 mètres
* si approché, utilise l'action *Se précipiter* pour rester à distance (18 mètres)
* si réduit à 5 pv ou moins, fuit en utilisant action *Se précipiter*
