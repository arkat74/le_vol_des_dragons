---
layout: page
title: Localisation de la Pierre de Golorr et des ses Yeux
nav_order: 2
grand_parent: Préparations
parent: Parties
---

# LOCALISATION DE LA PIERRE DE GOLORR ET DES SES YEUX

## PIERRE DE GOLORR

* ~~Avec le vif-acier~~ Avec Laeral Silverhand au château de Pieirgeon

## YEUX

1. Repaire : antre de Xanathar - X19 (dans le bocal à poisson de Sylgar)
2. Repaire : les Tours de Kolat - E9 (dans la chambe forte astrale)
3. Repaire : villa des Cassalantre - C6 (caché dans le compartiment secret du bureau de Victoro)
