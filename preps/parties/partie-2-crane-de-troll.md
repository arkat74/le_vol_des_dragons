---
layout: page
title: Le Manoir du Crâne-de-troll
nav_order: 21
grand_parent: Préparations
parent: Parties
---

# LE MANOIR DU CRÂNE-DE-TROLL
{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

![Manoir du Crâne-de-troll](../../../assets/images/manoir-du-crane-de-troll-sans-mobilier.png)

## EXPLORATION DU MANOIR

Voir [partie 8 - Description du manoir du Crâne-de-troll](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-8-description-manoir.html)

* ajouter les [3 Gamins](https://arkat74.gitlab.io/trad_dragon_heist_remix/partie-3d-autres-equipes-intervention.html#les-trois-gamins) dans la salle d'auberge en train de jouer à des jeux imaginaires
* le poste du guet le plus proche est localisé au croisement de la voie Windborne et de la rue Sashtar. Il est dirigé par le capitaine Vollmer Harnid.

![Poste du Guet près de l'allée du Crâne-de-troll](../../../assets/images/localisation-poste-guet.png)

## MISE EN PLACE DU MANOIR

Voir [ici](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-2-mise-en-place-du-manoir.html)

## LES GUILDES

### Guilde des charpentiers, couvreurs et plâtriers

Rue de Telshambra, quartier Sud

## FINANCER LA RECONSTRUCTION

### LES SOURCES D'EMPRUNT

* **Istrid Corne**, une membre du Zhentarim (les Pillards de tenures), voir p.211, peut offrir des emprunts jusqu'à 2,500 po avec un taux d'intérêt de 10% par décade (voir p.17)
* **Mirt**, des Ménestrels, est connu sous le nom de Mirt l'Usurier.  Il pourrait probablement être appelé à prêter de l'argent au groupe... et si un membre du groupe est recruté par les Ménestrels, il pourrait leur faire une bonne offre. Mirt est extrêmement riche... si le groupe peut rendre l'emprunt attrayant, il pourrait être persuadé d'investir dans la taverne pour un pourcentage des bénéfices. Si vous lui proposez de faire du manoir un abri ou une base d'opération pour les Ménestrels, il pourrait simplement couvrir votre marge manquante en tant qu'investissement dans le bien-être des Ménestrels. Tout autre noble que le groupe a appris à connaître (tel que Raenar Neverember) pourrait également être un investisseur potentiel. Et, bien sûr, toute autre Faction pourrait également sponsoriser la réparation du Manoir, à condition qu'elle en profite de manière appropriée.
* **les Cassalantre** sont également appelés à prêter de l'argent (même taux d'emprunt que les autres), ce qui pourrait donner lieu à un jeu très intéressant s'ils sont aussi les méchants
* **les [Irlingstar](https://forgottenrealms.fandom.com/wiki/Irlingstar)**. L'annexe B indique également que les taux d'intérêt d'Istrid sont en ligne avec ceux des autres prêteurs d'argent, donc encore une fois...10% par décade (ce qui est raide...ouch)
* **[Y a-t-il quelque chose qui ressemble à une "banque" dans Waterdeep](https://www.sageadvice.eu/2018/01/19/is-there-anything-akin-to-a-bank-in-waterdeep/)**. Beaucoup ! Les temples et les guildes (pour leurs membres uniquement, ou ceux qui essaient de payer leurs membres) font office de banques. La ville aussi (le Palais de Piergeiron). Et pour ceux qui veulent faire des transactions "officieuses", il existe de très nombreux prêteurs privés (comme le tristement célèbre Mirt).

### LES MISSIONS

* **Obaya Uday** offre de l'or pour les livres de sorts, basé sur le sort de plus haut niveau dans le livre. Pour un livre ne contenant que des sorts de premier niveau, comme celui qui se trouve dans la cachette de la guilde de Xanathar, elle offre 5pp (50 po). Pour un livre contenant des sorts de 2ème niveau, comme celui acquis lors de la mission des Ménestrels de 3ème niveau, elle offre 25pp (250 po)
* La mission de l'Enclave d'Émeraude de 3ème niveau paie 100 po par personnage qui y participe
* La mission des Ménestrels de 3ème niveau donne un livre de sorts avec des sorts de niveau 2 (voir ci-dessus, peut valoir de l'argent à Obaya)
* La mission du Zhentarim de 2ème niveau paie 50 po par personnage qui y participe
* La mission Zhentarim de 3e niveau paie un forfait de 15pp (150 po)
* Sauver Kitty peut aussi rapporter 250 po
* Les missions dans les livres **Benjamin Reece - Mr Porter's Tome of Skullduggery**
* [Détectives d'animaux de compagnie](../missions/detectives-animaux-compagnie/)
* [Il l'a dans la peau](../missions/il-l-a-dans-la-peau/)

## RECRUTEMENT

* Que les PJ recrutent ou non Patrick Nivka (un des possibles serveurs présenté par Broxley), s'ils ne lui accorde pas une chambre au Manoir, celui-ci mourra 2 jours après son entrevu, tué alors qu'il squattait dans un bouge du quartier des Docks

## GRANDE OUVERTURE

* a lieu le 19 Ches pendant le jour des Fées
* **Jour des Fées :** on raconte qu'en ce jour, le voile entre ce monde et le royaume féerique du Feywild est plus fragile que de coutume. Si ce phénomène est synonyme de prudence dans les zones rurales (les paysans évitent alors les zones boisées, déposent des offrandes sur le seuil de leur porte, etc.), c'est à Waterdeep l'occasion de boire, de chanter et de danser.  
Les plus riches organisent des bals masqués tandis que les moins nantis revêtent des costumes de leur création et vont de logis en logis, afin de participer brièvement aux célébrations qui s'y déroulent en échange de l'interprétation d'une chanson ou d'une saynète. Tous adoptent l'apparence d'êtres féeriques ou des dirigeants présumé du Feywild, comme la reine Titania, Obéron et Hyrsam, le prince des Fous. Ceux qui trouvent ces divertissements frivoles ont tout intérêt à rester chez eux car les fêtards font leur possible pour arracher un sourire à tous ceux qu'ils croisent.

### LE LIEU

* Le manoir du Crâne-de-troll :
  * salle principale
  * bar
  * extérieur proche

### LISTE DES INVITÉS

* [Renaer](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-6-amis-renaer.html#renaer-neverember-vdd-p213-214)
* [Floon](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-6-amis-renaer.html#floon-blagmaar-vdd-p-197)
* [Mattrim](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#mattrim-trois-cordes-mereg-vdd-p-20)
* [Jalester](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#jalester-silvermane-vdd-p200)
* [Laraelra “Elra” Harsard](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-6-amis-renaer.html#laraelra-elra-harsard)
* [Seigneur Torlyn Wands](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-6-amis-renaer.html#seigneur-torlyn-wands)
* [Talisolvanar "Tally" Mortebranche](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#talisolvanar-tally-mortebranche-vdd-p32)
* [Embric](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#embric-vdd-p32)
* [Avi](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#avi-vdd-p32)
* [Fala Lefaliir](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#fala-lefaliir-vdd-p32) vient accompagné de Ziraj, son ami Zhentarim.
* [Vincent Tranché](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#vincent-tranch%C3%A9e-vdd-p32-33)
* [Ulkoria Moellepierre](https://arkat74.gitlab.io/trad_dragon_heist_remix/le-manoir/partie-5-les-clients.html#ulkoria-moellepierre-vdd-p42)

### ÉVÉNEMENTS

* Arrivée Ulkoria
* Arrivée Renaer et ses amis (Floon, Laraelra et Torlyn)
* Danse entre Renaer et Laraelra, sur une musique de Mattrim (et Arikel ?)
* Passage du Guet (avec le capitaine Vollmer Harnid et 1 sergent et 2 Lames)
* Frewn vient rendre une petite visite (essais de faire venir certains client chez lui)
* Ulkoria félicite les nouveaux patrons de la taverne, elle trouve qu'ils ont redonner de la prestance à son ancien étblissemet, leur propose de graver 1 glyphe de protection de leur choix, avant de s'en aller

### SUJETS DE CONVERSATION

* Venue du Carnaval des Sirènes
* Infestation de rats dans l'allée, personne ne sait d'où ils viennent mais ils font des dégâts
* Un feu étrange sur la rue de la Cage  
Un magasin de robes, de manteaux et d'accessoires de luxe situé dans le "bloc nord-ouest" de la rue de la Cage, dans le quartier du Château, a été la proie des flammes hier : des flammes d'un bleu vif, sans chaleur ni fumée, qui n'ont rien consumé et ont rapidement disparu. Le personnel de l'_Étoile Brillante de Mhalavo_ n'a pu donner aucune raison à ce mystérieus incendie, et a insisté sur le fait qu'il ne s'agissait pas d'un "tentative de gloire" de leur part.  
La responsable de la boutique Daztriiya Ghallowglond pense que le feu était "manifestement magique... et ceux qui utilisent de telles forces sont malveillants et indignes de confiance, voire dérangés". Ghallowglond pense que la méchanceté est une raison plus probable que la folie, et "si c'était une tentative pour distraire le personnel afin de voler nos superbes marchandises, elle a échoué complètement : comme le font toujours de telles tentatives".  
Ghallowglond a prévenu les voleurs potentiels que l'_Étoile Brillante_ est gardé par "des yeux invisibles et très attentifs". Elle estime que le Guet "a réagi trop lentement, compte tenu de notre importance et de notre proximité avec la Tour d'Ahghairon et le Palais", et attend d'eux qu'ils empêchent "d'autres problèmes", tandis que l'_Étoile Brillante_ restera "le premier choix des courtisans avisés, ainsi que des visiteurs de notre ville ayant d'importants engagements civils ou sociaux, souhaitant être vêtus convenablement".
* Confession dans l'assassinat de Flavauro  
Un épicier de la rue du Fanal a admis avoir poignardé l'acteur, danseur et ancien aventurier populaire Mistram Flavauro le mois dernier. Le beau et débonnaire Flavauro était connu pour sa popularité auprès de toutes les dames de Waterdeep et beaucoup soupçonnaient que son meurtre était l'oeuvre d'un mari, d'un père ou d'un frère jaloux d'une de ses conquêtes parfois triquotidiennes. Si la confession d'Armest Harrigo est vraie, ces soupçons étaient corrects.  
Harrigo, de la Bonne Table d'Harrigo (du côté est de la rue du Fanal, trois portes au sud de la jonction avec rue Shoor), est un homme petit, corpulent et affairé, aux manières douces et hésitantes. Même les agents du Guet disent qu'il semble être un spécimen au physique improbable pour battre au combat l'acrobate et expert en armes Flavauro. Harrigo raconte qu'un client lui a dit que Flavauro recevait ses deux soeurs (les frères et soeurs célibataires d'Harrigo habitent ensemble au-dessus de l'épicerie) dans sa luxueuse et grande maison sur la rue de l'Ivoire, dans le quartier Maritime. Harrigo s'y est précipité, a découvert cette vérité et a attaqué Flavauro.  
Il raconte que ses soeurs l'ont furieusement supplié de partir, et que Flavauro, nu, s'est moqué de son couperet et de son couteau à navet, riant si fort qu'Harrigo a facilement tranché les doigts de l'homme avec l'un d'eux, et a enfoncé l'autre dans le ventre de Flavauro. Il dit que Flavauro a reculé en hurlant et est tombé à travers une fenêtre pour atterrir empalé sur les lances de pierre des statues de héros dans son propre jardin. Harrigo a pillé la cave à vin de l'homme assassiné pour calmer ses soeurs hystériques, et les a ramenées chez elles, titubantes et rieuses, dans l'obscurité : au grand amusement de pas moins de six patrouilles du Guet sur le chemin.  
Harrigo affirme qu'il défendrait à nouveau sa famille de la même manière, et il n'a avoué récemment que grâce aux visions que lui a envoyées la sainte Chauntea. Les officiers du Guet ont consigné ses paroles, mais n'ont pas encore dit si Harrigo sera jugé. Plus de quarante personnes ont avoué le meurtre de Flavauro, mais un officier anonyme du Guet a déclaré que les détails de la confession d'Harrigo en font le premier à correspondre à "certaines preuves" dans cette affaire. Un garde du Guet reste en poste dans la maison de Flavauro, maintes fois pillée.
