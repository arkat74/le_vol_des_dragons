---
layout: page
title: Musiques
nav_order: 4
grand_parent: Préparations
parent: Parties
---

# MUSIQUES

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## THEME PRINCIPAL CAMPAGNE

* [Daniel Pemberton - Out Of The Garage (from album The Man from UNCLE Soundtrack)](https://www.youtube.com/watch?v=sk-TjQ2DF4M)

## CHAINES YOUTUBE

* [Sword Coast Soundscapes](https://www.youtube.com/channel/UCUGy8GD5oY4EX9awX4FSqBw)

## AMBIANCE TAVERNES

* [Taverne bondée](https://www.youtube.com/watch?v=EULoybB2Nsw)
* [Le Portail Béant](https://www.youtube.com/watch?v=OhYfyVRZNQ4)

## POURSUITES

* [J.G. Thirlwell - Assclamp! (from album The Venture Bros.: The Music of J. G. Thirlwell)](https://www.youtube.com/watch?v=SuxRxO0Y_g4)
* [Earl Scruggs And Friends - Foggy Mountain Breakdown](https://www.youtube.com/watch?v=yQIJuu3N5EY)
* [Charles Mingus - Boogie Stop Shuffle (from album Ah Um)](https://www.youtube.com/watch?v=uYFachqAup4)
* [The Herbaliser - The Missing Suitcase (from album Very Mercenary)](https://www.youtube.com/watch?v=o5M-iWj8v4g)

## CAMBRIOLAGES

* Nick Bärtsch - Modul 4 (from album Continuum)
* [Payday 2 - And Now We Wait (from album Payday 2 Official Soundtrack)](https://www.youtube.com/watch?v=BjXNlIaGfrY)
* [Ian Carr's Nucleus - Roots (from album Roots)](https://www.youtube.com/watch?v=gBRKnvK1JUE)
* [J.G. Thirlwell - Gawker (from album The Venture Bros.: The Music of J. G. Thirlwell)](https://www.youtube.com/watch?v=05J9bPMYDzo)
* [J.G. Thirlwell - Fumblestealth (from album The Venture Bros.: The Music of J. G. Thirlwell)](https://www.youtube.com/watch?v=MRBIJU60AL8)

## FILATURES

* [Lalo Schifrin - Shifting Gears (from album Bullitt Soundtrack)](https://www.youtube.com/watch?v=hurIhvGPUCc)
* [The Herbaliser - Goldrush (from album Very Mercenary)](https://www.youtube.com/watch?v=AAqWxrFGxvQ)
* [Daniel Pemberton - Laced Drinks (Betrayal Pt II) (from album The Man from UNCLE Soundtrack)](https://www.youtube.com/watch?v=M49Q4JnwCIY)
* [Don Elis - Whiplash (from album Soaring)](https://www.youtube.com/watch?v=vZb2so5nJVU)

## INFILTATIONS

* [John Ottman - New York's Finest (from album The Usual Suspects OST)](https://www.youtube.com/watch?v=gAweqG0FhdQ)

## INQUIETANT

* [John Ottman - The Usual Suspects Theme (Piano Cover](https://www.youtube.com/watch?v=_f58qcdKFzQ)
* [John Ottman - Verbal Kint (from album Film Noir!)](https://www.youtube.com/watch?v=o-vDTqM9tyk)
* [John Morris - The Elephant Man (from album Film Noir!)](https://www.youtube.com/watch?v=6JPd5HdCMm0)
* [Angelo Badalamenti - Silencio (from album Film Noir!)](https://www.youtube.com/watch?v=s0cptLQtq_k)

## DIVERS

* [Le marché](https://www.youtube.com/watch?v=aTaIFSnReOs)
* [Temple d'Asmodée](https://www.youtube.com/watch?v=UxOLopND4CA)
* [Grognement de Troll](https://www.youtube.com/watch?v=h6hzBSZAoAA)
* [Grognement de Troll (best)](https://www.youtube.com/watch?v=FprLkSb95rM)