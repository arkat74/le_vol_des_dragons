---
layout: page
title: Trouver Floon
nav_order: 20
grand_parent: Préparations
parent: Parties
---

# TROUVER FLOON

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Voir [partie 5b - Trouver Floon](https://arkat74.gitlab.io/trad_dragon_heist_remix/partie-5b-trouver-floon.html)

## DRAGON À LA BROCHE

* Taverne en piteux état
* **Berca la tenancière**
  * a reconnu l'autre ami de Floon (Renaer)
  * ne se préoccupe pas des 5 malfrats du Zhentarim
* **Solomil Doigtdargent**, un nain éméché
  * dira tout aux PJs si, et seulement si, ils acceptent un combat 1 vs 1 à la loyal (sans armure, sans arme, sans magie) ou une partie de Sava.
  * **Combat :** Solomil a comme stat celle d'un Bandit mais pour le combat ses stats sont : CA 10, 32 PV, CaC +4, Dégâts 3. Comme il est éméché il est désavantagé sur ses attaques. S'il gagne, il dit tout en échange de 10 po. S'il perd, il dit tout gratuitement.
  * **Partie de Sava :** Stats de Solomil: INT +2, CHA 0, SAG 0. Pari de 10 po par partie. S'il gagne, il prend les po et offre une revanche en assurant donner les informations quelque soit l'issue de cette seconde partie. S'il perd, il donne les informations et ne prend pas les po.
* **Informations :**
  * Floon et Ranaer Neverember ont continué à boire puis sont sortis
  * sont suivis de près par 5 hommes habitué de la taverne
  * malfrats du Zhentarims
  * ont des tatouages de serpents ailés
  * sont souvent dans un entrepot de l'allée des Chandelles reconnaissable au dessin d'un serpent ailé sur la porte.


## L'ENTREPÔT DU ZHENTARIM

### LOCALISATION

allée des Chandelles

![Emplacement entrepôt](../assets/images/preps-partie-1-localisation-entrepot-zhentarim.png "Emplacement entrepôt")

* la ruelle est sombre et nauséabonde.
* une bougie éternelle magique d'un réverbère éclaire la devanture d'un entrepôt avec un dessin de serpent ailé au dessus de la poignée de porte (Test INT DD 10 pour faire le lien avec le Zhentarim).
* entrepôt est précédée d'un petite cour grillagée.

### PLAN

![Entrepôt du Zhentarim](../assets/images/preps-partie-1-entrepot-zhentarim.jpg "Entrepôt du Zhentarim")

#### Cour

* portail pas fermé.
* **3 points d'entrée**
  * une fenêtre au verre peint
  * une grande porte de chargement
  * porte principale (qui a un judas).
  * peuvent être crochetées avec des outils de voleur et un test de **Dextérité DD 12**
  * peuvent forcées avec un test de **Force (athlétisme) DD 10**.
* **serpent noir volant gît mort** dans la cour inférieure, transpercé par une flèche (note au MD : les Zhentarim ont essayé de l’envoyer comme messager pendant l’attaque, mais un agent de Xanathar l’a abattu).
* Si les **PJ se font entendre**, les kenkus à l'intérieur se cache rapidement et bruyamment, un PJ avec **perception passive de 16 ou plus** les entend.

#### Z1. Salle principale

* 5 cadavres de mercenaire humain du Zhentarim (portent tous un tatouage de serpent ailé noir sur cou ou avant bras)
* 7 cadavres de malfrat humain de la guilde de Xanathar (1 porte sur la main un tatouage noir - cercle avec 10 rayons pointent de la circonférence vers l'extérieur)
* tables, chaises et caisses avec divers matériels du Zhentarim.
* **[4 kenkus](kenkus.html)** sont là et se **battent jusqu'à ce que deux d'entre eux meurent**, ensuite les survivants **tentent de fuir**.
* PJs capturent un kenku et réussissent à l'intimider avec un test CHA (intimidation) DD 10, il révèle les infos suivantes :
  * (voix grave orc) "Xanathar vous transmet ses respects"
  * (voix petite nasillarde) "Attache le joli garçon dans la pièce à l'arrière !" et "Suis les signes jaunes dans les égouts"
  * (voix éraillée) "Pas le temps de piller cet endroit. Amenez-le simplement au chef"
* Kenku peuvent mener les PJs à la planque dans les égouts mais ne savent pas la décrire. **Ils tenteront de fuir**

#### Z2. Le Cagibi

* pièce sent fort le vinaigre et le poisson
* Au fond, sous une toile, respiration saccadée (Renaer Neverember qui raconte facilement son histoire une fois rassuré d'être en sécurité)
* rien d'autre
* Renaer pourra dire aux PJs
  * a été interrogé par les agents du Zhentarim sur le demi-million de dragons que son père a volé dans la ville
  * ont arraché un médaillon qui lui était très précieux
  * si PJs trouvent le médaillon et voient le compartiment secret (maintenant vide) à l’intérieur, Renaer pourra également leur dire qu’il n’avait aucune idée de l’existence de ce compartiment ni de ce qui y était stocké.

#### Z3. La pièce secrète

* pièce peut être découverte en réussissant un **test de Sagesse (Perception) DD 15**.
* ouverture de la porte déclenche une sonnerie de cloche à l'étage (Z5)
* pièce contient :
  * une caisse de **4 tableaux au cadre en bois** et enroulés dans du cuir d'une valeur de **75po chacun**. Ils représentent les villes de Luskan, Neverwinter, Silverymoon, Porte de Baldur.
  * **15 lingots d'argent** noircis de 5kg chacun (poids total : 75 kg) et d'une valeur unitaire de 50po (total : 750 po)


#### Z4. La galerie

* galerie surplombe salle principale
* ne contient que de vieilles caisses remplis de bibelots sans aucune valeur

#### Z5. Bureaux

* enfilade de bureaux non utilisés par les Zhents
* aperçoit une cloche dans chaque bureau, lié à la salle secrète du dessous.
* fouille explicite des bureaux, PJs peuvent trouver 
  * **[un oiseau de papier](objet-oiseau-de-papier.html)**
  * **médaillon de deuil de Renaer**

## ARRIVÉ DU GUET

* Hyustus Staget (Capitaine du Guet) arrive dans l'entrepôt avec 10 vétérans
* tentent d’empêcher quiconque de sortir
* interroge tout le monde et finit par donner le code pénal aux PJs en les mettant en garde.
* si Kenkus vivant ils les emmènent.
* Hyustus Staget
  * paticipe au maintient de la paix dans le quartier de Docks
  * ne boit pas, ne se laisse pas corrompre, ne se laisse pas emporter par la colère
  * connu et respecté de tous dans le quartier
  * avait mis l'entrepôt sous sureveillance, mais y avait ensuite mis fin
  * surveillance pour attraper un gros bonnet Zhent, un dénommé **Urstul Floxin** (**ne le révèlera qu'à Rynnarod**)
  * **reconnait Renaer** mais ne le connait pas plus que cela
  * noble impliqué => Staget se montrent sous son meilleur jour
  * n'aidera pas plus que ça les PJ dans leur quête mais ne les empêchera pas non plus tant que leurs actions resteront dans les égouts.

## LOCALISER FLOON

* si PJs ont pensé à interroger un kenku, ils savent déjà qu'il leur faut aller dans les égouts, rue de la chandelle, et suivre les marques jaunes.
* interrogation des gens du quartier (coût 5 po)
* test de Sagesse (Survie) DD 11 réussi permet de suivre facilement leurs traces
* demander à Renaer => 2 contacts
* Mirt
  * rencontre dans son manoir accompagné de Renaer, au coin de l'allée de l'Argent Terni et de l'allée des Pièces (emplacement 52 sur la carte)

![Manoir de Mirt](../assets/images/preps-partie-1-manoir-mirt.png "Manoir de Mirt")

  * demande info aux Ménestrels via sa *Pierre messagère*
  * questionne les PJs sur leurs motivations et histoires respectives
  * reçoit finalement le message
* Vajra Safahr
  * rencontre à la Tour du Bâton Noir accompagné de Renaer sur la rue des Épées (emplacment 6 sur la carte)

![Tour du Bâton Noir](../assets/images/preps-partie-1-tour-baton-noir.png "Tour du Bâton Noir")

  * demande à ses espions via un sort de *Envoi de message*
  * questionne les PJs sur leurs motivations et histoires respectives
  * reçoit finalement le message

## VERS LA PLANQUE DE LA GUILDE DE XANATHAR 

* les membres de la Guilde de Xanathar sont sortis de l’entrepôt par la cour et ont accédé aux égouts à un demi-pâté de maison plus loin dans l’allée
* dans les égouts
  * pas de lumière naturelle => ceux qui n'ont pas l'infravision devront sortir une torche ou tout autre source de lumière.
  * 30 cm d'eau usée
  * test de Sagesse (Perception) DD 13 pour remarquer le signe de guilde, si trouvé, plus de tests de survie nécessaire
  * sinon 3 tests de Sagesse (Survie) DD 13 réussis pour trouver la planque
    * 1 échec: les PJs perdent un temps considérable à faire marche arrière et à reprendre la piste
    * 3 échecs, ils ont perdu trop de temps : lorsqu’ils arrivent à la planque, ils la trouvent abandonnée, à l’exception des gardiens gobelins en Q2 et de Zemk, le gardien habituel de la planque, en Q5. Le cadavre de Floon repose en Q7 (Zemk le jettera dans l’égout plus tard dans la journée quand il aura le temps de s’en occuper).
  * **signe de guilde** : des symboles griffonnés à la craie jaune - une représentation stylisée de Xanathar - sont marqués à chaque intersection de tunnel indiquant le chemin qui doit être suivi par la direction que l’oeil principal regarde
* **rencontre un [Observateur](observateur.md)** dans les égouts (voir p.27)
  * à une intersection à 3 embranchements
  * échelle grimpe et mène dans la cave du Poisson Bondissant (taverne du quartier des Docks)
  * signe de guilde inscrit 
  * créature sphérique de la taille d'un gros pamplemousse, oeil central et 4 petits pédoncules

## LA PLANQUE DE LA GUILDE DE XANATHAR

![Planque de la Guilde de Xanathar](../assets/images/preps-partie-1-planque-xanathar.png "Planque de la Guilde de Xanathar")

* toutes les portes sont déverrouillées

#### Q1. Croisée centrale

* intersection circulaire
* murs extérieurs percés de 2 meurtrières qui se font face
* deux chemins continuent, un vers le nord et un vers le sud.
* corniche au sud-ouest

Si les PJs font du bruit, 2 gobelins se réveillent en Q2a et Q2b et leur tirent dessus par les meurtrières (leur fournit **abri important**).

En cherchant bien avec un test de Sagesse (Perception) DD 15, les PJ peuvent trouver la porte secrète vers Q2b

#### Q2. Postes de garde

* 2 gobelins (1 en Q2A et 1 autre en Q2B)
* vision dans le noir
* endormis, peuvent être passés avec un test de Dextérité (Discrétion) DD 9

Trésor : Chaque Gobelin a sur lui une bourse contenant 1d6 pc

#### Q3. Pièce en désordre

* ne contient aucun objet de valeur
* juste d'armes rouillées et vêtements élimés.

#### Q4. Dortoir vide

* ne contient aucun objet de valeur
* 6 matelas de paille
* Si les occupants en Q5 sont y sont toujours alors les PJs **entendent du bruit venant de Q5** en arrivant dans cette salle.

#### Q5. Dortoir

* 6 matelas de paille
* Zemk un duergar
* Krentz, un bandit humain, s'il n'a pas été tué ou attrapé pendant l'échauffourée du Portail Béant.
* Zemk est en train de barricader la porte vers Q6
* Krentz lui dit que ça ne sert à rien, qu'il faut boucher le vide sous la porte plutôt.
* **n'entendent pas les PJ arriver** sauf si ces derniers font exceptionnellement **beaucoup de bruit**.
* Zemk se bat jusqu'à la mort.

Krentz réagit selon plusieurs options :
- si les PJ l'ont aidé au Portail Béant, il leur laisse le choix de partir mais se bat aux côtés de Zemk si les PJ persistent.
- sinon il les attaque directement avec Zemk mais tente de fuir si son acolyte tombe.

Trésor : Les deux ont les poches vides.

#### Q6. Latrines

* trou
* une **vase grise** est sortie du trou des latrines et a déjà tué 2 gobelins

#### Q7. Combat contre le chef

* longue salle
* rideaux élimés sur le mur est
* demi-orc musclé, Grum'shar, **apprenti magicien**
  * chef insignifiant de la Guile de Xanathar
  * vêtu d'une robe miteuse
  * se trouve au milieu de la salle
  * pied posé sur la poitrine d'un humain aux cheveux vénitiens ondulés
  * poing serré nimbé de flammes
  * victime se tord de douleur et essaie de se dégager
* créature cauchemardesque (Nihiloor), un **flagelleur mental**
  * vétue d'une robe noire
  * grand yeux entièrement blanc, peau caoutchouteuse violette, quatre tentacules autour de sa bouche
  * assise sur une plate-forme surélevée au sud
  * caresse un **dévoreur d'intellect**

![Nihiloor](../assets/images/nihiloor.jpg "Nihiloor")

À l'arrivée des PJs, [Nihiloor](nihiloor.html) se lève et quitte la planque en laissant à Grum'shar et son dévoreur d'intellect de soin le s'occuper d'eux. Il **part à l'ouest vers le portail en Q11**.

[Grum'shar](grumshar.html) et le dévoreur d'intellect combattent jusqu'à la mort.

Trésor : 
* Grum'shar possède un grimoire dans sa sacoche avec les sorts *mains brûlantes*, *déguisement*, *simulacre de vie*, *bouclier*, *serviteur invisible* et *carreau ensorcelé*
* un coffre en bois derrière la chaise de pierre où était assis au départ Nihiloor, elle contient 2 potions de soins, 16 po, 82 pa, 250pc.

#### Q8. Échappatoire

* salle vide
* test Sagesse (Perception) DD 10 permet de découvrir une dalle amovible qui ouvre le chemin vers Q9

#### Q9. Cave privée

* salle est en fait la cave d'une maison halfline de l'allée des Boyaux-de-Poisson du quartier des Docks, les Peabody, qui brasse leur propre bière ici
* les Peabody ne sont pas au courant de ce passage et après une belle frayeur de voir arriver les PJs comme ça de leur cave, ils s'engagent à le boucher très rapidement.
* pour rentrer dans la cave réussir un test de Force DD 10

#### Q10. Dortoir

Toutes les salles sont de simples dortoirs avec des paillasse au sol et sans objet de valeur.

#### Q11. La porte de derrière

* salle avec un pilier en pierre en son centre
* gravé sur le pilier symbole d'un cercle avec 10 rayons partant de sa circonférence pour aller vers l'extérieur

![Symbole guilde de Xanathar](../assets/images/tatouage-xanathar.png "Symbole guilde de Xanathar")
* au centre du cercle, un renfoncement circulaire
* pilier est en fait un portail vers l'antre de Xanathar (X22)
* il faut la pierre en forme d'oeil de Nihiloor pour l'activer
* porte secrète visible

#### Q12. Cave d'auberge

* passage étroit (créature taille Moyenne max)
* mène à la cave d'une auberge tenue par des halfelins, rue des Épices dans le quartier des docks.
* auberge est la base des Mépriseurs d'éclat, une bande de rats-garous halfelins
* Roscoe garde la cave et préfère fuir que se battre jusqu'à la mort mais il le fera si besoin.
* tous les autre membres du gang sont absents.
