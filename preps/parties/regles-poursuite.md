---
layout: page
title: Règles de poursuite
nav_order: 3
grand_parent: Préparations
parent: Parties
---

# RÈGLE DE COURSE-POURSUITE

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

Article original : [How To Run a Chase in 5e D&D…. Step by Step Rules!](https://www.hipstersanddragons.com/new-chase-mechanics-5e-dnd/)

Je vais organiser les poursuites comme une série de tests entre les participants. Un succès de la proie sur un poursuivant donné signifie qu'il y a un niveau de séparation (désormais appelé un "écart") entre les deux. Un succès pour un poursuivant signifie que la proie n'est pas en mesure de creuser une distance entre eux, tandis qu'un succès de 5 signifie que le poursuivant comble 1 écart sur la proie.

*Note :* en fonction de la façon dont les tests se déroulent, il se peut que la proie doive gagner de 5 points pour creuser un écart. Un match nul ne permet pas aux participants de gagner de 5 points.

**Pour résumer :**

* Succès pour la proie = +1 écart
* Succès pour le poursuivant = maintient de la distance
* Succès du poursuivant de +5 = -1 écart.

Cela favorise évidemment un peu la proie, même si je pense qu'il est plus facile de s'échapper que d'attraper, et que la plupart des proies seront poursuivies par plusieurs Poursuivants... donc ça me va. Je suggère également, dans le même paragraphe, que tu pourrais changer pour...

Succès de la proie de +5 : +1 écart
Aucun des deux ne gagne le test de +5 = maintient de la distance
Succès du poursuivant de +5 = -1 écart

Voici une feuille d'instructions sur la façon d'exécuter une course-poursuite

## Établir qu'une poursuite a commencé

Une poursuite commence lorsqu'une créature utilise à la fois son action de déplacement et son action de Se précipiter pour fuir, et qu'au moins une autre créature décide de la poursuivre, en utilisant à la fois son action de déplacement et son action de Se précipiter pour tenter de la suivre (pour être sur un pied d'égalité, elle doit le faire avant le début du prochain tour du fuyard). Lorsque cela se produit, le poursuivant déclenche une course-poursuite, et la proie et le poursuivant effectuent tous deux des jets de dé. Tout autre poursuivant participe également aux tests, en suivant l'ordre d'initiative et en comparant son jet à celui de la proie.

## Établir la compétence appropriée pour les tests

En tant que MD, déterminez la compétence que vous souhaitez utiliser comme base des tests de poursuite. Je suggère la Force (Athlétisme) pour une poursuite se déroulant sur un terrain relativement ouvert, comme un champ ou une colline, ou la Dextérité (Acrobatie) pour une poursuite sur un terrain riche en obstacles, comme une forêt dense ou les rues sinueuses et encombrées d'une ville.

(Si vous ne voulez pas punir les PNJ et les monstres qui n'ont pas tendance à avoir autant de compétences que les PJ, vous pouvez opter pour des tests directs de Force / Dextérité. Et si vous voulez quelque chose entre la Force et la Dextérité, vous pouvez opter pour un test de compétences croisées... Dextérité (Athlétisme), par exemple).

## Établir le modificateur de poursuite de chaque créature

Nous devons refléter le fait que certaines créatures sont plus rapides que d'autres, et que certaines - comme les voleurs avec leur capacité de Ruse - ont une mobilité accrue. Nous pouvons le faire en appliquant des "modificateurs de poursuite" supplémentaires aux tests.

Pour chaque 1,5 mètre de mouvement au-dessus de 9 mètres, une créature ajoute +4 à son modificateur de tests de poursuite, pour chaque 1,5 mètre en moins, utilisez un modificateur de -4. (En d'autres termes, une créature avec une vitesse de 12 mètres ajoute +8 à son tests de poursuite, tandis qu'une créature avec une vitesse de 7,5 mètres a un modificateur de -4).

Les créatures qui utilisent une capacité, comme la capacité Ruse du roublard, pour effectuer l'action de Se précipiter deux fois en un round, gagnent un avantage sur leur jet de test de poursuite.

## Déterminer le succès de tout poursuivant

Déterminez si un poursuivant perd 1 écart, maintient la distance ou comble 1 écart sur la proie, en comparant ses jets à ceux de la proie.

Tout poursuivant qui termine son tour avec zéro écart entre lui et la proie peut faire une action Attaquer dirigée vers la proie (indice : il peut vouloir choisir d'agripper la proie pour tenter de mettre fin à la poursuite).

Comme pour les règles du DMG, nous supprimons les attaques d'opportunité une fois que la poursuite est en cours, donc si la proie est toujours en vie et n'est pas agrippée, elle peut continuer de fuir sans provoquer d'autres attaques.

## S'échapper / Mettre fin à la poursuite

Lorsque la poursuite commence, en tant que DM, déterminez combien d'écarts la proie doit creuser entre elle et son plus proche poursuivant pour s'échapper et terminer la poursuite. Je suggérerais entre 3 et 4 espaces pour une poursuite urbaine, ou 4 ou 5 pour une poursuite plus ouverte.

En option, vous pourriez donner à la proie une chance de mettre fin à la poursuite 1 écart plus tôt que les écarts requis pour distancer les poursuivants, en effectuant un test de Dextérité (Furtivité) contre la Sagesse (Perception) de tous les poursuivants. En cas de succès, elle a déjoué ses chasseurs, en trouvant une cachette ou en se mettant à l'abri. En cas d'échec, en tant que MD, vous devrez décider si la proie est maintenant coincée ou si elle est en mesure de s'élancer et de recommencer la poursuite.

Ces 5 étapes devraient vous donner un aperçu solide d'un mécanisme de poursuite utilisable.

Quelques éléments supplémentaires à garder à l'esprit...

## Mesure des distances / Points de départ variables

Un écart n'est pas censé représenter une distance exacte, mais, lorsque vous en avez besoin, vous pouvez considérer qu'un écart représente environ 9 mètres. Cela signifie que lorsqu'une créature commence à 18 mètres d'un adversaire qui se retourne et s'enfuit, la poursuite commence avec 2 écarts entre la proie et son poursuivant, avant même que le premier jet de dé ne soit effectué.

Dans le scénario où une créature s'enfuit d'un combat, et est poursuivie non seulement par la créature qu'elle combattait, mais aussi par une seconde créature qui était légèrement plus éloignée sur la grille de combat, alors la seconde créature subit un modificateur de -2 pour chaque 1,5 mètre qui la sépare de la proie (avant que celle-ci ne s'enfuie) lors de son premier test de poursuite. Évidemment, si elle était à 9 mètres, il suffit de commencer avec 1 écart entre elles, avant le début du premier test. (Si elle était à 12 mètres, commencez avec un écart de 1 et un modificateur de -4 sur le premier jet de dé).

**Règle optionnelle :** Si quelqu'un veut poursuivre tout en utilisant son action (pour lancer un sort, etc.), alors vous pouvez le laisser perdre automatiquement un écart sur la proie et faire le test habituel pour en perdre potentiellement un deuxième. Dans ce cas, supprimez la possibilité de combler un écart, même s'ils obtiennent un résultat de 5 au-dessus de la proie dans le test de poursuite.

Quelqu'un qui n'utilise ni son mouvement, ni son action pour s'élancer, perd automatiquement 2 écarts sur la proie. (Cela peut se produire si quelqu'un choisit de faire quelque chose avant d'entrer dans la course-poursuite).

## Pensez à introduire l'épuisement

Je ne m'embêterais pas à introduire des tests d'épuisement dans les poursuites pour commencer, car ils ralentiraient encore plus la scène, ce qui est à peu près la dernière chose que vous voulez pendant une poursuite à grande vitesse. Mais une fois que vous avez une bonne maîtrise de ces mécanismes, je pense qu'il y a un certain réalisme et un certain mérite aux règles du DMG (p.252).

Pour résumer : une créature peut utiliser l'action Se précipiter un nombre de rounds successifs égal à 3 plus son modificateur de Constitution. Après cela, elle doit effectuer un test de Constitution DD 10 ou subir un niveau d'épuisement. (Les niveaux d'épuisement gagnés pendant la poursuite peuvent être supprimés par un simple court repos).

## Obstacles / Complications

La gestion des obstacles est intégrée dans ce système de variantes des règles de poursuite, en ce sens que le succès ou l'échec des tests de poursuite dépend effectivement de la façon dont une créature gère des choses comme des branches basses, des racines d'arbres, des fossés, ou dans une poursuite urbaine, des foules, des chariots, des coins serrés, des tas de détritus, etc....

Cependant, rien ne vous empêche d'ajouter les complications de poursuite du DMG (p.254), une fois que vous avez mis en place les mécanismes de base. Il suffit d'utiliser le bon sens pour ajuster le résultat à ce système. Si la proie glisse et tombe à plat ventre par exemple, chaque poursuivant peut gagner automatiquement 1 écart (s'il n'est pas lui-même la victime du même obstacle).

Une autre façon de gérer les obstacles ou les changements de décor lors d'une poursuite serait de changer la compétence utilisée pour le concours pendant un tour. Par exemple, si vous utilisez la Dextérité (Acrobatie) pour une poursuite dans les ruelles étroites de Waterdeep, vous pouvez passer à la Force (Athlétisme) lorsque la poursuite débouche sur un long tronçon de route principale. C'est aussi un peu plus rapide que de consulter une table, ce qui peut ralentir les choses.

## Narration d'une course-poursuite

Il n'est que trop facile pour une poursuite potentiellement époustouflante dans Donjons & Dragons de se transformer en une suite de lancers de dés fastidieux, que vous utilisiez mon système ou le RAW (Rules As Written).

Un jet de dé pour établir ou combler un écart, sans aucun contexte descriptif, est extrêmement aride et ennuyeux. Un jet de dé pour déterminer avec quelle habileté un PJ parvient à sauter par-dessus des barils qui tombent et à contourner un virage serré est beaucoup plus immersif et amusant.

En d'autres termes, le succès d'une scène de poursuite dans D&D dépend plus de la façon dont vous la décrivez que des mécanismes, alors donnez-vous la permission d'improviser et de vous amuser.

Mettez également les joueurs dans le coup, en décrivant le décor de la poursuite et en leur demandant de raconter comment leur personnage s'en sort parmi les cages à poules empilées, les barils de pétrole qui tombent, le troupeau de moutons paniqués, etc.

## Commentaires

**Max :**

Ceci étant dit, je ne comprends pas les fondements de cet article. Les conditions pour mettre fin à une poursuite sont évidentes, et les moyens d'y parvenir sont déjà intégrés dans les règles de base : vous avez une vitesse de déplacement, l'autre camp a une vitesse de déplacement, et toutes les parties peuvent choisir d'utiliser le Se précipiter pour augmenter la leur pendant un tour donné, ou pas. Le problème n'était pas qu'il n'y avait aucun moyen de mettre fin à une poursuite, vous la résolvez en utilisant de simples mathématiques. La théorie de la section du DMG sur les poursuites (p252, comme vous le notez) est que le seul système de votre vitesse contre ma vitesse ne parvient pas à capter un résultat commun dans lequel des créatures plus lentes sont capables de rattraper des créatures plus rapides par hasard et/ou par endurance.

Ce dernier point est traité dans le DMG p252, et il y est brièvement fait allusion au début de votre article. Il y est dit que vous ne pouvez utiliser votre option Se précipiter que 3 + [modificateur de CON] fois avant de devoir commencer à lancer des jets de Con pour éviter de prendre des niveaux d'épuisement. L'épuisement est un agent limitant ici, évidemment, et fournit un ralentissement naturel de la durée de la poursuite, toutes choses étant égales par ailleurs.

Mais, bien sûr, cela serait presque aussi ennuyeux que le système original "sans système", et ne correspondrait pas à la réalité (même une réalité imaginaire). Donc, le premier, la chance pure, est exprimée par une combinaison de la mécanique de dissimulation de la proie (DMG p253) et de la table des Complications de poursuite dans le DMG p254, comme indiqué dans votre article. Toutes les complications ont un effet délétère sur votre vitesse de déplacement au cours de ce tour, améliorant ainsi les chances que vous perdiez la plus grande poursuite car, par simple calcul, nous verrons que l'écart s'est creusé.

Puisque toutes les créatures ont également une limite de vision, après laquelle il peut être décidé qu'un sujet est hors de vue, la mécanique pour la proie se cachant ou non gère ce scénario ... si elle réussit, la poursuite est terminée. Si elle échoue, la poursuite continue et la distance entre les deux se réduit en vertu du fait que les poursuivants ajoutent leur pleine vitesse de déplacement pour ce round, et que la proie n'ajoute rien (parce qu'elle était occupée à essayer de se cacher dans une zone proche, et non à fuir la poursuite). Cela devient plus compliqué dans un cadre urbain, mais dans ces types de scénarios, au lieu de limitations de vision en soi, utilisez quelque chose comme une " portée d'information (perturbation) " de 50′-100′, c'est-à-dire la distance à laquelle le poursuivant et la proie échangent encore des informations via quelque chose d'autre qu'une ligne de vue directe (à savoir, le chahut des piétons surpris réagissant en temps réel au passage de la proie).

Quoi qu'il en soit, le fait est qu'en utilisant quelques calculs très simples et la table Complications, vous pouvez résoudre des poursuites dans des environnements riches en fonctionnalités assez facilement en utilisant uniquement le RAW.

