---
layout: page
title: Chronologie globale
nav_order: 1
grand_parent: Préparations
parent: Parties
---

# CHRONOLOGIE GLOBALE

{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## L'ASCENSION ET LA CHUTE DU SEIGNEUR NEVEREMBER

* 1451 CV : Neverwinter est détruite quand un petit groupe d'aventuriers (dont Jarlaxle Baenre) réveille le [Primordial](https://forgottenrealms.fandom.com/wiki/Primordial) [Maegera](https://forgottenrealms.fandom.com/wiki/Maegera) sous le [Mont Hotenow](https://forgottenrealms.fandom.com/wiki/Mount_Hotenow).
* 1467 CV : le Seigneur Dagult Neverember, Seigneur Manifeste de Waterdeep, se proclame souverain de Neverwinter et commence le programme New Neverwinter pour reconstruire la ville.
* le Seigneur Neverember commence à détourner l'argent du trésor de Waterdeep.
* le Seigneur Neverember découvre la Pierre de Golorr à Neverwinter.
* Lorsque Dame Alethea Brandath (la femme du Seigneur Neverember) meurt, le Seigneur Neverember apprend l'existence d'une ancienne chambre forte naine sous le Mausolée de Brandath.
* Le Seigneur Neverember accède à l'ancienne chambre forte de Melairkyn sous le mausolée de Brandath. Il commence à y stocker l'argent détourné (totalisant finalement un demi-million de dragons d'or) et utilise la Pierre de Golorr pour cacher son existence.
* Comme sécurité supplémentaire, le Seigneur Neverember rend la Pierre de Golorr aveugle : laissant la Pierre dans le Palais de Waterdeep, il garde un des Yeux avec lui à Neverwinter, en donne secrètement un à son fils (caché dans un médaillon de deuil), et cache le dernier dans le Mausolée de Brandath.
* Pendant ce temps, diverses factions se rendent compte que le Seigneur Neverember est engagé dans un grand projet dont on ne connaît que les contours. L'expression "l'Énigme de Neverember" est inventée.
* 1489 CV : Laeral Silverhand retourne à Waterdeep et le Seigneur Neverember est déposé.
* Dans la confusion de la transition du pouvoir, le Seigneur Neverember envoie des agents pour récupérer la Pierre de Golorr au palais. Lorsque ces agents quittent la ville, ils sont pris en embuscade par des agents de Xanathar qui volent la Pierre.

## LE GRAND JEU

* Les Cassalantre récupèrent ce qu'ils croient être l'Énigme de Neverember - mais qui n'est en fait qu'un des Yeux - dans le Mausolée de Brandath. Leurs recherches révèlent rapidement qu'il fait partie de la Pierre de Golorr.
* Le Seigneur Neverember envoie un groupe d'agents à Waterdeep. Beaucoup de ces agents tentent de localiser la Pierre. L'un d'entre eux, Dalakhar, est chargé de garder un oeil sur Renaer, le fils de Dagult (Dalakhar n'est pas informé de cela, mais la principale préoccupation du Seigneur Neverember est que l'Oeil porté involontairement par Renaer soit gardé en sécurité jusqu'à ce qu'il puisse récupérer la Pierre).
* Les Zhentarim de Manshoon volent l'Oeil dans l'Enclave du Protecteur à Neverwinter.
* Les comptables de Laeral Silverhand découvrent le détournement du Seigneur Neverember. La nouvelle ne tarde pas à se répandre.
* Les Zhentarim se présentent à Xanathar pour proposer une alliance. Xanathar tue l'ambassade et prend leur Oeil. Une guerre des gangs éclate entre la Guilde et le Réseau Noir.
* Le Seigneur Neverember découvre que Xanathar a volé la Pierre. Dalakhar est envoyé pour infiltrer l'organisation de Xanathar et reprendre la Pierre (à ce stade, le Seigneur Neverember croit que Xanathar possède la Pierre et que les Zhentarim ont toujours l'Oeil qu'ils lui ont volé. Pour maintenir le cloisonnement initial des informations, Dalakhar n'est pas informé sur les Yeux et ne sait pas qu'il faut chercher celui que Xanathar détient maintenant).
* Jarlaxle Baenre arrive à Waterdeep et commence à vendre des Vifs-acier.

## DÉBUT DU VOL DES DRAGONS (1492 CV)

* 1 Ches : les joueurs arrivent au Portail Béant, sauvetage Renaer
* 2 Ches : sauvetage Floon
* 4 Ches : récupération et exploration Manoir du Crâne-de-troll
* 5 Ches : visite Floon et Renaer
* 6 Ches : mission donnée par Jalester (les 3 épouvantails de Sousfalaise)
* 8 Ches : retour à Waterdeep avec le Gang
* 9 Ches : attaque rat-garou de Frewn
* 10 Ches : annonce de l'arrivée du Carnaval des Sirènes
* 10 Ches : Mission 3 des Ménestrels
* 12 Ches : rats-garous de Frewn cherchent à se faire embaucher comme cuisinier
* 12 Ches : Mission 3 de l'Alliance des Seigneurs
* 19 Ches : ouverture du Manoir de Crâne-de-troll
* 20 Ches : Dalakhar vole avec succès la Pierre à Xanathar
* 20 Ches : les rats-garous de Frewn tentent d'empoisonner la nourriture de l'auberge
* 21 Ches : Carnaval des Sirènes participe aux Parades des Jumeaux de Selûne/Sashelas
* 22 Ches : Dalakhar est tué par le Vif-acier des Gralhund
* 23 Ches : invitation des Cassalantre (pour le 24)
* 24 Ches : chez les Cassalantre
* 24 Ches : le soir, raid sur la villa Gralhund
* 25 Ches : lettre de Kalain (arrivée à l'auberge de la Dague Sanglante)
* 25 Ches : défilé du Carnaval des Sirènes jusqu’à la Maison des constructeurs naval
* 25 Ches : Bal des Constructeurs naval, invitation des Cassalantre
* 28 Ches : le soir intervention du Xanathar/Zhentarim (à voir lequel)
* 1er Tarsakh : Carnaval des Sirènes rejoint le défilé des fêtes de Caravance
* 5 Tarsakh : Carnaval des Sirènes, défilé spécial à minuit pendant la Nuit-Dorée
* 11 Tarsakh : enfants Cassalantre ont leur âme aspirée en enfer
