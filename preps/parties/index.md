---
layout: page
title:  "Parties"
has_children: true
parent: Préparations
nav_order: 1
---

# Préparations des séances

Liens pour plans Braegan d'Aerthe :
* [l'Agitateur et le Brise coeur](https://www.reddit.com/r/WaterdeepDragonHeist/comments/p3mgje/sea_maidens_faire_waterdeep_dragon_heist_34x34/)
* [le Tape-à-l'oeil](https://www.reddit.com/r/dungeondraft/comments/p7faky/the_eyecatcher_in_winter_34x34_waterdeep_dragon/)
* [Vue extérieur des navires](https://www.reddit.com/r/WaterdeepDragonHeist/comments/lnrx75/sea_maiden_fair_shipviews_blueprint/)
