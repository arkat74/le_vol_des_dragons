---
layout: page
title: La Villa Gralhund
nav_order: 22
grand_parent: Préparations
parent: Parties
---

# ATTAQUE SUR LA VILLA GRALHUND
{: .no_toc }

<details open markdown="block">
  <summary>
    Table des matières
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

![Plan de la Villa](../../../assets/images/villa-gralhund.jpg)

## DISTANCE POSTE OBSERVATION PJ ET VILLA

* 226 m
* Marche (normal) : 2 minutes 30 sec
* Marche (rapide) : 1 minute 40 sec
* Course : 12 rounds

## FORCES EN PRÉSENCE

### ZHENTARIM

* Urstul Floxin (assassin)
* 10 Zhents

### GRALHUND

* 20 gardes dont :
    * 4 gardes dans la salle à manger (G8)
    * 2 gardes dans le petit salon (G9)

* Orlond Gralhund (noble)
* Yalah Gralhund (noble)
* Vif-acier
* Hrabbaz
* enfants Gralhund (Zartan + Greth)
* 13 serviteurs (dont 3 morts)

### BREGAN D'AERTHE

* Fel’Rekt Lafeen
* 2 pistoliers drow
* 8 drows

## GESTION DES GARDES GRALHUND

Si l’alarme est déclenchée, alors :

1. 2 des gardes de la salle à manger (G8) se dirigeront vers la zone d’alerte.
1. 1d4 tours plus tard, 4 gardes armés du Quartiers des Gardes (G4) se mobilisent. Deux se dirigeront vers la zone d’alerte ; les deux autres se dirigeront vers le Seigneur Gralhund.
1. 1d4 tours plus tard, les gardes non armés du Quartiers des Gardes (G4) qui n’étaient PAS endormis se mobiliseront avec des armes mais sans armure.
1. 1d4 tours plus tard, les gardes non armés du Quartiers des Gardes (G4) qui DORMAIENT se mobiliseront avec des armes mais sans armure.

## DÉROULÉ DE L'ATTAQUE DES ZHENTS

1. Urstul Floxin et 10 Zhents en armure de cuir noir crochetent la serrure du garde-manger (G6)
2. en G6 découvrent deux domestiques (le majordome et la cuisinière halfeline), utilisent le garde-manger pour une liaison amoureuse illicite et nocturne
3. cuisinière s’enfuit vers cuisine (G5), criant au meurtre. Zhents assassinent les deux serviteurs.
4. cris de la cuisinière alertent la servante en chef (travaillait dans la buanderie (G7)). 
    1. vient voir de quoi il s’agit
    1. entre dans la cuisine, voit, par la porte d’en face Zhents tuer la cuisinière
    1. se retourne et court à travers la buanderie en criant.
5. Floxin dit à la moitié de ses hommes de la poursuivre
    1. la rattrapent dans l’escalier arrière de l’aile des serviteurs (G19) et la tuent. 
    1. 2 autres domestiques qui descendaient les escaliers remontent en courant
    1. déclenchant l’alarme
    1. domestiques finissent par se barricader en G19
6. cris de la servante en chef alertent les 4 gardes dans la Salle à Manger (G8)
    1. un d’eux sort par les portes d’entrée et fait le tour du quartier des gardes (G4) pour réveiller le reste de la garde.
    1. trois autres se déplacent dans la buanderie (G7) et commencent à combattre les Zhents là-bas.
7. Cela fonctionne en fait légèrement à l’avantage de Floxin, laissant la salle à manger (G8) vide
    1. lui et 4 Zhents se précipitent du garde-manger (G6) à l’extrémité sud de la salle à manger (G8)
    1. 2 gardes en patrouille dans le petit salon (G9) sortent par la porte
    1. Urstul et ses hommes attaquent, ramenant les gardes en G9 et les tuent
8. Seigneur Orond sort de la bibliothèque (G12)
    1. Urstul, toujours dans le couloir juste à l’extérieur du petit salon (G9) le voit, pousse un cri
    1. Seigneur Orond s’enfuit en haut des escaliers
    1. Urstul et ses hommes le pourchassent
9. gardes qui combattent les Zhents dans la buanderie (G7) sont mortellement blessés
    1. reviennent presque simultanément à la salle à manger (G8)
    1. Zhents qu’ils combattaient les poursuivent et les tuent.
10. à l’étage, les gardes du palier supérieur (G13) ont déjà barricadé les portes de la chambre parentale (G16) où se trouvent Dame Yalah et les enfants
    1. Seigneur Orond arrive en courant à l’étage, l’un des gardes l’introduit dans la chambre d’amis (G15)
    1. garde est tué par Urstul Floxin alors qu’il ferme la porte
11. pendant ce temps, les gardes sortent des baraquements (G4)
    1. entrent dans la maison par la salle à manger (G8) et la cuisine (G5)
    1. engagent la deuxième force Zhent dans la salle à manger (G8)
12. à l’étage, les choses vont mal pour les Gralhund
    1. Zhents expédient l’autre garde du palier supérieur (G13)
    1. Zhents en bas tiennent la ligne et réussissent à y tuer huit gardes au total
    1. mais après que deux Zhent aient été tués, sont forcés de battre en retraite dans les escaliers
13. furieuse mêlée éclate en haut de l’escalier
    1. Urstul tente désespérément de défoncer la porte de la chambre d’amis (G15b) pour rejoindre le Seigneur Orond et le prendre en otage
    1. certains Zhent parviennent à enfoncer la porte de la chambre parentale (G16)
    1. sont ensuite forcés de se retourner et de se battre pendant que les gardes arrivent en montant les escaliers

## LA SUITE

**Une fois les Gralhund pris au piège, Yalah demande au vif-acier de s'enfuir avec la Pierre**. Celui-ci tentera de sauter du 1er étage par une fenêtre pour s'enfuir.

1. équipe de Jarlaxle se met en mouvement
    1. Fel’Rekt Lafeen et 6 drows traversent le toit de la villa Gralhund,
    1. prennent position juste au-dessus de la galerie (G17)
    1. Deux pistoliers drows et deux autres drows se déplacent sur les toits et prennent position en face des grandes fenêtres du Palier supérieur (G13).
2. les Gardes des Gralhund réussissent à tuer le reste des Zhents. Pour l’instant, il **ne reste que quatre gardes**
3. les tireurs d’élite drow abattent les gardes des Gralhund à travers la fenêtre
    1. simultanément, Fel’Rekt et son équipe tombent sur la galerie (G17)
    1. ouvrent les portes
4. Dame Yalah s’enfuit dans la chambre d’enfants (G18)
5. pendant qu’un drow va à G15 et récupère le Seigneur Orond
    1. Fel’Rekt passe 6 tours en duel contre Hrabbaz
    1. finalement le tue
6. le Seigneur Orond est traîné dans la chambre parentale (G16)
    1. Fel’Rekt négocie avec Dame Yalah par la porte
    1. Dame Yalah finit par rompre et, en sanglotant, donne à Fel’Rekt la Pierre de Golorr
    1. lui et son équipe s’enfuient par le chemin qu’ils ont emprunté
7. le Guet fait irruption dans la maison à l’étage inférieur

## À LA POURSUITE DU VIF-ACIER

* le vif-acier a été envoyé se cacher chez des amis bourgeois redevables : la famille Iowyn
* localisation à l'est de la rue Sulmor

![Refuge du vif-acier](../../../assets/images/preps-partie-3-refuge-vif-acier.png)

* il est en état "de veille" dans le salon principal

![Manoir des Iowyn](../../../assets/images/preps-partie-3-manoir-iowyn.png)

* le manoir est habité par un vieux bourgeois et sa femme. Ils n'ont que 2 serviteurs et 2 gardes
* personne n'interviendra tnt que les PJ ne s'en prennent pas à la famille, mais un serviteur ira prévenir le Guet
* le Guet interviendra au bout de 10 rounds, une fois le combat avec le vif-acier démarré
* une équipe d'intervention de Bregan d'Aerthe est déjà sur place (Felrek, 2 pistoliers drow et 2 drows)
* l'équipe n'interviendra pas mais suivra les PJ jusqu'au manoir du Crâne-de-troll